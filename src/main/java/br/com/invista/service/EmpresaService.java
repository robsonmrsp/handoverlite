package br.com.invista.service;

import java.util.List;
import org.joda.time.LocalDateTime;
import br.com.invista.model.Empresa;
import br.com.invista.core.persistence.pagination.Pager;
import br.com.invista.core.persistence.pagination.PaginationParams;
/**
*  generated: 29/09/2016 12:48:00
**/
public interface EmpresaService {

	Empresa get(Integer id);

	List<Empresa> all();
	
	Pager<Empresa> all(PaginationParams paginationParams);

	List<Empresa> filter(PaginationParams paginationParams);
	
	List<Empresa> search(String searchText);

	Empresa save(Empresa entity);

	Empresa update(Empresa entity);
    List<Empresa> last(LocalDateTime lastSyncDate);		

	Boolean delete(Integer id);
}
