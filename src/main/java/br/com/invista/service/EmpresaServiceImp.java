package br.com.invista.service;

import java.util.List;
import org.apache.log4j.Logger;
import javax.inject.Inject;
import javax.inject.Named;
import java.util.ArrayList;
import org.springframework.transaction.annotation.Transactional;
import org.joda.time.LocalDateTime;


import br.com.invista.model.Empresa;
import br.com.invista.persistence.DaoEmpresa;

import br.com.invista.core.persistence.pagination.Pager;
import br.com.invista.core.persistence.pagination.Pagination;
import br.com.invista.core.persistence.pagination.PaginationParams;
import br.com.invista.core.utils.DateUtil;
import br.com.invista.core.utils.Util;

/**
*  generated: 29/09/2016 12:48:00
**/

@Named
@Transactional
public class EmpresaServiceImp implements EmpresaService {

	private static final Logger LOGGER = Logger.getLogger(EmpresaServiceImp.class);
	
	@Inject
	DaoEmpresa daoEmpresa;

	@Override
	public Empresa get(Integer id) {
		return daoEmpresa.find(id);
	}
	

	@Override
	public Pager<Empresa> all(PaginationParams paginationParams) {
		Pagination<Empresa> pagination = daoEmpresa.getAll(paginationParams);
		return new Pager<Empresa>(pagination.getResults(), 0, pagination.getTotalRecords());
	}
	
	
	@Override
	public List<Empresa> filter(PaginationParams paginationParams) {
		List<Empresa> list = daoEmpresa.filter(paginationParams);
		return list;
	}
	
	@Override
	public List<Empresa> all() {
		return daoEmpresa.getAll();
	}

	@Override
	public List<Empresa> search(String description) {
		return new ArrayList<Empresa>();
	}
	
	public List<Empresa> last(LocalDateTime lastSyncDate){
		return daoEmpresa.last(lastSyncDate);
	}
			
	@Override
	public Empresa save(Empresa entity) {
		return daoEmpresa.save(entity);
	}

	@Override
	public Empresa update(Empresa entity) {
		return daoEmpresa.save(entity);
	}

	@Override
	public Boolean delete(Integer id) {
		return daoEmpresa.delete(id);
	}


}
