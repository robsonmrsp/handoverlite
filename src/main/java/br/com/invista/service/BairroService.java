package br.com.invista.service;

import java.util.List;
import org.joda.time.LocalDateTime;
import br.com.invista.model.Bairro;
import br.com.invista.core.persistence.pagination.Pager;
import br.com.invista.core.persistence.pagination.PaginationParams;
/**
*  generated: 29/09/2016 12:48:00
**/
public interface BairroService {

	Bairro get(Integer id);

	List<Bairro> all();
	
	Pager<Bairro> all(PaginationParams paginationParams);

	List<Bairro> filter(PaginationParams paginationParams);
	
	List<Bairro> search(String searchText);

	Bairro save(Bairro entity);

	Bairro update(Bairro entity);
    List<Bairro> last(LocalDateTime lastSyncDate);		

	Boolean delete(Integer id);
}
