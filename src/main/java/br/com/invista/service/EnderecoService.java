package br.com.invista.service;

import java.util.List;
import org.joda.time.LocalDateTime;
import br.com.invista.model.Endereco;
import br.com.invista.core.persistence.pagination.Pager;
import br.com.invista.core.persistence.pagination.PaginationParams;
/**
*  generated: 29/09/2016 12:48:00
**/
public interface EnderecoService {

	Endereco get(Integer id);

	List<Endereco> all();
	
	Pager<Endereco> all(PaginationParams paginationParams);

	List<Endereco> filter(PaginationParams paginationParams);
	
	List<Endereco> search(String searchText);

	Endereco save(Endereco entity);

	Endereco update(Endereco entity);
    List<Endereco> last(LocalDateTime lastSyncDate);		

	Boolean delete(Integer id);
}
