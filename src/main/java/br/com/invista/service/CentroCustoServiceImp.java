package br.com.invista.service;

import java.util.List;
import org.apache.log4j.Logger;
import javax.inject.Inject;
import javax.inject.Named;
import java.util.ArrayList;
import org.springframework.transaction.annotation.Transactional;
import org.joda.time.LocalDateTime;


import br.com.invista.model.CentroCusto;
import br.com.invista.persistence.DaoCentroCusto;

import br.com.invista.core.persistence.pagination.Pager;
import br.com.invista.core.persistence.pagination.Pagination;
import br.com.invista.core.persistence.pagination.PaginationParams;
import br.com.invista.core.utils.DateUtil;
import br.com.invista.core.utils.Util;

/**
*  generated: 29/09/2016 12:48:00
**/

@Named
@Transactional
public class CentroCustoServiceImp implements CentroCustoService {

	private static final Logger LOGGER = Logger.getLogger(CentroCustoServiceImp.class);
	
	@Inject
	DaoCentroCusto daoCentroCusto;

	@Override
	public CentroCusto get(Integer id) {
		return daoCentroCusto.find(id);
	}
	

	@Override
	public Pager<CentroCusto> all(PaginationParams paginationParams) {
		Pagination<CentroCusto> pagination = daoCentroCusto.getAll(paginationParams);
		return new Pager<CentroCusto>(pagination.getResults(), 0, pagination.getTotalRecords());
	}
	
	
	@Override
	public List<CentroCusto> filter(PaginationParams paginationParams) {
		List<CentroCusto> list = daoCentroCusto.filter(paginationParams);
		return list;
	}
	
	@Override
	public List<CentroCusto> all() {
		return daoCentroCusto.getAll();
	}

	@Override
	public List<CentroCusto> search(String description) {
		return new ArrayList<CentroCusto>();
	}
	
	public List<CentroCusto> last(LocalDateTime lastSyncDate){
		return daoCentroCusto.last(lastSyncDate);
	}
			
	@Override
	public CentroCusto save(CentroCusto entity) {
		return daoCentroCusto.save(entity);
	}

	@Override
	public CentroCusto update(CentroCusto entity) {
		return daoCentroCusto.save(entity);
	}

	@Override
	public Boolean delete(Integer id) {
		return daoCentroCusto.delete(id);
	}


}
