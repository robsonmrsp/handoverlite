package br.com.invista.service;

import java.util.List;
import org.joda.time.LocalDateTime;
import br.com.invista.model.Funcionario;
import br.com.invista.core.persistence.pagination.Pager;
import br.com.invista.core.persistence.pagination.PaginationParams;
/**
*  generated: 29/09/2016 12:48:00
**/
public interface FuncionarioService {

	Funcionario get(Integer id);

	List<Funcionario> all();
	
	Pager<Funcionario> all(PaginationParams paginationParams);

	List<Funcionario> filter(PaginationParams paginationParams);
	
	List<Funcionario> search(String searchText);

	Funcionario save(Funcionario entity);

	Funcionario update(Funcionario entity);
    List<Funcionario> last(LocalDateTime lastSyncDate);		

	Boolean delete(Integer id);
}
