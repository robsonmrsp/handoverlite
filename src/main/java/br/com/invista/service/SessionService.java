package br.com.invista.service;

import java.util.List;
import org.joda.time.LocalDateTime;
import br.com.invista.model.Session;
import br.com.invista.core.persistence.pagination.Pager;
import br.com.invista.core.persistence.pagination.PaginationParams;
/**
*  generated: 29/09/2016 12:48:00
**/
public interface SessionService {

	Session get(Integer id);

	List<Session> all();
	
	Pager<Session> all(PaginationParams paginationParams);

	List<Session> filter(PaginationParams paginationParams);
	
	List<Session> search(String searchText);

	Session save(Session entity);

	Session update(Session entity);
    List<Session> last(LocalDateTime lastSyncDate);		

	Boolean delete(Integer id);
}
