package br.com.invista.service;

import java.util.List;
import org.apache.log4j.Logger;
import javax.inject.Inject;
import javax.inject.Named;
import java.util.ArrayList;
import org.springframework.transaction.annotation.Transactional;
import org.joda.time.LocalDateTime;


import br.com.invista.model.Operation;
import br.com.invista.persistence.DaoOperation;

import br.com.invista.core.persistence.pagination.Pager;
import br.com.invista.core.persistence.pagination.Pagination;
import br.com.invista.core.persistence.pagination.PaginationParams;
import br.com.invista.core.utils.DateUtil;
import br.com.invista.core.utils.Util;

/**
*  generated: 29/09/2016 12:48:00
**/

@Named
@Transactional
public class OperationServiceImp implements OperationService {

	private static final Logger LOGGER = Logger.getLogger(OperationServiceImp.class);
	
	@Inject
	DaoOperation daoOperation;

	@Override
	public Operation get(Integer id) {
		return daoOperation.find(id);
	}
	

	@Override
	public Pager<Operation> all(PaginationParams paginationParams) {
		Pagination<Operation> pagination = daoOperation.getAll(paginationParams);
		return new Pager<Operation>(pagination.getResults(), 0, pagination.getTotalRecords());
	}
	
	
	@Override
	public List<Operation> filter(PaginationParams paginationParams) {
		List<Operation> list = daoOperation.filter(paginationParams);
		return list;
	}
	
	@Override
	public List<Operation> all() {
		return daoOperation.getAll();
	}

	@Override
	public List<Operation> search(String description) {
		return new ArrayList<Operation>();
	}
	
	public List<Operation> last(LocalDateTime lastSyncDate){
		return daoOperation.last(lastSyncDate);
	}
			
	@Override
	public Operation save(Operation entity) {
		return daoOperation.save(entity);
	}

	@Override
	public Operation update(Operation entity) {
		return daoOperation.save(entity);
	}

	@Override
	public Boolean delete(Integer id) {
		return daoOperation.delete(id);
	}


}
