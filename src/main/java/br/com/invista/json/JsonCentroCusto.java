package br.com.invista.json;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;

import br.com.invista.core.json.SyncOperation;
import br.com.invista.core.serialization.CustomSyncObjectIdDeserializer;
import br.com.invista.core.serialization.CustomDoubleDeserializer;

/**
*  generated: 29/09/2016 12:48:00
**/
@JsonIgnoreProperties(ignoreUnknown = true)
public class JsonCentroCusto implements Serializable {
	private static final long serialVersionUID = 1L;

	@JsonDeserialize(using = CustomSyncObjectIdDeserializer.class)
	private Integer id;
	private SyncOperation syncOperation;
	private String descricao;
	@JsonDeserialize(using = CustomDoubleDeserializer.class)
	private Double valorLimite;
	private String observacao;
	private String obseracaoTaxista;
	private Boolean inativo;
	private ArrayList<JsonFuncionario> funcionarios = new ArrayList<JsonFuncionario>();		
	private JsonEmpresa empresa;		
	
	public  JsonCentroCusto() {
		
	}
	
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}
	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}
	public Double getValorLimite() {
		return valorLimite;
	}

	public void setValorLimite(Double valorLimite) {
		this.valorLimite = valorLimite;
	}
	public String getObservacao() {
		return observacao;
	}

	public void setObservacao(String observacao) {
		this.observacao = observacao;
	}
	public String getObseracaoTaxista() {
		return obseracaoTaxista;
	}

	public void setObseracaoTaxista(String obseracaoTaxista) {
		this.obseracaoTaxista = obseracaoTaxista;
	}
	public Boolean getInativo() {
		return inativo;
	}

	public void setInativo(Boolean inativo) {
		this.inativo = inativo;
	}
	
	public ArrayList<JsonFuncionario> getFuncionarios() {
		return funcionarios;
	}
	
	public void setFuncionarios(ArrayList<JsonFuncionario> funcionario) {
		this.funcionarios = funcionario;
	}

	public JsonEmpresa getEmpresa() {
		return empresa;
	}
	
	public void setEmpresa(JsonEmpresa empresa) {
		this.empresa = empresa;
	}
	public SyncOperation getSyncOperation (){
		if(syncOperation == null){
			this.syncOperation = SyncOperation.NONE;
		}
		return syncOperation;
	}
	
	public void setSyncOperation (SyncOperation  syncOperation){
		this.syncOperation = syncOperation;
	}
	
}