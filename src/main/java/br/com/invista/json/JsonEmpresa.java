package br.com.invista.json;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;

import br.com.invista.core.json.SyncOperation;
import br.com.invista.core.serialization.CustomSyncObjectIdDeserializer;
import br.com.invista.core.serialization.CustomDoubleDeserializer;

/**
*  generated: 29/09/2016 12:48:00
**/
@JsonIgnoreProperties(ignoreUnknown = true)
public class JsonEmpresa implements Serializable {
	private static final long serialVersionUID = 1L;

	@JsonDeserialize(using = CustomSyncObjectIdDeserializer.class)
	private Integer id;
	private SyncOperation syncOperation;
	private String cnpj;
	private String numeroContrato;
	private String razaoSocial;
	private String nomeFantasia;
	private Boolean utilizaVoucher;
	private Boolean utilizaTicket;
	private String observacaoTaxista;
	private String observacaoCentral;
	@JsonDeserialize(using = CustomDoubleDeserializer.class)
	private Double percursoDesconto;
	private String inscricaoMunicipal;
	private String inscricaoEstadual;
	private Boolean nf;
	@JsonDeserialize(using = CustomDoubleDeserializer.class)
	private Double percentualIss;
	@JsonDeserialize(using = CustomDoubleDeserializer.class)
	private Double percentualIrf;
	@JsonDeserialize(using = CustomDoubleDeserializer.class)
	private Double percentualInss;
	private Integer diaVencimento;
	@JsonDeserialize(using = CustomDoubleDeserializer.class)
	private Double percentualMotorista;
	private Integer banco;
	private String agencia;
	private String conta;
	private String email;
	private ArrayList<JsonCentroCusto> centroCustos = new ArrayList<JsonCentroCusto>();		
	private ArrayList<JsonFuncionario> funcionarios = new ArrayList<JsonFuncionario>();		
	
	public  JsonEmpresa() {
		
	}
	
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}
	public String getCnpj() {
		return cnpj;
	}

	public void setCnpj(String cnpj) {
		this.cnpj = cnpj;
	}
	public String getNumeroContrato() {
		return numeroContrato;
	}

	public void setNumeroContrato(String numeroContrato) {
		this.numeroContrato = numeroContrato;
	}
	public String getRazaoSocial() {
		return razaoSocial;
	}

	public void setRazaoSocial(String razaoSocial) {
		this.razaoSocial = razaoSocial;
	}
	public String getNomeFantasia() {
		return nomeFantasia;
	}

	public void setNomeFantasia(String nomeFantasia) {
		this.nomeFantasia = nomeFantasia;
	}
	public Boolean getUtilizaVoucher() {
		return utilizaVoucher;
	}

	public void setUtilizaVoucher(Boolean utilizaVoucher) {
		this.utilizaVoucher = utilizaVoucher;
	}
	public Boolean getUtilizaTicket() {
		return utilizaTicket;
	}

	public void setUtilizaTicket(Boolean utilizaTicket) {
		this.utilizaTicket = utilizaTicket;
	}
	public String getObservacaoTaxista() {
		return observacaoTaxista;
	}

	public void setObservacaoTaxista(String observacaoTaxista) {
		this.observacaoTaxista = observacaoTaxista;
	}
	public String getObservacaoCentral() {
		return observacaoCentral;
	}

	public void setObservacaoCentral(String observacaoCentral) {
		this.observacaoCentral = observacaoCentral;
	}
	public Double getPercursoDesconto() {
		return percursoDesconto;
	}

	public void setPercursoDesconto(Double percursoDesconto) {
		this.percursoDesconto = percursoDesconto;
	}
	public String getInscricaoMunicipal() {
		return inscricaoMunicipal;
	}

	public void setInscricaoMunicipal(String inscricaoMunicipal) {
		this.inscricaoMunicipal = inscricaoMunicipal;
	}
	public String getInscricaoEstadual() {
		return inscricaoEstadual;
	}

	public void setInscricaoEstadual(String inscricaoEstadual) {
		this.inscricaoEstadual = inscricaoEstadual;
	}
	public Boolean getNf() {
		return nf;
	}

	public void setNf(Boolean nf) {
		this.nf = nf;
	}
	public Double getPercentualIss() {
		return percentualIss;
	}

	public void setPercentualIss(Double percentualIss) {
		this.percentualIss = percentualIss;
	}
	public Double getPercentualIrf() {
		return percentualIrf;
	}

	public void setPercentualIrf(Double percentualIrf) {
		this.percentualIrf = percentualIrf;
	}
	public Double getPercentualInss() {
		return percentualInss;
	}

	public void setPercentualInss(Double percentualInss) {
		this.percentualInss = percentualInss;
	}
	public Integer getDiaVencimento() {
		return diaVencimento;
	}

	public void setDiaVencimento(Integer diaVencimento) {
		this.diaVencimento = diaVencimento;
	}
	public Double getPercentualMotorista() {
		return percentualMotorista;
	}

	public void setPercentualMotorista(Double percentualMotorista) {
		this.percentualMotorista = percentualMotorista;
	}
	public Integer getBanco() {
		return banco;
	}

	public void setBanco(Integer banco) {
		this.banco = banco;
	}
	public String getAgencia() {
		return agencia;
	}

	public void setAgencia(String agencia) {
		this.agencia = agencia;
	}
	public String getConta() {
		return conta;
	}

	public void setConta(String conta) {
		this.conta = conta;
	}
	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}
	
	public ArrayList<JsonCentroCusto> getCentroCustos() {
		return centroCustos;
	}
	
	public void setCentroCustos(ArrayList<JsonCentroCusto> centroCusto) {
		this.centroCustos = centroCusto;
	}

	public ArrayList<JsonFuncionario> getFuncionarios() {
		return funcionarios;
	}
	
	public void setFuncionarios(ArrayList<JsonFuncionario> funcionario) {
		this.funcionarios = funcionario;
	}

	public SyncOperation getSyncOperation (){
		if(syncOperation == null){
			this.syncOperation = SyncOperation.NONE;
		}
		return syncOperation;
	}
	
	public void setSyncOperation (SyncOperation  syncOperation){
		this.syncOperation = syncOperation;
	}
	
}