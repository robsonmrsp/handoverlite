package br.com.invista.json;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;

import br.com.invista.core.json.SyncOperation;
import br.com.invista.core.serialization.CustomSyncObjectIdDeserializer;
import br.com.invista.core.serialization.CustomDoubleDeserializer;

/**
*  generated: 29/09/2016 12:48:00
**/
@JsonIgnoreProperties(ignoreUnknown = true)
public class JsonFuncionario implements Serializable {
	private static final long serialVersionUID = 1L;

	@JsonDeserialize(using = CustomSyncObjectIdDeserializer.class)
	private Integer id;
	private SyncOperation syncOperation;
	private String nome;
	private String email;
	private String telefone;
	@JsonDeserialize(using = CustomDoubleDeserializer.class)
	private Double limiteMensal;
	private String cpf;
	private String matricula;
	private String statusFuncionario;
	private Boolean autorizadoTicket;
	private String observacao;
	private JsonEmpresa empresa;		
	private JsonCentroCusto centroCusto;		
	
	public  JsonFuncionario() {
		
	}
	
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}
	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}
	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}
	public String getTelefone() {
		return telefone;
	}

	public void setTelefone(String telefone) {
		this.telefone = telefone;
	}
	public Double getLimiteMensal() {
		return limiteMensal;
	}

	public void setLimiteMensal(Double limiteMensal) {
		this.limiteMensal = limiteMensal;
	}
	public String getCpf() {
		return cpf;
	}

	public void setCpf(String cpf) {
		this.cpf = cpf;
	}
	public String getMatricula() {
		return matricula;
	}

	public void setMatricula(String matricula) {
		this.matricula = matricula;
	}
	public String getStatusFuncionario() {
		return statusFuncionario;
	}

	public void setStatusFuncionario(String statusFuncionario) {
		this.statusFuncionario = statusFuncionario;
	}
	public Boolean getAutorizadoTicket() {
		return autorizadoTicket;
	}

	public void setAutorizadoTicket(Boolean autorizadoTicket) {
		this.autorizadoTicket = autorizadoTicket;
	}
	public String getObservacao() {
		return observacao;
	}

	public void setObservacao(String observacao) {
		this.observacao = observacao;
	}
	
	public JsonEmpresa getEmpresa() {
		return empresa;
	}
	
	public void setEmpresa(JsonEmpresa empresa) {
		this.empresa = empresa;
	}
	public JsonCentroCusto getCentroCusto() {
		return centroCusto;
	}
	
	public void setCentroCusto(JsonCentroCusto centroCusto) {
		this.centroCusto = centroCusto;
	}
	public SyncOperation getSyncOperation (){
		if(syncOperation == null){
			this.syncOperation = SyncOperation.NONE;
		}
		return syncOperation;
	}
	
	public void setSyncOperation (SyncOperation  syncOperation){
		this.syncOperation = syncOperation;
	}
	
}