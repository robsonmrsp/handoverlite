package br.com.invista.persistence;

import javax.inject.Named;
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.hibernate.Criteria;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Restrictions;

import br.com.invista.model.Funcionario;
import br.com.invista.core.persistence.AccessibleHibernateDao;
import br.com.invista.model.filter.FilterFuncionario;
import br.com.invista.core.persistence.pagination.Pagination;
import br.com.invista.core.persistence.pagination.PaginationParams;
import br.com.invista.core.persistence.pagination.Paginator;

import br.com.invista.model.Funcionario;
/**
*  generated: 29/09/2016 12:48:00
**/

@Named
@SuppressWarnings("rawtypes")
public class DaoFuncionario extends AccessibleHibernateDao<Funcionario> {
	private static final Logger LOGGER = Logger.getLogger(DaoFuncionario.class);

	public DaoFuncionario() {
		super(Funcionario.class);
	}

	@Override
	public Pagination<Funcionario> getAll(PaginationParams paginationParams) {
		FilterFuncionario filterFuncionario = (FilterFuncionario) paginationParams.getFilter();
		Criteria searchCriteria = criteria();
		Criteria countCriteria = criteria();
		if (filterFuncionario.getNome() != null) {
			searchCriteria.add(Restrictions.ilike("nome", filterFuncionario.getNome(), MatchMode.ANYWHERE));
			countCriteria.add(Restrictions.ilike("nome", filterFuncionario.getNome(), MatchMode.ANYWHERE));
		}
		if (filterFuncionario.getEmail() != null) {
			searchCriteria.add(Restrictions.ilike("email", filterFuncionario.getEmail(), MatchMode.ANYWHERE));
			countCriteria.add(Restrictions.ilike("email", filterFuncionario.getEmail(), MatchMode.ANYWHERE));
		}
		if (filterFuncionario.getTelefone() != null) {
			searchCriteria.add(Restrictions.ilike("telefone", filterFuncionario.getTelefone(), MatchMode.ANYWHERE));
			countCriteria.add(Restrictions.ilike("telefone", filterFuncionario.getTelefone(), MatchMode.ANYWHERE));
		}
		if (filterFuncionario.getLimiteMensal() != null) {
			searchCriteria.add(Restrictions.eq("limiteMensal", filterFuncionario.getLimiteMensal()));
			countCriteria.add(Restrictions.eq("limiteMensal", filterFuncionario.getLimiteMensal()));
		}				
		if (filterFuncionario.getCpf() != null) {
			searchCriteria.add(Restrictions.ilike("cpf", filterFuncionario.getCpf(), MatchMode.ANYWHERE));
			countCriteria.add(Restrictions.ilike("cpf", filterFuncionario.getCpf(), MatchMode.ANYWHERE));
		}
		if (filterFuncionario.getMatricula() != null) {
			searchCriteria.add(Restrictions.ilike("matricula", filterFuncionario.getMatricula(), MatchMode.ANYWHERE));
			countCriteria.add(Restrictions.ilike("matricula", filterFuncionario.getMatricula(), MatchMode.ANYWHERE));
		}
		if (filterFuncionario.getStatusFuncionario() != null) {
			searchCriteria.add(Restrictions.ilike("statusFuncionario", filterFuncionario.getStatusFuncionario(), MatchMode.ANYWHERE));
			countCriteria.add(Restrictions.ilike("statusFuncionario", filterFuncionario.getStatusFuncionario(), MatchMode.ANYWHERE));
		}
		if (filterFuncionario.getAutorizadoTicket() != null) {
			searchCriteria.add(Restrictions.eq("autorizadoTicket", filterFuncionario.getAutorizadoTicket()));
			countCriteria.add(Restrictions.eq("autorizadoTicket", filterFuncionario.getAutorizadoTicket()));
		}				
		if (filterFuncionario.getObservacao() != null) {
			searchCriteria.add(Restrictions.ilike("observacao", filterFuncionario.getObservacao(), MatchMode.ANYWHERE));
			countCriteria.add(Restrictions.ilike("observacao", filterFuncionario.getObservacao(), MatchMode.ANYWHERE));
		}
		if (filterFuncionario.getEmpresa() != null) {
			searchCriteria.createAlias("empresa", "empresa_");
			countCriteria.createAlias("empresa", "empresa_");
			searchCriteria.add(Restrictions.eq("empresa_.id", filterFuncionario.getEmpresa()));
			countCriteria.add(Restrictions.eq("empresa_.id", filterFuncionario.getEmpresa()));
		}
		if (filterFuncionario.getCentroCusto() != null) {
			searchCriteria.createAlias("centroCusto", "centroCusto_");
			countCriteria.createAlias("centroCusto", "centroCusto_");
			searchCriteria.add(Restrictions.eq("centroCusto_.id", filterFuncionario.getCentroCusto()));
			countCriteria.add(Restrictions.eq("centroCusto_.id", filterFuncionario.getCentroCusto()));
		}

		return new Paginator<Funcionario>(searchCriteria, countCriteria).paginate(paginationParams);
	}
	
	public List<Funcionario> filter(PaginationParams paginationParams) {
		List<Funcionario> list = new ArrayList<Funcionario>();
		FilterFuncionario filterFuncionario = (FilterFuncionario) paginationParams.getFilter();
		Criteria searchCriteria = criteria();
		if (filterFuncionario.getNome() != null) {
			searchCriteria.add(Restrictions.eq("nome", filterFuncionario.getNome()));
		}
		if (filterFuncionario.getEmail() != null) {
			searchCriteria.add(Restrictions.eq("email", filterFuncionario.getEmail()));
		}
		if (filterFuncionario.getTelefone() != null) {
			searchCriteria.add(Restrictions.eq("telefone", filterFuncionario.getTelefone()));
		}
		if (filterFuncionario.getLimiteMensal() != null) {
			searchCriteria.add(Restrictions.eq("limiteMensal", filterFuncionario.getLimiteMensal()));
		}
		if (filterFuncionario.getCpf() != null) {
			searchCriteria.add(Restrictions.eq("cpf", filterFuncionario.getCpf()));
		}
		if (filterFuncionario.getMatricula() != null) {
			searchCriteria.add(Restrictions.eq("matricula", filterFuncionario.getMatricula()));
		}
		if (filterFuncionario.getStatusFuncionario() != null) {
			searchCriteria.add(Restrictions.eq("statusFuncionario", filterFuncionario.getStatusFuncionario()));
		}
		if (filterFuncionario.getAutorizadoTicket() != null) {
			searchCriteria.add(Restrictions.eq("autorizadoTicket", filterFuncionario.getAutorizadoTicket()));
		}
		if (filterFuncionario.getObservacao() != null) {
			searchCriteria.add(Restrictions.eq("observacao", filterFuncionario.getObservacao()));
		}
		if (filterFuncionario.getEmpresa() != null) {
			searchCriteria.createAlias("empresa", "empresa_");
			searchCriteria.add(Restrictions.eq("empresa_.id", filterFuncionario.getEmpresa()));
		}
		if (filterFuncionario.getCentroCusto() != null) {
			searchCriteria.createAlias("centroCusto", "centroCusto_");
			searchCriteria.add(Restrictions.eq("centroCusto_.id", filterFuncionario.getCentroCusto()));
		}

		list.addAll(searchCriteria.list());
		return list;
	}
}
