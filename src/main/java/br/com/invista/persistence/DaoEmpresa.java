package br.com.invista.persistence;

import javax.inject.Named;
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.hibernate.Criteria;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Restrictions;

import br.com.invista.model.Empresa;
import br.com.invista.core.persistence.AccessibleHibernateDao;
import br.com.invista.model.filter.FilterEmpresa;
import br.com.invista.core.persistence.pagination.Pagination;
import br.com.invista.core.persistence.pagination.PaginationParams;
import br.com.invista.core.persistence.pagination.Paginator;

import br.com.invista.model.Empresa;
/**
*  generated: 29/09/2016 12:48:00
**/

@Named
@SuppressWarnings("rawtypes")
public class DaoEmpresa extends AccessibleHibernateDao<Empresa> {
	private static final Logger LOGGER = Logger.getLogger(DaoEmpresa.class);

	public DaoEmpresa() {
		super(Empresa.class);
	}

	@Override
	public Pagination<Empresa> getAll(PaginationParams paginationParams) {
		FilterEmpresa filterEmpresa = (FilterEmpresa) paginationParams.getFilter();
		Criteria searchCriteria = criteria();
		Criteria countCriteria = criteria();
		if (filterEmpresa.getCnpj() != null) {
			searchCriteria.add(Restrictions.ilike("cnpj", filterEmpresa.getCnpj(), MatchMode.ANYWHERE));
			countCriteria.add(Restrictions.ilike("cnpj", filterEmpresa.getCnpj(), MatchMode.ANYWHERE));
		}
		if (filterEmpresa.getNumeroContrato() != null) {
			searchCriteria.add(Restrictions.ilike("numeroContrato", filterEmpresa.getNumeroContrato(), MatchMode.ANYWHERE));
			countCriteria.add(Restrictions.ilike("numeroContrato", filterEmpresa.getNumeroContrato(), MatchMode.ANYWHERE));
		}
		if (filterEmpresa.getRazaoSocial() != null) {
			searchCriteria.add(Restrictions.ilike("razaoSocial", filterEmpresa.getRazaoSocial(), MatchMode.ANYWHERE));
			countCriteria.add(Restrictions.ilike("razaoSocial", filterEmpresa.getRazaoSocial(), MatchMode.ANYWHERE));
		}
		if (filterEmpresa.getNomeFantasia() != null) {
			searchCriteria.add(Restrictions.ilike("nomeFantasia", filterEmpresa.getNomeFantasia(), MatchMode.ANYWHERE));
			countCriteria.add(Restrictions.ilike("nomeFantasia", filterEmpresa.getNomeFantasia(), MatchMode.ANYWHERE));
		}
		if (filterEmpresa.getUtilizaVoucher() != null) {
			searchCriteria.add(Restrictions.eq("utilizaVoucher", filterEmpresa.getUtilizaVoucher()));
			countCriteria.add(Restrictions.eq("utilizaVoucher", filterEmpresa.getUtilizaVoucher()));
		}				
		if (filterEmpresa.getUtilizaTicket() != null) {
			searchCriteria.add(Restrictions.eq("utilizaTicket", filterEmpresa.getUtilizaTicket()));
			countCriteria.add(Restrictions.eq("utilizaTicket", filterEmpresa.getUtilizaTicket()));
		}				
		if (filterEmpresa.getObservacaoTaxista() != null) {
			searchCriteria.add(Restrictions.ilike("observacaoTaxista", filterEmpresa.getObservacaoTaxista(), MatchMode.ANYWHERE));
			countCriteria.add(Restrictions.ilike("observacaoTaxista", filterEmpresa.getObservacaoTaxista(), MatchMode.ANYWHERE));
		}
		if (filterEmpresa.getObservacaoCentral() != null) {
			searchCriteria.add(Restrictions.ilike("observacaoCentral", filterEmpresa.getObservacaoCentral(), MatchMode.ANYWHERE));
			countCriteria.add(Restrictions.ilike("observacaoCentral", filterEmpresa.getObservacaoCentral(), MatchMode.ANYWHERE));
		}
		if (filterEmpresa.getPercursoDesconto() != null) {
			searchCriteria.add(Restrictions.eq("percursoDesconto", filterEmpresa.getPercursoDesconto()));
			countCriteria.add(Restrictions.eq("percursoDesconto", filterEmpresa.getPercursoDesconto()));
		}				
		if (filterEmpresa.getInscricaoMunicipal() != null) {
			searchCriteria.add(Restrictions.ilike("inscricaoMunicipal", filterEmpresa.getInscricaoMunicipal(), MatchMode.ANYWHERE));
			countCriteria.add(Restrictions.ilike("inscricaoMunicipal", filterEmpresa.getInscricaoMunicipal(), MatchMode.ANYWHERE));
		}
		if (filterEmpresa.getInscricaoEstadual() != null) {
			searchCriteria.add(Restrictions.ilike("inscricaoEstadual", filterEmpresa.getInscricaoEstadual(), MatchMode.ANYWHERE));
			countCriteria.add(Restrictions.ilike("inscricaoEstadual", filterEmpresa.getInscricaoEstadual(), MatchMode.ANYWHERE));
		}
		if (filterEmpresa.getNf() != null) {
			searchCriteria.add(Restrictions.eq("nf", filterEmpresa.getNf()));
			countCriteria.add(Restrictions.eq("nf", filterEmpresa.getNf()));
		}				
		if (filterEmpresa.getPercentualIss() != null) {
			searchCriteria.add(Restrictions.eq("percentualIss", filterEmpresa.getPercentualIss()));
			countCriteria.add(Restrictions.eq("percentualIss", filterEmpresa.getPercentualIss()));
		}				
		if (filterEmpresa.getPercentualIrf() != null) {
			searchCriteria.add(Restrictions.eq("percentualIrf", filterEmpresa.getPercentualIrf()));
			countCriteria.add(Restrictions.eq("percentualIrf", filterEmpresa.getPercentualIrf()));
		}				
		if (filterEmpresa.getPercentualInss() != null) {
			searchCriteria.add(Restrictions.eq("percentualInss", filterEmpresa.getPercentualInss()));
			countCriteria.add(Restrictions.eq("percentualInss", filterEmpresa.getPercentualInss()));
		}				
		if (filterEmpresa.getDiaVencimento() != null) {
			searchCriteria.add(Restrictions.eq("diaVencimento", filterEmpresa.getDiaVencimento()));
			countCriteria.add(Restrictions.eq("diaVencimento", filterEmpresa.getDiaVencimento()));
		}				
		if (filterEmpresa.getPercentualMotorista() != null) {
			searchCriteria.add(Restrictions.eq("percentualMotorista", filterEmpresa.getPercentualMotorista()));
			countCriteria.add(Restrictions.eq("percentualMotorista", filterEmpresa.getPercentualMotorista()));
		}				
		if (filterEmpresa.getBanco() != null) {
			searchCriteria.add(Restrictions.eq("banco", filterEmpresa.getBanco()));
			countCriteria.add(Restrictions.eq("banco", filterEmpresa.getBanco()));
		}				
		if (filterEmpresa.getAgencia() != null) {
			searchCriteria.add(Restrictions.ilike("agencia", filterEmpresa.getAgencia(), MatchMode.ANYWHERE));
			countCriteria.add(Restrictions.ilike("agencia", filterEmpresa.getAgencia(), MatchMode.ANYWHERE));
		}
		if (filterEmpresa.getConta() != null) {
			searchCriteria.add(Restrictions.ilike("conta", filterEmpresa.getConta(), MatchMode.ANYWHERE));
			countCriteria.add(Restrictions.ilike("conta", filterEmpresa.getConta(), MatchMode.ANYWHERE));
		}
		if (filterEmpresa.getEmail() != null) {
			searchCriteria.add(Restrictions.ilike("email", filterEmpresa.getEmail(), MatchMode.ANYWHERE));
			countCriteria.add(Restrictions.ilike("email", filterEmpresa.getEmail(), MatchMode.ANYWHERE));
		}

		return new Paginator<Empresa>(searchCriteria, countCriteria).paginate(paginationParams);
	}
	
	public List<Empresa> filter(PaginationParams paginationParams) {
		List<Empresa> list = new ArrayList<Empresa>();
		FilterEmpresa filterEmpresa = (FilterEmpresa) paginationParams.getFilter();
		Criteria searchCriteria = criteria();
		if (filterEmpresa.getCnpj() != null) {
			searchCriteria.add(Restrictions.eq("cnpj", filterEmpresa.getCnpj()));
		}
		if (filterEmpresa.getNumeroContrato() != null) {
			searchCriteria.add(Restrictions.eq("numeroContrato", filterEmpresa.getNumeroContrato()));
		}
		if (filterEmpresa.getRazaoSocial() != null) {
			searchCriteria.add(Restrictions.eq("razaoSocial", filterEmpresa.getRazaoSocial()));
		}
		if (filterEmpresa.getNomeFantasia() != null) {
			searchCriteria.add(Restrictions.eq("nomeFantasia", filterEmpresa.getNomeFantasia()));
		}
		if (filterEmpresa.getUtilizaVoucher() != null) {
			searchCriteria.add(Restrictions.eq("utilizaVoucher", filterEmpresa.getUtilizaVoucher()));
		}
		if (filterEmpresa.getUtilizaTicket() != null) {
			searchCriteria.add(Restrictions.eq("utilizaTicket", filterEmpresa.getUtilizaTicket()));
		}
		if (filterEmpresa.getObservacaoTaxista() != null) {
			searchCriteria.add(Restrictions.eq("observacaoTaxista", filterEmpresa.getObservacaoTaxista()));
		}
		if (filterEmpresa.getObservacaoCentral() != null) {
			searchCriteria.add(Restrictions.eq("observacaoCentral", filterEmpresa.getObservacaoCentral()));
		}
		if (filterEmpresa.getPercursoDesconto() != null) {
			searchCriteria.add(Restrictions.eq("percursoDesconto", filterEmpresa.getPercursoDesconto()));
		}
		if (filterEmpresa.getInscricaoMunicipal() != null) {
			searchCriteria.add(Restrictions.eq("inscricaoMunicipal", filterEmpresa.getInscricaoMunicipal()));
		}
		if (filterEmpresa.getInscricaoEstadual() != null) {
			searchCriteria.add(Restrictions.eq("inscricaoEstadual", filterEmpresa.getInscricaoEstadual()));
		}
		if (filterEmpresa.getNf() != null) {
			searchCriteria.add(Restrictions.eq("nf", filterEmpresa.getNf()));
		}
		if (filterEmpresa.getPercentualIss() != null) {
			searchCriteria.add(Restrictions.eq("percentualIss", filterEmpresa.getPercentualIss()));
		}
		if (filterEmpresa.getPercentualIrf() != null) {
			searchCriteria.add(Restrictions.eq("percentualIrf", filterEmpresa.getPercentualIrf()));
		}
		if (filterEmpresa.getPercentualInss() != null) {
			searchCriteria.add(Restrictions.eq("percentualInss", filterEmpresa.getPercentualInss()));
		}
		if (filterEmpresa.getDiaVencimento() != null) {
			searchCriteria.add(Restrictions.eq("diaVencimento", filterEmpresa.getDiaVencimento()));
		}
		if (filterEmpresa.getPercentualMotorista() != null) {
			searchCriteria.add(Restrictions.eq("percentualMotorista", filterEmpresa.getPercentualMotorista()));
		}
		if (filterEmpresa.getBanco() != null) {
			searchCriteria.add(Restrictions.eq("banco", filterEmpresa.getBanco()));
		}
		if (filterEmpresa.getAgencia() != null) {
			searchCriteria.add(Restrictions.eq("agencia", filterEmpresa.getAgencia()));
		}
		if (filterEmpresa.getConta() != null) {
			searchCriteria.add(Restrictions.eq("conta", filterEmpresa.getConta()));
		}
		if (filterEmpresa.getEmail() != null) {
			searchCriteria.add(Restrictions.eq("email", filterEmpresa.getEmail()));
		}

		list.addAll(searchCriteria.list());
		return list;
	}
}
