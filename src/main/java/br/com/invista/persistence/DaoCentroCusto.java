package br.com.invista.persistence;

import javax.inject.Named;
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.hibernate.Criteria;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Restrictions;

import br.com.invista.model.CentroCusto;
import br.com.invista.core.persistence.AccessibleHibernateDao;
import br.com.invista.model.filter.FilterCentroCusto;
import br.com.invista.core.persistence.pagination.Pagination;
import br.com.invista.core.persistence.pagination.PaginationParams;
import br.com.invista.core.persistence.pagination.Paginator;

import br.com.invista.model.CentroCusto;
/**
*  generated: 29/09/2016 12:48:00
**/

@Named
@SuppressWarnings("rawtypes")
public class DaoCentroCusto extends AccessibleHibernateDao<CentroCusto> {
	private static final Logger LOGGER = Logger.getLogger(DaoCentroCusto.class);

	public DaoCentroCusto() {
		super(CentroCusto.class);
	}

	@Override
	public Pagination<CentroCusto> getAll(PaginationParams paginationParams) {
		FilterCentroCusto filterCentroCusto = (FilterCentroCusto) paginationParams.getFilter();
		Criteria searchCriteria = criteria();
		Criteria countCriteria = criteria();
		if (filterCentroCusto.getDescricao() != null) {
			searchCriteria.add(Restrictions.ilike("descricao", filterCentroCusto.getDescricao(), MatchMode.ANYWHERE));
			countCriteria.add(Restrictions.ilike("descricao", filterCentroCusto.getDescricao(), MatchMode.ANYWHERE));
		}
		if (filterCentroCusto.getValorLimite() != null) {
			searchCriteria.add(Restrictions.eq("valorLimite", filterCentroCusto.getValorLimite()));
			countCriteria.add(Restrictions.eq("valorLimite", filterCentroCusto.getValorLimite()));
		}				
		if (filterCentroCusto.getObservacao() != null) {
			searchCriteria.add(Restrictions.ilike("observacao", filterCentroCusto.getObservacao(), MatchMode.ANYWHERE));
			countCriteria.add(Restrictions.ilike("observacao", filterCentroCusto.getObservacao(), MatchMode.ANYWHERE));
		}
		if (filterCentroCusto.getObseracaoTaxista() != null) {
			searchCriteria.add(Restrictions.ilike("obseracaoTaxista", filterCentroCusto.getObseracaoTaxista(), MatchMode.ANYWHERE));
			countCriteria.add(Restrictions.ilike("obseracaoTaxista", filterCentroCusto.getObseracaoTaxista(), MatchMode.ANYWHERE));
		}
		if (filterCentroCusto.getInativo() != null) {
			searchCriteria.add(Restrictions.eq("inativo", filterCentroCusto.getInativo()));
			countCriteria.add(Restrictions.eq("inativo", filterCentroCusto.getInativo()));
		}				
		if (filterCentroCusto.getEmpresa() != null) {
			searchCriteria.createAlias("empresa", "empresa_");
			countCriteria.createAlias("empresa", "empresa_");
			searchCriteria.add(Restrictions.eq("empresa_.id", filterCentroCusto.getEmpresa()));
			countCriteria.add(Restrictions.eq("empresa_.id", filterCentroCusto.getEmpresa()));
		}

		return new Paginator<CentroCusto>(searchCriteria, countCriteria).paginate(paginationParams);
	}
	
	public List<CentroCusto> filter(PaginationParams paginationParams) {
		List<CentroCusto> list = new ArrayList<CentroCusto>();
		FilterCentroCusto filterCentroCusto = (FilterCentroCusto) paginationParams.getFilter();
		Criteria searchCriteria = criteria();
		if (filterCentroCusto.getDescricao() != null) {
			searchCriteria.add(Restrictions.eq("descricao", filterCentroCusto.getDescricao()));
		}
		if (filterCentroCusto.getValorLimite() != null) {
			searchCriteria.add(Restrictions.eq("valorLimite", filterCentroCusto.getValorLimite()));
		}
		if (filterCentroCusto.getObservacao() != null) {
			searchCriteria.add(Restrictions.eq("observacao", filterCentroCusto.getObservacao()));
		}
		if (filterCentroCusto.getObseracaoTaxista() != null) {
			searchCriteria.add(Restrictions.eq("obseracaoTaxista", filterCentroCusto.getObseracaoTaxista()));
		}
		if (filterCentroCusto.getInativo() != null) {
			searchCriteria.add(Restrictions.eq("inativo", filterCentroCusto.getInativo()));
		}
		if (filterCentroCusto.getEmpresa() != null) {
			searchCriteria.createAlias("empresa", "empresa_");
			searchCriteria.add(Restrictions.eq("empresa_.id", filterCentroCusto.getEmpresa()));
		}

		list.addAll(searchCriteria.list());
		return list;
	}
}
