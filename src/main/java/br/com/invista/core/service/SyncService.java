package br.com.invista.core.service;

import java.util.List;


import javax.inject.Inject;
import javax.inject.Named;

import br.com.invista.core.model.SyncInfo;
import br.com.invista.core.json.*;
import br.com.invista.core.model.*;
import br.com.invista.core.persistence.DaoSyncInfo;
import br.com.invista.core.json.DtoDataBase;

public interface SyncService {
		public DtoDataBase sync(DtoDataBase dataBase) ;
}
