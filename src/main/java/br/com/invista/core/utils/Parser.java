package br.com.invista.core.utils;

import java.util.ArrayList;

import java.util.List;

import org.joda.time.DateTime;
import org.joda.time.LocalDateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

import br.com.invista.json.JsonCentroCusto;
import br.com.invista.model.CentroCusto;
import br.com.invista.json.JsonEmpresa;
import br.com.invista.model.Empresa;
import br.com.invista.json.JsonFuncionario;
import br.com.invista.model.Funcionario;
import br.com.invista.json.JsonBairro;
import br.com.invista.model.Bairro;
import br.com.invista.json.JsonCep;
import br.com.invista.model.Cep;
import br.com.invista.json.JsonCidade;
import br.com.invista.model.Cidade;
import br.com.invista.json.JsonEndereco;
import br.com.invista.model.Endereco;
import br.com.invista.json.JsonEstado;
import br.com.invista.model.Estado;
import br.com.invista.json.JsonPais;
import br.com.invista.model.Pais;
import br.com.invista.json.JsonItem;
import br.com.invista.model.Item;
import br.com.invista.json.JsonItemType;
import br.com.invista.model.ItemType;
import br.com.invista.json.JsonOperation;
import br.com.invista.model.Operation;
import br.com.invista.json.JsonPermission;
import br.com.invista.model.Permission;
import br.com.invista.json.JsonRole;
import br.com.invista.model.Role;
import br.com.invista.json.JsonSession;
import br.com.invista.model.Session;
import br.com.invista.json.JsonUser;
import br.com.invista.model.User;
import br.com.invista.model.User;
import br.com.invista.json.JsonUser;

//saporra
public class Parser {

	private static final DateTimeFormatter DATE_TIME_FORMAT = DateTimeFormat.forPattern("dd/MM/yyyy HH:mm");
	private static final DateTimeFormatter DATE_FORMAT = DateTimeFormat.forPattern("dd/MM/yyyy");
	private static final DateTimeFormatter HOUR_FORMAT = DateTimeFormat.forPattern("HH:mm");

	public static String getHourAsString(LocalDateTime date) {
		String format = "";
		try {
			format = HOUR_FORMAT.print(date);
		} catch (Exception e) {
			format = "00:00";
		}
		return format;
	}

	public static String getDateTimeAsString(LocalDateTime date) {
		String format = "";
		try {
			format = DATE_TIME_FORMAT.print(date);
		} catch (Exception e) {
			format = DATE_TIME_FORMAT.print(new DateTime());
		}
		return format;
	}

	public static String getDateAsString(LocalDateTime date) {
		String format = "";
		try {
			format = DATE_FORMAT.print(date);
		} catch (Exception e) {
			format = DATE_FORMAT.print(new DateTime());
		}
		return format;
	}

	//
	private static DateTime getHour(String date) {
		if (!date.isEmpty()) {
			try {
				return HOUR_FORMAT.parseDateTime(date);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		return null;
	}

	private static LocalDateTime getDate(String date) {
		if (!date.isEmpty()) {
			try {
				LocalDateTime dateTime = DATE_FORMAT.parseLocalDateTime(date);
				return dateTime;
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		return null;
	}

	private static LocalDateTime getDateTime(String date) {
		if (!date.isEmpty()) {
			try {
				LocalDateTime dateTime = DATE_TIME_FORMAT.parseLocalDateTime(date);
				return dateTime;
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		return null;
	}
	
	//converte de entidade para json --------------------
	private static JsonCentroCusto toBasicJson(CentroCusto centroCusto) {
		JsonCentroCusto jsonCentroCusto = new JsonCentroCusto();
		applyBasicJsonValues(jsonCentroCusto, centroCusto);
		return jsonCentroCusto;
	}
	
	private static CentroCusto toBasicEntity(JsonCentroCusto jsonCentroCusto) {
		CentroCusto centroCusto = new CentroCusto();
		applyBasicEntityValues(centroCusto, jsonCentroCusto);
		return centroCusto;
	}
	
	private static void applyBasicJsonValues(JsonCentroCusto jsonCentroCusto, CentroCusto centroCusto) {
		jsonCentroCusto.setId(centroCusto.getId());
	    jsonCentroCusto.setDescricao(centroCusto.getDescricao());
	    jsonCentroCusto.setValorLimite(centroCusto.getValorLimite());
	    jsonCentroCusto.setObservacao(centroCusto.getObservacao());
	    jsonCentroCusto.setObseracaoTaxista(centroCusto.getObseracaoTaxista());
	    jsonCentroCusto.setInativo(centroCusto.getInativo());
	}	
	private static void applyBasicEntityValues(CentroCusto centroCusto, JsonCentroCusto jsonCentroCusto) {
		centroCusto.setId(jsonCentroCusto.getId());
		centroCusto.setDescricao(jsonCentroCusto.getDescricao());
		centroCusto.setValorLimite(jsonCentroCusto.getValorLimite());
		centroCusto.setObservacao(jsonCentroCusto.getObservacao());
		centroCusto.setObseracaoTaxista(jsonCentroCusto.getObseracaoTaxista());
		centroCusto.setInativo(jsonCentroCusto.getInativo());
	}	
	
	public static JsonCentroCusto toJson(CentroCusto centroCusto) {
		JsonCentroCusto jsonCentroCusto = new JsonCentroCusto();

		applyBasicJsonValues(jsonCentroCusto, centroCusto);

		List<Funcionario> listFuncionarios = centroCusto.getFuncionarios();
		if (listFuncionarios != null) {
			for (Funcionario loopFuncionario : listFuncionarios) {
				jsonCentroCusto.getFuncionarios().add(toBasicJson(loopFuncionario));
			}
		}
		Empresa empresa_ = centroCusto.getEmpresa();
		if (empresa_ != null) {
			jsonCentroCusto.setEmpresa(toJson(empresa_));
		}
		return jsonCentroCusto;
	}


	public static CentroCusto apply(CentroCusto centroCusto, JsonCentroCusto jsonCentroCusto) {
	
		if(centroCusto ==  null)
			centroCusto = new CentroCusto();
		
		applyBasicEntityValues(centroCusto, jsonCentroCusto) ;

		JsonEmpresa empresa_ = jsonCentroCusto.getEmpresa();
		if (empresa_ != null) {
			centroCusto.setEmpresa(toEntity(empresa_));
		}	
		return centroCusto;
		
	}		
	public static CentroCusto toEntity(JsonCentroCusto jsonCentroCusto) {
		CentroCusto centroCusto = new CentroCusto();
		
		return apply(centroCusto, jsonCentroCusto);
	}		
	
	public static List<JsonCentroCusto> toListJsonCentroCustos(List<CentroCusto> all) {
		List<JsonCentroCusto> jsonCentroCustos = new ArrayList<JsonCentroCusto>();
		for (CentroCusto centroCusto : all) {
			jsonCentroCustos.add(toJson(centroCusto));
		}
		return jsonCentroCustos;
	}
	//converte de entidade para json --------------------
	private static JsonEmpresa toBasicJson(Empresa empresa) {
		JsonEmpresa jsonEmpresa = new JsonEmpresa();
		applyBasicJsonValues(jsonEmpresa, empresa);
		return jsonEmpresa;
	}
	
	private static Empresa toBasicEntity(JsonEmpresa jsonEmpresa) {
		Empresa empresa = new Empresa();
		applyBasicEntityValues(empresa, jsonEmpresa);
		return empresa;
	}
	
	private static void applyBasicJsonValues(JsonEmpresa jsonEmpresa, Empresa empresa) {
		jsonEmpresa.setId(empresa.getId());
	    jsonEmpresa.setCnpj(empresa.getCnpj());
	    jsonEmpresa.setNumeroContrato(empresa.getNumeroContrato());
	    jsonEmpresa.setRazaoSocial(empresa.getRazaoSocial());
	    jsonEmpresa.setNomeFantasia(empresa.getNomeFantasia());
	    jsonEmpresa.setUtilizaVoucher(empresa.getUtilizaVoucher());
	    jsonEmpresa.setUtilizaTicket(empresa.getUtilizaTicket());
	    jsonEmpresa.setObservacaoTaxista(empresa.getObservacaoTaxista());
	    jsonEmpresa.setObservacaoCentral(empresa.getObservacaoCentral());
	    jsonEmpresa.setPercursoDesconto(empresa.getPercursoDesconto());
	    jsonEmpresa.setInscricaoMunicipal(empresa.getInscricaoMunicipal());
	    jsonEmpresa.setInscricaoEstadual(empresa.getInscricaoEstadual());
	    jsonEmpresa.setNf(empresa.getNf());
	    jsonEmpresa.setPercentualIss(empresa.getPercentualIss());
	    jsonEmpresa.setPercentualIrf(empresa.getPercentualIrf());
	    jsonEmpresa.setPercentualInss(empresa.getPercentualInss());
	    jsonEmpresa.setDiaVencimento(empresa.getDiaVencimento());
	    jsonEmpresa.setPercentualMotorista(empresa.getPercentualMotorista());
	    jsonEmpresa.setBanco(empresa.getBanco());
	    jsonEmpresa.setAgencia(empresa.getAgencia());
	    jsonEmpresa.setConta(empresa.getConta());
	    jsonEmpresa.setEmail(empresa.getEmail());
	}	
	private static void applyBasicEntityValues(Empresa empresa, JsonEmpresa jsonEmpresa) {
		empresa.setId(jsonEmpresa.getId());
		empresa.setCnpj(jsonEmpresa.getCnpj());
		empresa.setNumeroContrato(jsonEmpresa.getNumeroContrato());
		empresa.setRazaoSocial(jsonEmpresa.getRazaoSocial());
		empresa.setNomeFantasia(jsonEmpresa.getNomeFantasia());
		empresa.setUtilizaVoucher(jsonEmpresa.getUtilizaVoucher());
		empresa.setUtilizaTicket(jsonEmpresa.getUtilizaTicket());
		empresa.setObservacaoTaxista(jsonEmpresa.getObservacaoTaxista());
		empresa.setObservacaoCentral(jsonEmpresa.getObservacaoCentral());
		empresa.setPercursoDesconto(jsonEmpresa.getPercursoDesconto());
		empresa.setInscricaoMunicipal(jsonEmpresa.getInscricaoMunicipal());
		empresa.setInscricaoEstadual(jsonEmpresa.getInscricaoEstadual());
		empresa.setNf(jsonEmpresa.getNf());
		empresa.setPercentualIss(jsonEmpresa.getPercentualIss());
		empresa.setPercentualIrf(jsonEmpresa.getPercentualIrf());
		empresa.setPercentualInss(jsonEmpresa.getPercentualInss());
		empresa.setDiaVencimento(jsonEmpresa.getDiaVencimento());
		empresa.setPercentualMotorista(jsonEmpresa.getPercentualMotorista());
		empresa.setBanco(jsonEmpresa.getBanco());
		empresa.setAgencia(jsonEmpresa.getAgencia());
		empresa.setConta(jsonEmpresa.getConta());
		empresa.setEmail(jsonEmpresa.getEmail());
	}	
	
	public static JsonEmpresa toJson(Empresa empresa) {
		JsonEmpresa jsonEmpresa = new JsonEmpresa();

		applyBasicJsonValues(jsonEmpresa, empresa);

		List<CentroCusto> listCentroCustos = empresa.getCentroCustos();
		if (listCentroCustos != null) {
			for (CentroCusto loopCentroCusto : listCentroCustos) {
				jsonEmpresa.getCentroCustos().add(toBasicJson(loopCentroCusto));
			}
		}
		List<Funcionario> listFuncionarios = empresa.getFuncionarios();
		if (listFuncionarios != null) {
			for (Funcionario loopFuncionario : listFuncionarios) {
				jsonEmpresa.getFuncionarios().add(toBasicJson(loopFuncionario));
			}
		}
		return jsonEmpresa;
	}


	public static Empresa apply(Empresa empresa, JsonEmpresa jsonEmpresa) {
	
		if(empresa ==  null)
			empresa = new Empresa();
		
		applyBasicEntityValues(empresa, jsonEmpresa) ;

		return empresa;
		
	}		
	public static Empresa toEntity(JsonEmpresa jsonEmpresa) {
		Empresa empresa = new Empresa();
		
		return apply(empresa, jsonEmpresa);
	}		
	
	public static List<JsonEmpresa> toListJsonEmpresas(List<Empresa> all) {
		List<JsonEmpresa> jsonEmpresas = new ArrayList<JsonEmpresa>();
		for (Empresa empresa : all) {
			jsonEmpresas.add(toJson(empresa));
		}
		return jsonEmpresas;
	}
	//converte de entidade para json --------------------
	private static JsonFuncionario toBasicJson(Funcionario funcionario) {
		JsonFuncionario jsonFuncionario = new JsonFuncionario();
		applyBasicJsonValues(jsonFuncionario, funcionario);
		return jsonFuncionario;
	}
	
	private static Funcionario toBasicEntity(JsonFuncionario jsonFuncionario) {
		Funcionario funcionario = new Funcionario();
		applyBasicEntityValues(funcionario, jsonFuncionario);
		return funcionario;
	}
	
	private static void applyBasicJsonValues(JsonFuncionario jsonFuncionario, Funcionario funcionario) {
		jsonFuncionario.setId(funcionario.getId());
	    jsonFuncionario.setNome(funcionario.getNome());
	    jsonFuncionario.setEmail(funcionario.getEmail());
	    jsonFuncionario.setTelefone(funcionario.getTelefone());
	    jsonFuncionario.setLimiteMensal(funcionario.getLimiteMensal());
	    jsonFuncionario.setCpf(funcionario.getCpf());
	    jsonFuncionario.setMatricula(funcionario.getMatricula());
	    jsonFuncionario.setStatusFuncionario(funcionario.getStatusFuncionario());
	    jsonFuncionario.setAutorizadoTicket(funcionario.getAutorizadoTicket());
	    jsonFuncionario.setObservacao(funcionario.getObservacao());
	}	
	private static void applyBasicEntityValues(Funcionario funcionario, JsonFuncionario jsonFuncionario) {
		funcionario.setId(jsonFuncionario.getId());
		funcionario.setNome(jsonFuncionario.getNome());
		funcionario.setEmail(jsonFuncionario.getEmail());
		funcionario.setTelefone(jsonFuncionario.getTelefone());
		funcionario.setLimiteMensal(jsonFuncionario.getLimiteMensal());
		funcionario.setCpf(jsonFuncionario.getCpf());
		funcionario.setMatricula(jsonFuncionario.getMatricula());
		funcionario.setStatusFuncionario(jsonFuncionario.getStatusFuncionario());
		funcionario.setAutorizadoTicket(jsonFuncionario.getAutorizadoTicket());
		funcionario.setObservacao(jsonFuncionario.getObservacao());
	}	
	
	public static JsonFuncionario toJson(Funcionario funcionario) {
		JsonFuncionario jsonFuncionario = new JsonFuncionario();

		applyBasicJsonValues(jsonFuncionario, funcionario);

		Empresa empresa_ = funcionario.getEmpresa();
		if (empresa_ != null) {
			jsonFuncionario.setEmpresa(toJson(empresa_));
		}
		CentroCusto centroCusto_ = funcionario.getCentroCusto();
		if (centroCusto_ != null) {
			jsonFuncionario.setCentroCusto(toJson(centroCusto_));
		}
		return jsonFuncionario;
	}


	public static Funcionario apply(Funcionario funcionario, JsonFuncionario jsonFuncionario) {
	
		if(funcionario ==  null)
			funcionario = new Funcionario();
		
		applyBasicEntityValues(funcionario, jsonFuncionario) ;

		JsonEmpresa empresa_ = jsonFuncionario.getEmpresa();
		if (empresa_ != null) {
			funcionario.setEmpresa(toEntity(empresa_));
		}	
		JsonCentroCusto centroCusto_ = jsonFuncionario.getCentroCusto();
		if (centroCusto_ != null) {
			funcionario.setCentroCusto(toEntity(centroCusto_));
		}	
		return funcionario;
		
	}		
	public static Funcionario toEntity(JsonFuncionario jsonFuncionario) {
		Funcionario funcionario = new Funcionario();
		
		return apply(funcionario, jsonFuncionario);
	}		
	
	public static List<JsonFuncionario> toListJsonFuncionarios(List<Funcionario> all) {
		List<JsonFuncionario> jsonFuncionarios = new ArrayList<JsonFuncionario>();
		for (Funcionario funcionario : all) {
			jsonFuncionarios.add(toJson(funcionario));
		}
		return jsonFuncionarios;
	}
	//converte de entidade para json --------------------
	private static JsonBairro toBasicJson(Bairro bairro) {
		JsonBairro jsonBairro = new JsonBairro();
		applyBasicJsonValues(jsonBairro, bairro);
		return jsonBairro;
	}
	
	private static Bairro toBasicEntity(JsonBairro jsonBairro) {
		Bairro bairro = new Bairro();
		applyBasicEntityValues(bairro, jsonBairro);
		return bairro;
	}
	
	private static void applyBasicJsonValues(JsonBairro jsonBairro, Bairro bairro) {
		jsonBairro.setId(bairro.getId());
	    jsonBairro.setNome(bairro.getNome());
	}	
	private static void applyBasicEntityValues(Bairro bairro, JsonBairro jsonBairro) {
		bairro.setId(jsonBairro.getId());
		bairro.setNome(jsonBairro.getNome());
	}	
	
	public static JsonBairro toJson(Bairro bairro) {
		JsonBairro jsonBairro = new JsonBairro();

		applyBasicJsonValues(jsonBairro, bairro);

		Cidade cidade_ = bairro.getCidade();
		if (cidade_ != null) {
			jsonBairro.setCidade(toJson(cidade_));
		}
		Estado estado_ = bairro.getEstado();
		if (estado_ != null) {
			jsonBairro.setEstado(toJson(estado_));
		}
		return jsonBairro;
	}


	public static Bairro apply(Bairro bairro, JsonBairro jsonBairro) {
	
		if(bairro ==  null)
			bairro = new Bairro();
		
		applyBasicEntityValues(bairro, jsonBairro) ;

		JsonCidade cidade_ = jsonBairro.getCidade();
		if (cidade_ != null) {
			bairro.setCidade(toEntity(cidade_));
		}	
		JsonEstado estado_ = jsonBairro.getEstado();
		if (estado_ != null) {
			bairro.setEstado(toEntity(estado_));
		}	
		return bairro;
		
	}		
	public static Bairro toEntity(JsonBairro jsonBairro) {
		Bairro bairro = new Bairro();
		
		return apply(bairro, jsonBairro);
	}		
	
	public static List<JsonBairro> toListJsonBairros(List<Bairro> all) {
		List<JsonBairro> jsonBairros = new ArrayList<JsonBairro>();
		for (Bairro bairro : all) {
			jsonBairros.add(toJson(bairro));
		}
		return jsonBairros;
	}
	//converte de entidade para json --------------------
	private static JsonCep toBasicJson(Cep cep) {
		JsonCep jsonCep = new JsonCep();
		applyBasicJsonValues(jsonCep, cep);
		return jsonCep;
	}
	
	private static Cep toBasicEntity(JsonCep jsonCep) {
		Cep cep = new Cep();
		applyBasicEntityValues(cep, jsonCep);
		return cep;
	}
	
	private static void applyBasicJsonValues(JsonCep jsonCep, Cep cep) {
		jsonCep.setId(cep.getId());
	    jsonCep.setLogradouro(cep.getLogradouro());
	    jsonCep.setNumero(cep.getNumero());
	}	
	private static void applyBasicEntityValues(Cep cep, JsonCep jsonCep) {
		cep.setId(jsonCep.getId());
		cep.setLogradouro(jsonCep.getLogradouro());
		cep.setNumero(jsonCep.getNumero());
	}	
	
	public static JsonCep toJson(Cep cep) {
		JsonCep jsonCep = new JsonCep();

		applyBasicJsonValues(jsonCep, cep);

		Bairro bairro_ = cep.getBairro();
		if (bairro_ != null) {
			jsonCep.setBairro(toJson(bairro_));
		}
		Cidade cidade_ = cep.getCidade();
		if (cidade_ != null) {
			jsonCep.setCidade(toJson(cidade_));
		}
		Estado estado_ = cep.getEstado();
		if (estado_ != null) {
			jsonCep.setEstado(toJson(estado_));
		}
		return jsonCep;
	}


	public static Cep apply(Cep cep, JsonCep jsonCep) {
	
		if(cep ==  null)
			cep = new Cep();
		
		applyBasicEntityValues(cep, jsonCep) ;

		JsonBairro bairro_ = jsonCep.getBairro();
		if (bairro_ != null) {
			cep.setBairro(toEntity(bairro_));
		}	
		JsonCidade cidade_ = jsonCep.getCidade();
		if (cidade_ != null) {
			cep.setCidade(toEntity(cidade_));
		}	
		JsonEstado estado_ = jsonCep.getEstado();
		if (estado_ != null) {
			cep.setEstado(toEntity(estado_));
		}	
		return cep;
		
	}		
	public static Cep toEntity(JsonCep jsonCep) {
		Cep cep = new Cep();
		
		return apply(cep, jsonCep);
	}		
	
	public static List<JsonCep> toListJsonCeps(List<Cep> all) {
		List<JsonCep> jsonCeps = new ArrayList<JsonCep>();
		for (Cep cep : all) {
			jsonCeps.add(toJson(cep));
		}
		return jsonCeps;
	}
	//converte de entidade para json --------------------
	private static JsonCidade toBasicJson(Cidade cidade) {
		JsonCidade jsonCidade = new JsonCidade();
		applyBasicJsonValues(jsonCidade, cidade);
		return jsonCidade;
	}
	
	private static Cidade toBasicEntity(JsonCidade jsonCidade) {
		Cidade cidade = new Cidade();
		applyBasicEntityValues(cidade, jsonCidade);
		return cidade;
	}
	
	private static void applyBasicJsonValues(JsonCidade jsonCidade, Cidade cidade) {
		jsonCidade.setId(cidade.getId());
	    jsonCidade.setNome(cidade.getNome());
	    jsonCidade.setCep(cidade.getCep());
	}	
	private static void applyBasicEntityValues(Cidade cidade, JsonCidade jsonCidade) {
		cidade.setId(jsonCidade.getId());
		cidade.setNome(jsonCidade.getNome());
		cidade.setCep(jsonCidade.getCep());
	}	
	
	public static JsonCidade toJson(Cidade cidade) {
		JsonCidade jsonCidade = new JsonCidade();

		applyBasicJsonValues(jsonCidade, cidade);

		Estado estado_ = cidade.getEstado();
		if (estado_ != null) {
			jsonCidade.setEstado(toJson(estado_));
		}
		return jsonCidade;
	}


	public static Cidade apply(Cidade cidade, JsonCidade jsonCidade) {
	
		if(cidade ==  null)
			cidade = new Cidade();
		
		applyBasicEntityValues(cidade, jsonCidade) ;

		JsonEstado estado_ = jsonCidade.getEstado();
		if (estado_ != null) {
			cidade.setEstado(toEntity(estado_));
		}	
		return cidade;
		
	}		
	public static Cidade toEntity(JsonCidade jsonCidade) {
		Cidade cidade = new Cidade();
		
		return apply(cidade, jsonCidade);
	}		
	
	public static List<JsonCidade> toListJsonCidades(List<Cidade> all) {
		List<JsonCidade> jsonCidades = new ArrayList<JsonCidade>();
		for (Cidade cidade : all) {
			jsonCidades.add(toJson(cidade));
		}
		return jsonCidades;
	}
	//converte de entidade para json --------------------
	private static JsonEndereco toBasicJson(Endereco endereco) {
		JsonEndereco jsonEndereco = new JsonEndereco();
		applyBasicJsonValues(jsonEndereco, endereco);
		return jsonEndereco;
	}
	
	private static Endereco toBasicEntity(JsonEndereco jsonEndereco) {
		Endereco endereco = new Endereco();
		applyBasicEntityValues(endereco, jsonEndereco);
		return endereco;
	}
	
	private static void applyBasicJsonValues(JsonEndereco jsonEndereco, Endereco endereco) {
		jsonEndereco.setId(endereco.getId());
	    jsonEndereco.setComplemento(endereco.getComplemento());
	    jsonEndereco.setNumero(endereco.getNumero());
	}	
	private static void applyBasicEntityValues(Endereco endereco, JsonEndereco jsonEndereco) {
		endereco.setId(jsonEndereco.getId());
		endereco.setComplemento(jsonEndereco.getComplemento());
		endereco.setNumero(jsonEndereco.getNumero());
	}	
	
	public static JsonEndereco toJson(Endereco endereco) {
		JsonEndereco jsonEndereco = new JsonEndereco();

		applyBasicJsonValues(jsonEndereco, endereco);

		Cep cep_ = endereco.getCep();
		if (cep_ != null) {
			jsonEndereco.setCep(toJson(cep_));
		}
		return jsonEndereco;
	}


	public static Endereco apply(Endereco endereco, JsonEndereco jsonEndereco) {
	
		if(endereco ==  null)
			endereco = new Endereco();
		
		applyBasicEntityValues(endereco, jsonEndereco) ;

		JsonCep cep_ = jsonEndereco.getCep();
		if (cep_ != null) {
			endereco.setCep(toEntity(cep_));
		}	
		return endereco;
		
	}		
	public static Endereco toEntity(JsonEndereco jsonEndereco) {
		Endereco endereco = new Endereco();
		
		return apply(endereco, jsonEndereco);
	}		
	
	public static List<JsonEndereco> toListJsonEnderecos(List<Endereco> all) {
		List<JsonEndereco> jsonEnderecos = new ArrayList<JsonEndereco>();
		for (Endereco endereco : all) {
			jsonEnderecos.add(toJson(endereco));
		}
		return jsonEnderecos;
	}
	//converte de entidade para json --------------------
	private static JsonEstado toBasicJson(Estado estado) {
		JsonEstado jsonEstado = new JsonEstado();
		applyBasicJsonValues(jsonEstado, estado);
		return jsonEstado;
	}
	
	private static Estado toBasicEntity(JsonEstado jsonEstado) {
		Estado estado = new Estado();
		applyBasicEntityValues(estado, jsonEstado);
		return estado;
	}
	
	private static void applyBasicJsonValues(JsonEstado jsonEstado, Estado estado) {
		jsonEstado.setId(estado.getId());
	    jsonEstado.setNome(estado.getNome());
	    jsonEstado.setFaixaCep1Ini(estado.getFaixaCep1Ini());
	    jsonEstado.setFaixaCep1Fim(estado.getFaixaCep1Fim());
	    jsonEstado.setFaixaCep2Ini(estado.getFaixaCep2Ini());
	    jsonEstado.setFaixaCep2Fim(estado.getFaixaCep2Fim());
	}	
	private static void applyBasicEntityValues(Estado estado, JsonEstado jsonEstado) {
		estado.setId(jsonEstado.getId());
		estado.setNome(jsonEstado.getNome());
		estado.setFaixaCep1Ini(jsonEstado.getFaixaCep1Ini());
		estado.setFaixaCep1Fim(jsonEstado.getFaixaCep1Fim());
		estado.setFaixaCep2Ini(jsonEstado.getFaixaCep2Ini());
		estado.setFaixaCep2Fim(jsonEstado.getFaixaCep2Fim());
	}	
	
	public static JsonEstado toJson(Estado estado) {
		JsonEstado jsonEstado = new JsonEstado();

		applyBasicJsonValues(jsonEstado, estado);

		return jsonEstado;
	}


	public static Estado apply(Estado estado, JsonEstado jsonEstado) {
	
		if(estado ==  null)
			estado = new Estado();
		
		applyBasicEntityValues(estado, jsonEstado) ;

		return estado;
		
	}		
	public static Estado toEntity(JsonEstado jsonEstado) {
		Estado estado = new Estado();
		
		return apply(estado, jsonEstado);
	}		
	
	public static List<JsonEstado> toListJsonEstados(List<Estado> all) {
		List<JsonEstado> jsonEstados = new ArrayList<JsonEstado>();
		for (Estado estado : all) {
			jsonEstados.add(toJson(estado));
		}
		return jsonEstados;
	}
	//converte de entidade para json --------------------
	private static JsonPais toBasicJson(Pais pais) {
		JsonPais jsonPais = new JsonPais();
		applyBasicJsonValues(jsonPais, pais);
		return jsonPais;
	}
	
	private static Pais toBasicEntity(JsonPais jsonPais) {
		Pais pais = new Pais();
		applyBasicEntityValues(pais, jsonPais);
		return pais;
	}
	
	private static void applyBasicJsonValues(JsonPais jsonPais, Pais pais) {
		jsonPais.setId(pais.getId());
	    jsonPais.setCodigo(pais.getCodigo());
	    jsonPais.setNome(pais.getNome());
	}	
	private static void applyBasicEntityValues(Pais pais, JsonPais jsonPais) {
		pais.setId(jsonPais.getId());
		pais.setCodigo(jsonPais.getCodigo());
		pais.setNome(jsonPais.getNome());
	}	
	
	public static JsonPais toJson(Pais pais) {
		JsonPais jsonPais = new JsonPais();

		applyBasicJsonValues(jsonPais, pais);

		return jsonPais;
	}


	public static Pais apply(Pais pais, JsonPais jsonPais) {
	
		if(pais ==  null)
			pais = new Pais();
		
		applyBasicEntityValues(pais, jsonPais) ;

		return pais;
		
	}		
	public static Pais toEntity(JsonPais jsonPais) {
		Pais pais = new Pais();
		
		return apply(pais, jsonPais);
	}		
	
	public static List<JsonPais> toListJsonPaiss(List<Pais> all) {
		List<JsonPais> jsonPaiss = new ArrayList<JsonPais>();
		for (Pais pais : all) {
			jsonPaiss.add(toJson(pais));
		}
		return jsonPaiss;
	}
	//converte de entidade para json --------------------
	private static JsonItem toBasicJson(Item item) {
		JsonItem jsonItem = new JsonItem();
		applyBasicJsonValues(jsonItem, item);
		return jsonItem;
	}
	
	private static Item toBasicEntity(JsonItem jsonItem) {
		Item item = new Item();
		applyBasicEntityValues(item, jsonItem);
		return item;
	}
	
	private static void applyBasicJsonValues(JsonItem jsonItem, Item item) {
		jsonItem.setId(item.getId());
	    jsonItem.setName(item.getName());
	    jsonItem.setDescription(item.getDescription());
	}	
	private static void applyBasicEntityValues(Item item, JsonItem jsonItem) {
		item.setId(jsonItem.getId());
		item.setName(jsonItem.getName());
		item.setDescription(jsonItem.getDescription());
	}	
	
	public static JsonItem toJson(Item item) {
		JsonItem jsonItem = new JsonItem();

		applyBasicJsonValues(jsonItem, item);

		ItemType type_ = item.getType();
		if (type_ != null) {
			jsonItem.setType(toJson(type_));
		}
		List<Permission> listPermissions = item.getPermissions();
		if (listPermissions != null) {
			for (Permission loopPermission : listPermissions) {
				jsonItem.getPermissions().add(toBasicJson(loopPermission));
			}
		}
		return jsonItem;
	}


	public static Item apply(Item item, JsonItem jsonItem) {
	
		if(item ==  null)
			item = new Item();
		
		applyBasicEntityValues(item, jsonItem) ;

		JsonItemType type_ = jsonItem.getType();
		if (type_ != null) {
			item.setType(toEntity(type_));
		}	
		return item;
		
	}		
	public static Item toEntity(JsonItem jsonItem) {
		Item item = new Item();
		
		return apply(item, jsonItem);
	}		
	
	public static List<JsonItem> toListJsonItems(List<Item> all) {
		List<JsonItem> jsonItems = new ArrayList<JsonItem>();
		for (Item item : all) {
			jsonItems.add(toJson(item));
		}
		return jsonItems;
	}
	//converte de entidade para json --------------------
	private static JsonItemType toBasicJson(ItemType itemType) {
		JsonItemType jsonItemType = new JsonItemType();
		applyBasicJsonValues(jsonItemType, itemType);
		return jsonItemType;
	}
	
	private static ItemType toBasicEntity(JsonItemType jsonItemType) {
		ItemType itemType = new ItemType();
		applyBasicEntityValues(itemType, jsonItemType);
		return itemType;
	}
	
	private static void applyBasicJsonValues(JsonItemType jsonItemType, ItemType itemType) {
		jsonItemType.setId(itemType.getId());
	    jsonItemType.setName(itemType.getName());
	    jsonItemType.setDescription(itemType.getDescription());
	}	
	private static void applyBasicEntityValues(ItemType itemType, JsonItemType jsonItemType) {
		itemType.setId(jsonItemType.getId());
		itemType.setName(jsonItemType.getName());
		itemType.setDescription(jsonItemType.getDescription());
	}	
	
	public static JsonItemType toJson(ItemType itemType) {
		JsonItemType jsonItemType = new JsonItemType();

		applyBasicJsonValues(jsonItemType, itemType);

		return jsonItemType;
	}


	public static ItemType apply(ItemType itemType, JsonItemType jsonItemType) {
	
		if(itemType ==  null)
			itemType = new ItemType();
		
		applyBasicEntityValues(itemType, jsonItemType) ;

		return itemType;
		
	}		
	public static ItemType toEntity(JsonItemType jsonItemType) {
		ItemType itemType = new ItemType();
		
		return apply(itemType, jsonItemType);
	}		
	
	public static List<JsonItemType> toListJsonItemTypes(List<ItemType> all) {
		List<JsonItemType> jsonItemTypes = new ArrayList<JsonItemType>();
		for (ItemType itemType : all) {
			jsonItemTypes.add(toJson(itemType));
		}
		return jsonItemTypes;
	}
	//converte de entidade para json --------------------
	private static JsonOperation toBasicJson(Operation operation) {
		JsonOperation jsonOperation = new JsonOperation();
		applyBasicJsonValues(jsonOperation, operation);
		return jsonOperation;
	}
	
	private static Operation toBasicEntity(JsonOperation jsonOperation) {
		Operation operation = new Operation();
		applyBasicEntityValues(operation, jsonOperation);
		return operation;
	}
	
	private static void applyBasicJsonValues(JsonOperation jsonOperation, Operation operation) {
		jsonOperation.setId(operation.getId());
	    jsonOperation.setName(operation.getName());
	    jsonOperation.setCanEdit(operation.getCanEdit());
	    jsonOperation.setCanRead(operation.getCanRead());
	    jsonOperation.setCanUpdate(operation.getCanUpdate());
	    jsonOperation.setCanDelete(operation.getCanDelete());
	    jsonOperation.setCanExecute(operation.getCanExecute());
	}	
	private static void applyBasicEntityValues(Operation operation, JsonOperation jsonOperation) {
		operation.setId(jsonOperation.getId());
		operation.setName(jsonOperation.getName());
		operation.setCanEdit(jsonOperation.getCanEdit());
		operation.setCanRead(jsonOperation.getCanRead());
		operation.setCanUpdate(jsonOperation.getCanUpdate());
		operation.setCanDelete(jsonOperation.getCanDelete());
		operation.setCanExecute(jsonOperation.getCanExecute());
	}	
	
	public static JsonOperation toJson(Operation operation) {
		JsonOperation jsonOperation = new JsonOperation();

		applyBasicJsonValues(jsonOperation, operation);

		List<Permission> listPermissions = operation.getPermissions();
		if (listPermissions != null) {
			for (Permission loopPermission : listPermissions) {
				jsonOperation.getPermissions().add(toBasicJson(loopPermission));
			}
		}
		return jsonOperation;
	}


	public static Operation apply(Operation operation, JsonOperation jsonOperation) {
	
		if(operation ==  null)
			operation = new Operation();
		
		applyBasicEntityValues(operation, jsonOperation) ;

		return operation;
		
	}		
	public static Operation toEntity(JsonOperation jsonOperation) {
		Operation operation = new Operation();
		
		return apply(operation, jsonOperation);
	}		
	
	public static List<JsonOperation> toListJsonOperations(List<Operation> all) {
		List<JsonOperation> jsonOperations = new ArrayList<JsonOperation>();
		for (Operation operation : all) {
			jsonOperations.add(toJson(operation));
		}
		return jsonOperations;
	}
	//converte de entidade para json --------------------
	private static JsonPermission toBasicJson(Permission permission) {
		JsonPermission jsonPermission = new JsonPermission();
		applyBasicJsonValues(jsonPermission, permission);
		return jsonPermission;
	}
	
	private static Permission toBasicEntity(JsonPermission jsonPermission) {
		Permission permission = new Permission();
		applyBasicEntityValues(permission, jsonPermission);
		return permission;
	}
	
	private static void applyBasicJsonValues(JsonPermission jsonPermission, Permission permission) {
		jsonPermission.setId(permission.getId());
	    jsonPermission.setName(permission.getName());
	}	
	private static void applyBasicEntityValues(Permission permission, JsonPermission jsonPermission) {
		permission.setId(jsonPermission.getId());
		permission.setName(jsonPermission.getName());
	}	
	
	public static JsonPermission toJson(Permission permission) {
		JsonPermission jsonPermission = new JsonPermission();

		applyBasicJsonValues(jsonPermission, permission);

		List<Role> listRoles = permission.getRoles();
		if (listRoles != null) {
			for (Role loopRole : listRoles) {
				jsonPermission.getRoles().add(toBasicJson(loopRole));
			}
		}			

		Operation operation_ = permission.getOperation();
		if (operation_ != null) {
			jsonPermission.setOperation(toJson(operation_));
		}
		Item item_ = permission.getItem();
		if (item_ != null) {
			jsonPermission.setItem(toJson(item_));
		}
		return jsonPermission;
	}


	public static Permission apply(Permission permission, JsonPermission jsonPermission) {
	
		if(permission ==  null)
			permission = new Permission();
		
		applyBasicEntityValues(permission, jsonPermission) ;

		ArrayList<JsonRole> listRoles = jsonPermission.getRoles();			
		if (listRoles != null) {
			for (JsonRole loopJsonRole : listRoles) {
				permission.addRoles(toBasicEntity(loopJsonRole));
			}
		}
			
		JsonOperation operation_ = jsonPermission.getOperation();
		if (operation_ != null) {
			permission.setOperation(toEntity(operation_));
		}	
		JsonItem item_ = jsonPermission.getItem();
		if (item_ != null) {
			permission.setItem(toEntity(item_));
		}	
		return permission;
		
	}		
	public static Permission toEntity(JsonPermission jsonPermission) {
		Permission permission = new Permission();
		
		return apply(permission, jsonPermission);
	}		
	
	public static List<JsonPermission> toListJsonPermissions(List<Permission> all) {
		List<JsonPermission> jsonPermissions = new ArrayList<JsonPermission>();
		for (Permission permission : all) {
			jsonPermissions.add(toJson(permission));
		}
		return jsonPermissions;
	}
	//converte de entidade para json --------------------
	private static JsonRole toBasicJson(Role role) {
		JsonRole jsonRole = new JsonRole();
		applyBasicJsonValues(jsonRole, role);
		return jsonRole;
	}
	
	private static Role toBasicEntity(JsonRole jsonRole) {
		Role role = new Role();
		applyBasicEntityValues(role, jsonRole);
		return role;
	}
	
	private static void applyBasicJsonValues(JsonRole jsonRole, Role role) {
		jsonRole.setId(role.getId());
	    jsonRole.setAuthority(role.getAuthority());
	    jsonRole.setDescription(role.getDescription());
	}	
	private static void applyBasicEntityValues(Role role, JsonRole jsonRole) {
		role.setId(jsonRole.getId());
		role.setAuthority(jsonRole.getAuthority());
		role.setDescription(jsonRole.getDescription());
	}	
	
	public static JsonRole toJson(Role role) {
		JsonRole jsonRole = new JsonRole();

		applyBasicJsonValues(jsonRole, role);

		List<Session> listSessions = role.getSessions();
		if (listSessions != null) {
			for (Session loopSession : listSessions) {
				jsonRole.getSessions().add(toBasicJson(loopSession));
			}
		}			

		List<User> listUsers = role.getUsers();
		if (listUsers != null) {
			for (User loopUser : listUsers) {
				jsonRole.getUsers().add(toBasicJson(loopUser));
			}
		}			

		List<Permission> listPermissions = role.getPermissions();
		if (listPermissions != null) {
			for (Permission loopPermission : listPermissions) {
				jsonRole.getPermissions().add(toJson(loopPermission));
			}
		}

		return jsonRole;
	}


	public static Role apply(Role role, JsonRole jsonRole) {
	
		if(role ==  null)
			role = new Role();
		
		applyBasicEntityValues(role, jsonRole) ;

		ArrayList<JsonSession> listSessions = jsonRole.getSessions();			
		if (listSessions != null) {
			for (JsonSession loopJsonSession : listSessions) {
				role.addSessions(toBasicEntity(loopJsonSession));
			}
		}
			
		ArrayList<JsonUser> listUsers = jsonRole.getUsers();			
		if (listUsers != null) {
			for (JsonUser loopJsonUser : listUsers) {
				role.addUsers(toBasicEntity(loopJsonUser));
			}
		}
			
		ArrayList<JsonPermission> listPermissions = jsonRole.getPermissions();			
		if (listPermissions != null) {
			for (JsonPermission loopJsonPermission : listPermissions) {
				role.addPermissions(toEntity(loopJsonPermission));
			}
		}
		return role;
		
	}		
	public static Role toEntity(JsonRole jsonRole) {
		Role role = new Role();
		
		return apply(role, jsonRole);
	}		
	
	public static List<JsonRole> toListJsonRoles(List<Role> all) {
		List<JsonRole> jsonRoles = new ArrayList<JsonRole>();
		for (Role role : all) {
			jsonRoles.add(toJson(role));
		}
		return jsonRoles;
	}
	//converte de entidade para json --------------------
	private static JsonSession toBasicJson(Session session) {
		JsonSession jsonSession = new JsonSession();
		applyBasicJsonValues(jsonSession, session);
		return jsonSession;
	}
	
	private static Session toBasicEntity(JsonSession jsonSession) {
		Session session = new Session();
		applyBasicEntityValues(session, jsonSession);
		return session;
	}
	
	private static void applyBasicJsonValues(JsonSession jsonSession, Session session) {
		jsonSession.setId(session.getId());
	    jsonSession.setName(session.getName());
	    jsonSession.setCreationDate(DateUtil.localDateTimeAsString(session.getCreationDate()));
	}	
	private static void applyBasicEntityValues(Session session, JsonSession jsonSession) {
		session.setId(jsonSession.getId());
		session.setName(jsonSession.getName());
	    session.setCreationDate(DateUtil.stringAsLocalDateTime(jsonSession.getCreationDate()));
	}	
	
	public static JsonSession toJson(Session session) {
		JsonSession jsonSession = new JsonSession();

		applyBasicJsonValues(jsonSession, session);

		List<Role> listRoles = session.getRoles();
		if (listRoles != null) {
			for (Role loopRole : listRoles) {
				jsonSession.getRoles().add(toJson(loopRole));
			}
		}

		User user_ = session.getUser();
		if (user_ != null) {
			jsonSession.setUser(toJson(user_));
		}
		return jsonSession;
	}


	public static Session apply(Session session, JsonSession jsonSession) {
	
		if(session ==  null)
			session = new Session();
		
		applyBasicEntityValues(session, jsonSession) ;

		ArrayList<JsonRole> listRoles = jsonSession.getRoles();			
		if (listRoles != null) {
			for (JsonRole loopJsonRole : listRoles) {
				session.addRoles(toEntity(loopJsonRole));
			}
		}
		JsonUser user_ = jsonSession.getUser();
		if (user_ != null) {
			session.setUser(toEntity(user_));
		}	
		return session;
		
	}		
	public static Session toEntity(JsonSession jsonSession) {
		Session session = new Session();
		
		return apply(session, jsonSession);
	}		
	
	public static List<JsonSession> toListJsonSessions(List<Session> all) {
		List<JsonSession> jsonSessions = new ArrayList<JsonSession>();
		for (Session session : all) {
			jsonSessions.add(toJson(session));
		}
		return jsonSessions;
	}
	//converte de entidade para json --------------------
	private static JsonUser toBasicJson(User user) {
		JsonUser jsonUser = new JsonUser();
		applyBasicJsonValues(jsonUser, user);
		return jsonUser;
	}
	
	private static User toBasicEntity(JsonUser jsonUser) {
		User user = new User();
		applyBasicEntityValues(user, jsonUser);
		return user;
	}
	
	private static void applyBasicJsonValues(JsonUser jsonUser, User user) {
		jsonUser.setId(user.getId());
	    jsonUser.setName(user.getName());
	    jsonUser.setUsername(user.getUsername());
	    jsonUser.setPassword(user.getPassword());
	    jsonUser.setEnable(user.getEnable());
	    jsonUser.setImage(user.getImage());
	}	
	private static void applyBasicEntityValues(User user, JsonUser jsonUser) {
		user.setId(jsonUser.getId());
		user.setName(jsonUser.getName());
		user.setUsername(jsonUser.getUsername());
		user.setPassword(jsonUser.getPassword());
		user.setEnable(jsonUser.getEnable());
		user.setImage(jsonUser.getImage());
	}	
	
	public static JsonUser toJson(User user) {
		JsonUser jsonUser = new JsonUser();

		applyBasicJsonValues(jsonUser, user);

		List<Role> listRoles = user.getRoles();
		if (listRoles != null) {
			for (Role loopRole : listRoles) {
				jsonUser.getRoles().add(toJson(loopRole));
			}
		}

		return jsonUser;
	}


	public static User apply(User user, JsonUser jsonUser) {
	
		if(user ==  null)
			user = new User();
		
		applyBasicEntityValues(user, jsonUser) ;

		ArrayList<JsonRole> listRoles = jsonUser.getRoles();			
		if (listRoles != null) {
			for (JsonRole loopJsonRole : listRoles) {
				user.addRoles(toEntity(loopJsonRole));
			}
		}
		return user;
		
	}		
	public static User toEntity(JsonUser jsonUser) {
		User user = new User();
		
		return apply(user, jsonUser);
	}		
	
	public static List<JsonUser> toListJsonUsers(List<User> all) {
		List<JsonUser> jsonUsers = new ArrayList<JsonUser>();
		for (User user : all) {
			jsonUsers.add(toJson(user));
		}
		return jsonUsers;
	}


}
