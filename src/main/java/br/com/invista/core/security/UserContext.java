package br.com.invista.core.security;

import br.com.invista.model.User;

public interface UserContext {

	User getCurrentUser();

	String getCurrentUserName();
	
	void setCurrentUser(User user);
}
