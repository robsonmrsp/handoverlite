package br.com.invista.core.persistence;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Restrictions;
import org.joda.time.LocalDateTime;

import br.com.invista.core.persistence.pagination.Pagination;
import br.com.invista.core.persistence.pagination.PaginationParams;
import br.com.invista.core.persistence.pagination.Paginator;

//
@SuppressWarnings("unchecked")
public abstract class AccessibleHibernateDao<Entity> extends HibernateDao<Entity> {

	public AccessibleHibernateDao(Class<Entity> clazz) {
		super(clazz);
	}

}
