package br.com.invista.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.JoinTable;
import javax.persistence.ManyToOne;
import javax.persistence.ManyToMany;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import org.hibernate.annotations.Type;
import org.hibernate.envers.Audited;
import org.joda.time.LocalDate;
import org.joda.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import org.hibernate.annotations.Cascade;
import org.hibernate.annotations.CascadeType;


import br.com.invista.core.serialization.CustomLocalDateTimeSerializer;
import br.com.invista.core.serialization.CustomLocalDateSerializer;
import br.com.invista.core.model.AbstractTimestampEntity;
/**
* generated: 29/09/2016 12:48:00
**/
@Entity
@Audited
@Table(name = "FUNCIONARIO")
@SequenceGenerator(name = "FUNCIONARIO_SEQUENCE", sequenceName = "FUNCIONARIO_SEQUENCE")
public class Funcionario extends AbstractTimestampEntity{
	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "FUNCIONARIO_SEQUENCE")	
	private Integer id;
		
	@Column(name = "NOME")
	private String nome;		
		
	@Column(name = "EMAIL")
	private String email;		
		
	@Column(name = "TELEFONE")
	private String telefone;		
		
	@Column(name = "LIMITE_MENSAL")
	private Double limiteMensal;  			
		
	@Column(name = "CPF")
	private String cpf;		
		
	@Column(name = "MATRICULA")
	private String matricula;		
		
	@Column(name = "STATUS_FUNCIONARIO")
	private String statusFuncionario;		
		
	@Column(name = "AUTORIZADO_TICKET")
	private Boolean autorizadoTicket;  			
		
	@Column(name = "OBSERVACAO")
	private String observacao;		
	
	@ManyToOne
	@JoinColumn(name = "ID_EMPRESA")
	private Empresa empresa;		
	
	@ManyToOne
	@JoinColumn(name = "ID_CENTROCUSTO")
	private CentroCusto centroCusto;		
		
	public  Funcionario() {
		
	}
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}
	
	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}
	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}
	public String getTelefone() {
		return telefone;
	}

	public void setTelefone(String telefone) {
		this.telefone = telefone;
	}
	public Double getLimiteMensal() {
		return limiteMensal;
	}

	public void setLimiteMensal(Double limiteMensal) {
		this.limiteMensal = limiteMensal;
	}
	public String getCpf() {
		return cpf;
	}

	public void setCpf(String cpf) {
		this.cpf = cpf;
	}
	public String getMatricula() {
		return matricula;
	}

	public void setMatricula(String matricula) {
		this.matricula = matricula;
	}
	public String getStatusFuncionario() {
		return statusFuncionario;
	}

	public void setStatusFuncionario(String statusFuncionario) {
		this.statusFuncionario = statusFuncionario;
	}
	public Boolean getAutorizadoTicket() {
		return autorizadoTicket;
	}

	public void setAutorizadoTicket(Boolean autorizadoTicket) {
		this.autorizadoTicket = autorizadoTicket;
	}
	public String getObservacao() {
		return observacao;
	}

	public void setObservacao(String observacao) {
		this.observacao = observacao;
	}
	public Empresa getEmpresa() {
		return empresa;
	}
	
	public void setEmpresa(Empresa empresa) {
		this.empresa = empresa;
	}
	
	public CentroCusto getCentroCusto() {
		return centroCusto;
	}
	
	public void setCentroCusto(CentroCusto centroCusto) {
		this.centroCusto = centroCusto;
	}
	
	
}
