package br.com.invista.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.JoinTable;
import javax.persistence.ManyToOne;
import javax.persistence.ManyToMany;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import org.hibernate.annotations.Type;
import org.hibernate.envers.Audited;
import org.joda.time.LocalDate;
import org.joda.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import org.hibernate.annotations.Cascade;
import org.hibernate.annotations.CascadeType;


import br.com.invista.core.serialization.CustomLocalDateTimeSerializer;
import br.com.invista.core.serialization.CustomLocalDateSerializer;
import br.com.invista.core.model.AbstractTimestampEntity;
/**
* generated: 29/09/2016 12:48:00
**/
@Entity
@Audited
@Table(name = "EMPRESA")
@SequenceGenerator(name = "EMPRESA_SEQUENCE", sequenceName = "EMPRESA_SEQUENCE")
public class Empresa extends AbstractTimestampEntity{
	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "EMPRESA_SEQUENCE")	
	private Integer id;
		
	@Column(name = "CNPJ")
	private String cnpj;		
		
	@Column(name = "NUMERO_CONTRATO")
	private String numeroContrato;		
		
	@Column(name = "RAZAO_SOCIAL")
	private String razaoSocial;		
		
	@Column(name = "NOME_FANTASIA")
	private String nomeFantasia;		
		
	@Column(name = "UTILIZA_VOUCHER")
	private Boolean utilizaVoucher;  			
		
	@Column(name = "UTILIZA_TICKET")
	private Boolean utilizaTicket;  			
		
	@Column(name = "OBSERVACAO_TAXISTA")
	private String observacaoTaxista;		
		
	@Column(name = "OBSERVACAO_CENTRAL")
	private String observacaoCentral;		
		
	@Column(name = "PERCURSO_DESCONTO")
	private Double percursoDesconto;  			
		
	@Column(name = "INSCRICAO_MUNICIPAL")
	private String inscricaoMunicipal;		
		
	@Column(name = "INSCRICAO_ESTADUAL")
	private String inscricaoEstadual;		
		
	@Column(name = "NF")
	private Boolean nf;  			
		
	@Column(name = "PERCENTUAL_ISS")
	private Double percentualIss;  			
		
	@Column(name = "PERCENTUAL_IRF")
	private Double percentualIrf;  			
		
	@Column(name = "PERCENTUAL_INSS")
	private Double percentualInss;  			
		
	@Column(name = "DIA_VENCIMENTO")
	private Integer diaVencimento;  			
		
	@Column(name = "PERCENTUAL_MOTORISTA")
	private Double percentualMotorista;  			
		
	@Column(name = "BANCO")
	private Integer banco;  			
		
	@Column(name = "AGENCIA")
	private String agencia;		
		
	@Column(name = "CONTA")
	private String conta;		
		
	@Column(name = "EMAIL")
	private String email;		
	
	@OneToMany(mappedBy="empresa")
	private List<CentroCusto> centroCustos;		
	
	@OneToMany(mappedBy="empresa")
	private List<Funcionario> funcionarios;		
		
	public  Empresa() {
		
	}
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}
	
	public String getCnpj() {
		return cnpj;
	}

	public void setCnpj(String cnpj) {
		this.cnpj = cnpj;
	}
	public String getNumeroContrato() {
		return numeroContrato;
	}

	public void setNumeroContrato(String numeroContrato) {
		this.numeroContrato = numeroContrato;
	}
	public String getRazaoSocial() {
		return razaoSocial;
	}

	public void setRazaoSocial(String razaoSocial) {
		this.razaoSocial = razaoSocial;
	}
	public String getNomeFantasia() {
		return nomeFantasia;
	}

	public void setNomeFantasia(String nomeFantasia) {
		this.nomeFantasia = nomeFantasia;
	}
	public Boolean getUtilizaVoucher() {
		return utilizaVoucher;
	}

	public void setUtilizaVoucher(Boolean utilizaVoucher) {
		this.utilizaVoucher = utilizaVoucher;
	}
	public Boolean getUtilizaTicket() {
		return utilizaTicket;
	}

	public void setUtilizaTicket(Boolean utilizaTicket) {
		this.utilizaTicket = utilizaTicket;
	}
	public String getObservacaoTaxista() {
		return observacaoTaxista;
	}

	public void setObservacaoTaxista(String observacaoTaxista) {
		this.observacaoTaxista = observacaoTaxista;
	}
	public String getObservacaoCentral() {
		return observacaoCentral;
	}

	public void setObservacaoCentral(String observacaoCentral) {
		this.observacaoCentral = observacaoCentral;
	}
	public Double getPercursoDesconto() {
		return percursoDesconto;
	}

	public void setPercursoDesconto(Double percursoDesconto) {
		this.percursoDesconto = percursoDesconto;
	}
	public String getInscricaoMunicipal() {
		return inscricaoMunicipal;
	}

	public void setInscricaoMunicipal(String inscricaoMunicipal) {
		this.inscricaoMunicipal = inscricaoMunicipal;
	}
	public String getInscricaoEstadual() {
		return inscricaoEstadual;
	}

	public void setInscricaoEstadual(String inscricaoEstadual) {
		this.inscricaoEstadual = inscricaoEstadual;
	}
	public Boolean getNf() {
		return nf;
	}

	public void setNf(Boolean nf) {
		this.nf = nf;
	}
	public Double getPercentualIss() {
		return percentualIss;
	}

	public void setPercentualIss(Double percentualIss) {
		this.percentualIss = percentualIss;
	}
	public Double getPercentualIrf() {
		return percentualIrf;
	}

	public void setPercentualIrf(Double percentualIrf) {
		this.percentualIrf = percentualIrf;
	}
	public Double getPercentualInss() {
		return percentualInss;
	}

	public void setPercentualInss(Double percentualInss) {
		this.percentualInss = percentualInss;
	}
	public Integer getDiaVencimento() {
		return diaVencimento;
	}

	public void setDiaVencimento(Integer diaVencimento) {
		this.diaVencimento = diaVencimento;
	}
	public Double getPercentualMotorista() {
		return percentualMotorista;
	}

	public void setPercentualMotorista(Double percentualMotorista) {
		this.percentualMotorista = percentualMotorista;
	}
	public Integer getBanco() {
		return banco;
	}

	public void setBanco(Integer banco) {
		this.banco = banco;
	}
	public String getAgencia() {
		return agencia;
	}

	public void setAgencia(String agencia) {
		this.agencia = agencia;
	}
	public String getConta() {
		return conta;
	}

	public void setConta(String conta) {
		this.conta = conta;
	}
	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}
	public void setCentroCustos(List<CentroCusto> centroCustos){
		this.centroCustos = centroCustos;
	}
	
	public List<CentroCusto>  getCentroCustos() {
		if(this.centroCustos == null){
			setCentroCustos(new ArrayList<CentroCusto>());
		}
		return this.centroCustos;
	}
		
	public boolean addCentroCustos(CentroCusto centroCusto){
		return getCentroCustos().add(centroCusto);
	}
	
	public boolean removeCentroCustos(CentroCusto centroCusto){
		return getCentroCustos().remove(centroCusto);
	}
	
	public void setFuncionarios(List<Funcionario> funcionarios){
		this.funcionarios = funcionarios;
	}
	
	public List<Funcionario>  getFuncionarios() {
		if(this.funcionarios == null){
			setFuncionarios(new ArrayList<Funcionario>());
		}
		return this.funcionarios;
	}
		
	public boolean addFuncionarios(Funcionario funcionario){
		return getFuncionarios().add(funcionario);
	}
	
	public boolean removeFuncionarios(Funcionario funcionario){
		return getFuncionarios().remove(funcionario);
	}
	
	
}
