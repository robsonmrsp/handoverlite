package br.com.invista.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.JoinTable;
import javax.persistence.ManyToOne;
import javax.persistence.ManyToMany;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import org.hibernate.annotations.Type;
import org.hibernate.envers.Audited;
import org.joda.time.LocalDate;
import org.joda.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import org.hibernate.annotations.Cascade;
import org.hibernate.annotations.CascadeType;


import br.com.invista.core.serialization.CustomLocalDateTimeSerializer;
import br.com.invista.core.serialization.CustomLocalDateSerializer;
import br.com.invista.core.model.AbstractTimestampEntity;
/**
* generated: 29/09/2016 12:48:00
**/
@Entity
@Audited
@Table(name = "CENTRO_CUSTO")
@SequenceGenerator(name = "CENTROCUSTO_SEQUENCE", sequenceName = "CENTROCUSTO_SEQUENCE")
public class CentroCusto extends AbstractTimestampEntity{
	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "CENTROCUSTO_SEQUENCE")	
	private Integer id;
		
	@Column(name = "DESCRICAO")
	private String descricao;		
		
	@Column(name = "VALOR_LIMITE")
	private Double valorLimite;  			
		
	@Column(name = "OBSERVACAO")
	private String observacao;		
		
	@Column(name = "OBSERACAO_TAXISTA")
	private String obseracaoTaxista;		
		
	@Column(name = "INATIVO")
	private Boolean inativo;  			
	
	@OneToMany(mappedBy="centroCusto")
	private List<Funcionario> funcionarios;		
	
	@ManyToOne
	@JoinColumn(name = "ID_EMPRESA")
	private Empresa empresa;		
		
	public  CentroCusto() {
		
	}
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}
	
	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}
	public Double getValorLimite() {
		return valorLimite;
	}

	public void setValorLimite(Double valorLimite) {
		this.valorLimite = valorLimite;
	}
	public String getObservacao() {
		return observacao;
	}

	public void setObservacao(String observacao) {
		this.observacao = observacao;
	}
	public String getObseracaoTaxista() {
		return obseracaoTaxista;
	}

	public void setObseracaoTaxista(String obseracaoTaxista) {
		this.obseracaoTaxista = obseracaoTaxista;
	}
	public Boolean getInativo() {
		return inativo;
	}

	public void setInativo(Boolean inativo) {
		this.inativo = inativo;
	}
	public void setFuncionarios(List<Funcionario> funcionarios){
		this.funcionarios = funcionarios;
	}
	
	public List<Funcionario>  getFuncionarios() {
		if(this.funcionarios == null){
			setFuncionarios(new ArrayList<Funcionario>());
		}
		return this.funcionarios;
	}
		
	public boolean addFuncionarios(Funcionario funcionario){
		return getFuncionarios().add(funcionario);
	}
	
	public boolean removeFuncionarios(Funcionario funcionario){
		return getFuncionarios().remove(funcionario);
	}
	
	public Empresa getEmpresa() {
		return empresa;
	}
	
	public void setEmpresa(Empresa empresa) {
		this.empresa = empresa;
	}
	
	
}
