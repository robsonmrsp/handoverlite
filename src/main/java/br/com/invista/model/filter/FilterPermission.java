package br.com.invista.model.filter;

import java.io.Serializable;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import org.joda.time.LocalDate;
import org.joda.time.LocalDateTime;


import br.com.invista.core.serialization.CustomLocalDateSerializer;
import br.com.invista.core.serialization.CustomLocalDateDeserializer;
import br.com.invista.core.serialization.CustomLocalDateTimeSerializer;
import br.com.invista.core.serialization.CustomLocalDateTimeDeserializer;


/**
*  generated: 29/09/2016 12:48:00
**/
public class FilterPermission implements Serializable {
	private static final long serialVersionUID = 1L;
	
	private String name;  			

	private Integer operation;		
	private Integer item;		
	
	public  FilterPermission() {
		
	}
	

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
		
	public Integer getOperation() {
		return operation;
	}
	
	public void setOperation(Integer operation) {
		this.operation = operation;
	}
	public Integer getItem() {
		return item;
	}
	
	public void setItem(Integer item) {
		this.item = item;
	}
	
}