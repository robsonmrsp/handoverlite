package br.com.invista.model.filter;

import java.io.Serializable;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import org.joda.time.LocalDate;
import org.joda.time.LocalDateTime;


import br.com.invista.core.serialization.CustomLocalDateSerializer;
import br.com.invista.core.serialization.CustomLocalDateDeserializer;
import br.com.invista.core.serialization.CustomLocalDateTimeSerializer;
import br.com.invista.core.serialization.CustomLocalDateTimeDeserializer;


/**
*  generated: 29/09/2016 12:48:00
**/
public class FilterCentroCusto implements Serializable {
	private static final long serialVersionUID = 1L;
	
	private String descricao;  			
	
	private Double valorLimite;  			
	
	private String observacao;  			
	
	private String obseracaoTaxista;  			
	
	private Boolean inativo;  			

	private Integer empresa;		
	
	public  FilterCentroCusto() {
		
	}
	

	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}
	public Double getValorLimite() {
		return valorLimite;
	}

	public void setValorLimite(Double valorLimite) {
		this.valorLimite = valorLimite;
	}
	public String getObservacao() {
		return observacao;
	}

	public void setObservacao(String observacao) {
		this.observacao = observacao;
	}
	public String getObseracaoTaxista() {
		return obseracaoTaxista;
	}

	public void setObseracaoTaxista(String obseracaoTaxista) {
		this.obseracaoTaxista = obseracaoTaxista;
	}
	public Boolean getInativo() {
		return inativo;
	}

	public void setInativo(Boolean inativo) {
		this.inativo = inativo;
	}
		
	public Integer getEmpresa() {
		return empresa;
	}
	
	public void setEmpresa(Integer empresa) {
		this.empresa = empresa;
	}
	
}