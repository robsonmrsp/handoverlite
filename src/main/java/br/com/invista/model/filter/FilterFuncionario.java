package br.com.invista.model.filter;

import java.io.Serializable;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import org.joda.time.LocalDate;
import org.joda.time.LocalDateTime;


import br.com.invista.core.serialization.CustomLocalDateSerializer;
import br.com.invista.core.serialization.CustomLocalDateDeserializer;
import br.com.invista.core.serialization.CustomLocalDateTimeSerializer;
import br.com.invista.core.serialization.CustomLocalDateTimeDeserializer;


/**
*  generated: 29/09/2016 12:48:00
**/
public class FilterFuncionario implements Serializable {
	private static final long serialVersionUID = 1L;
	
	private String nome;  			
	
	private String email;  			
	
	private String telefone;  			
	
	private Double limiteMensal;  			
	
	private String cpf;  			
	
	private String matricula;  			
	
	private String statusFuncionario;  			
	
	private Boolean autorizadoTicket;  			
	
	private String observacao;  			

	private Integer empresa;		
	private Integer centroCusto;		
	
	public  FilterFuncionario() {
		
	}
	

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}
	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}
	public String getTelefone() {
		return telefone;
	}

	public void setTelefone(String telefone) {
		this.telefone = telefone;
	}
	public Double getLimiteMensal() {
		return limiteMensal;
	}

	public void setLimiteMensal(Double limiteMensal) {
		this.limiteMensal = limiteMensal;
	}
	public String getCpf() {
		return cpf;
	}

	public void setCpf(String cpf) {
		this.cpf = cpf;
	}
	public String getMatricula() {
		return matricula;
	}

	public void setMatricula(String matricula) {
		this.matricula = matricula;
	}
	public String getStatusFuncionario() {
		return statusFuncionario;
	}

	public void setStatusFuncionario(String statusFuncionario) {
		this.statusFuncionario = statusFuncionario;
	}
	public Boolean getAutorizadoTicket() {
		return autorizadoTicket;
	}

	public void setAutorizadoTicket(Boolean autorizadoTicket) {
		this.autorizadoTicket = autorizadoTicket;
	}
	public String getObservacao() {
		return observacao;
	}

	public void setObservacao(String observacao) {
		this.observacao = observacao;
	}
		
	public Integer getEmpresa() {
		return empresa;
	}
	
	public void setEmpresa(Integer empresa) {
		this.empresa = empresa;
	}
	public Integer getCentroCusto() {
		return centroCusto;
	}
	
	public void setCentroCusto(Integer centroCusto) {
		this.centroCusto = centroCusto;
	}
	
}