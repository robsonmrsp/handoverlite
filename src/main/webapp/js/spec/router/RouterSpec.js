define(function(require) {
	var Router = require('Router');
	describe("Rotas", function() {

		beforeEach(function() {
			try {
				Backbone.history.stop();
			} catch (e) {
				console.error(e);
			}
		});
		
		afterEach(function() {
			// Reset URL
			var router = new Router();
			router.navigate("");
		});
				it("Rota de \"CentroCustos\"", function() {
			spyOn(Router.prototype, "CentroCustos")
			var router = new Router();
			Backbone.history.start();
			router.navigate('app/CentroCustos', true);
			expect(Router.prototype.CentroCustos).toHaveBeenCalled();
		});

		it("Rota de \"newcentroCusto\"", function() {
			spyOn(Router.prototype, "newcentroCusto")
			var router = new Router();
			Backbone.history.start();
			router.navigate('app/newcentroCusto', true);
			expect(Router.prototype.newcentroCusto).toHaveBeenCalled();
		});
		
		it("Rota de \"editcentroCusto\"", function() {
			spyOn(Router.prototype, "editcentroCusto")
			var router = new Router();
			Backbone.history.start();
			router.navigate('app/editcentroCusto/1', true);
			expect(Router.prototype.editcentroCusto).toHaveBeenCalled();
		});
		it("Rota de \"Empresas\"", function() {
			spyOn(Router.prototype, "Empresas")
			var router = new Router();
			Backbone.history.start();
			router.navigate('app/Empresas', true);
			expect(Router.prototype.Empresas).toHaveBeenCalled();
		});

		it("Rota de \"newempresa\"", function() {
			spyOn(Router.prototype, "newempresa")
			var router = new Router();
			Backbone.history.start();
			router.navigate('app/newempresa', true);
			expect(Router.prototype.newempresa).toHaveBeenCalled();
		});
		
		it("Rota de \"editempresa\"", function() {
			spyOn(Router.prototype, "editempresa")
			var router = new Router();
			Backbone.history.start();
			router.navigate('app/editempresa/1', true);
			expect(Router.prototype.editempresa).toHaveBeenCalled();
		});
		it("Rota de \"Funcionarios\"", function() {
			spyOn(Router.prototype, "Funcionarios")
			var router = new Router();
			Backbone.history.start();
			router.navigate('app/Funcionarios', true);
			expect(Router.prototype.Funcionarios).toHaveBeenCalled();
		});

		it("Rota de \"newfuncionario\"", function() {
			spyOn(Router.prototype, "newfuncionario")
			var router = new Router();
			Backbone.history.start();
			router.navigate('app/newfuncionario', true);
			expect(Router.prototype.newfuncionario).toHaveBeenCalled();
		});
		
		it("Rota de \"editfuncionario\"", function() {
			spyOn(Router.prototype, "editfuncionario")
			var router = new Router();
			Backbone.history.start();
			router.navigate('app/editfuncionario/1', true);
			expect(Router.prototype.editfuncionario).toHaveBeenCalled();
		});
		it("Rota de \"Bairros\"", function() {
			spyOn(Router.prototype, "Bairros")
			var router = new Router();
			Backbone.history.start();
			router.navigate('app/Bairros', true);
			expect(Router.prototype.Bairros).toHaveBeenCalled();
		});

		it("Rota de \"newbairro\"", function() {
			spyOn(Router.prototype, "newbairro")
			var router = new Router();
			Backbone.history.start();
			router.navigate('app/newbairro', true);
			expect(Router.prototype.newbairro).toHaveBeenCalled();
		});
		
		it("Rota de \"editbairro\"", function() {
			spyOn(Router.prototype, "editbairro")
			var router = new Router();
			Backbone.history.start();
			router.navigate('app/editbairro/1', true);
			expect(Router.prototype.editbairro).toHaveBeenCalled();
		});
		it("Rota de \"Ceps\"", function() {
			spyOn(Router.prototype, "Ceps")
			var router = new Router();
			Backbone.history.start();
			router.navigate('app/Ceps', true);
			expect(Router.prototype.Ceps).toHaveBeenCalled();
		});

		it("Rota de \"newcep\"", function() {
			spyOn(Router.prototype, "newcep")
			var router = new Router();
			Backbone.history.start();
			router.navigate('app/newcep', true);
			expect(Router.prototype.newcep).toHaveBeenCalled();
		});
		
		it("Rota de \"editcep\"", function() {
			spyOn(Router.prototype, "editcep")
			var router = new Router();
			Backbone.history.start();
			router.navigate('app/editcep/1', true);
			expect(Router.prototype.editcep).toHaveBeenCalled();
		});
		it("Rota de \"Cidades\"", function() {
			spyOn(Router.prototype, "Cidades")
			var router = new Router();
			Backbone.history.start();
			router.navigate('app/Cidades', true);
			expect(Router.prototype.Cidades).toHaveBeenCalled();
		});

		it("Rota de \"newcidade\"", function() {
			spyOn(Router.prototype, "newcidade")
			var router = new Router();
			Backbone.history.start();
			router.navigate('app/newcidade', true);
			expect(Router.prototype.newcidade).toHaveBeenCalled();
		});
		
		it("Rota de \"editcidade\"", function() {
			spyOn(Router.prototype, "editcidade")
			var router = new Router();
			Backbone.history.start();
			router.navigate('app/editcidade/1', true);
			expect(Router.prototype.editcidade).toHaveBeenCalled();
		});
		it("Rota de \"Enderecos\"", function() {
			spyOn(Router.prototype, "Enderecos")
			var router = new Router();
			Backbone.history.start();
			router.navigate('app/Enderecos', true);
			expect(Router.prototype.Enderecos).toHaveBeenCalled();
		});

		it("Rota de \"newendereco\"", function() {
			spyOn(Router.prototype, "newendereco")
			var router = new Router();
			Backbone.history.start();
			router.navigate('app/newendereco', true);
			expect(Router.prototype.newendereco).toHaveBeenCalled();
		});
		
		it("Rota de \"editendereco\"", function() {
			spyOn(Router.prototype, "editendereco")
			var router = new Router();
			Backbone.history.start();
			router.navigate('app/editendereco/1', true);
			expect(Router.prototype.editendereco).toHaveBeenCalled();
		});
		it("Rota de \"Estados\"", function() {
			spyOn(Router.prototype, "Estados")
			var router = new Router();
			Backbone.history.start();
			router.navigate('app/Estados', true);
			expect(Router.prototype.Estados).toHaveBeenCalled();
		});

		it("Rota de \"newestado\"", function() {
			spyOn(Router.prototype, "newestado")
			var router = new Router();
			Backbone.history.start();
			router.navigate('app/newestado', true);
			expect(Router.prototype.newestado).toHaveBeenCalled();
		});
		
		it("Rota de \"editestado\"", function() {
			spyOn(Router.prototype, "editestado")
			var router = new Router();
			Backbone.history.start();
			router.navigate('app/editestado/1', true);
			expect(Router.prototype.editestado).toHaveBeenCalled();
		});
		it("Rota de \"Paiss\"", function() {
			spyOn(Router.prototype, "Paiss")
			var router = new Router();
			Backbone.history.start();
			router.navigate('app/Paiss', true);
			expect(Router.prototype.Paiss).toHaveBeenCalled();
		});

		it("Rota de \"newpais\"", function() {
			spyOn(Router.prototype, "newpais")
			var router = new Router();
			Backbone.history.start();
			router.navigate('app/newpais', true);
			expect(Router.prototype.newpais).toHaveBeenCalled();
		});
		
		it("Rota de \"editpais\"", function() {
			spyOn(Router.prototype, "editpais")
			var router = new Router();
			Backbone.history.start();
			router.navigate('app/editpais/1', true);
			expect(Router.prototype.editpais).toHaveBeenCalled();
		});
		it("Rota de \"Items\"", function() {
			spyOn(Router.prototype, "Items")
			var router = new Router();
			Backbone.history.start();
			router.navigate('app/Items', true);
			expect(Router.prototype.Items).toHaveBeenCalled();
		});

		it("Rota de \"newitem\"", function() {
			spyOn(Router.prototype, "newitem")
			var router = new Router();
			Backbone.history.start();
			router.navigate('app/newitem', true);
			expect(Router.prototype.newitem).toHaveBeenCalled();
		});
		
		it("Rota de \"edititem\"", function() {
			spyOn(Router.prototype, "edititem")
			var router = new Router();
			Backbone.history.start();
			router.navigate('app/edititem/1', true);
			expect(Router.prototype.edititem).toHaveBeenCalled();
		});
		it("Rota de \"ItemTypes\"", function() {
			spyOn(Router.prototype, "ItemTypes")
			var router = new Router();
			Backbone.history.start();
			router.navigate('app/ItemTypes', true);
			expect(Router.prototype.ItemTypes).toHaveBeenCalled();
		});

		it("Rota de \"newitemType\"", function() {
			spyOn(Router.prototype, "newitemType")
			var router = new Router();
			Backbone.history.start();
			router.navigate('app/newitemType', true);
			expect(Router.prototype.newitemType).toHaveBeenCalled();
		});
		
		it("Rota de \"edititemType\"", function() {
			spyOn(Router.prototype, "edititemType")
			var router = new Router();
			Backbone.history.start();
			router.navigate('app/edititemType/1', true);
			expect(Router.prototype.edititemType).toHaveBeenCalled();
		});
		it("Rota de \"Operations\"", function() {
			spyOn(Router.prototype, "Operations")
			var router = new Router();
			Backbone.history.start();
			router.navigate('app/Operations', true);
			expect(Router.prototype.Operations).toHaveBeenCalled();
		});

		it("Rota de \"newoperation\"", function() {
			spyOn(Router.prototype, "newoperation")
			var router = new Router();
			Backbone.history.start();
			router.navigate('app/newoperation', true);
			expect(Router.prototype.newoperation).toHaveBeenCalled();
		});
		
		it("Rota de \"editoperation\"", function() {
			spyOn(Router.prototype, "editoperation")
			var router = new Router();
			Backbone.history.start();
			router.navigate('app/editoperation/1', true);
			expect(Router.prototype.editoperation).toHaveBeenCalled();
		});
		it("Rota de \"Permissions\"", function() {
			spyOn(Router.prototype, "Permissions")
			var router = new Router();
			Backbone.history.start();
			router.navigate('app/Permissions', true);
			expect(Router.prototype.Permissions).toHaveBeenCalled();
		});

		it("Rota de \"newpermission\"", function() {
			spyOn(Router.prototype, "newpermission")
			var router = new Router();
			Backbone.history.start();
			router.navigate('app/newpermission', true);
			expect(Router.prototype.newpermission).toHaveBeenCalled();
		});
		
		it("Rota de \"editpermission\"", function() {
			spyOn(Router.prototype, "editpermission")
			var router = new Router();
			Backbone.history.start();
			router.navigate('app/editpermission/1', true);
			expect(Router.prototype.editpermission).toHaveBeenCalled();
		});
		it("Rota de \"Roles\"", function() {
			spyOn(Router.prototype, "Roles")
			var router = new Router();
			Backbone.history.start();
			router.navigate('app/Roles', true);
			expect(Router.prototype.Roles).toHaveBeenCalled();
		});

		it("Rota de \"newrole\"", function() {
			spyOn(Router.prototype, "newrole")
			var router = new Router();
			Backbone.history.start();
			router.navigate('app/newrole', true);
			expect(Router.prototype.newrole).toHaveBeenCalled();
		});
		
		it("Rota de \"editrole\"", function() {
			spyOn(Router.prototype, "editrole")
			var router = new Router();
			Backbone.history.start();
			router.navigate('app/editrole/1', true);
			expect(Router.prototype.editrole).toHaveBeenCalled();
		});
		it("Rota de \"Sessions\"", function() {
			spyOn(Router.prototype, "Sessions")
			var router = new Router();
			Backbone.history.start();
			router.navigate('app/Sessions', true);
			expect(Router.prototype.Sessions).toHaveBeenCalled();
		});

		it("Rota de \"newsession\"", function() {
			spyOn(Router.prototype, "newsession")
			var router = new Router();
			Backbone.history.start();
			router.navigate('app/newsession', true);
			expect(Router.prototype.newsession).toHaveBeenCalled();
		});
		
		it("Rota de \"editsession\"", function() {
			spyOn(Router.prototype, "editsession")
			var router = new Router();
			Backbone.history.start();
			router.navigate('app/editsession/1', true);
			expect(Router.prototype.editsession).toHaveBeenCalled();
		});
		it("Rota de \"Users\"", function() {
			spyOn(Router.prototype, "Users")
			var router = new Router();
			Backbone.history.start();
			router.navigate('app/Users', true);
			expect(Router.prototype.Users).toHaveBeenCalled();
		});

		it("Rota de \"newuser\"", function() {
			spyOn(Router.prototype, "newuser")
			var router = new Router();
			Backbone.history.start();
			router.navigate('app/newuser', true);
			expect(Router.prototype.newuser).toHaveBeenCalled();
		});
		
		it("Rota de \"edituser\"", function() {
			spyOn(Router.prototype, "edituser")
			var router = new Router();
			Backbone.history.start();
			router.navigate('app/edituser/1', true);
			expect(Router.prototype.edituser).toHaveBeenCalled();
		});
	});
})
