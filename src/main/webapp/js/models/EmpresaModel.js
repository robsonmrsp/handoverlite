/* generated: 29/09/2016 12:48:00 */
define(function(require) {
	// Start "Import´s" Definition"
	var _ = require('adapters/underscore-adapter');
	var $ = require('adapters/jquery-adapter');
	var Backbone = require('adapters/backbone-adapter');
	var BaseModel = require('models/BaseModel');
	// End of "Import´s definition"

	// #####################################################################################################
	// ¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨ MAIN BODY  ¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨
	// #####################################################################################################

	var EmpresaModel = BaseModel.extend({

		urlRoot : 'rs/crud/empresas',

		defaults : {
			id: null,
	    	cnpj : '',    	
	    	numeroContrato : '',    	
	    	razaoSocial : '',    	
	    	nomeFantasia : '',    	
	    	utilizaVoucher : '',    	
	    	utilizaTicket : '',    	
	    	observacaoTaxista : '',    	
	    	observacaoCentral : '',    	
	    	percursoDesconto : '',    	
	    	inscricaoMunicipal : '',    	
	    	inscricaoEstadual : '',    	
	    	nf : '',    	
	    	percentualIss : '',    	
	    	percentualIrf : '',    	
	    	percentualInss : '',    	
	    	diaVencimento : '',    	
	    	percentualMotorista : '',    	
	    	banco : '',    	
	    	agencia : '',    	
	    	conta : '',    	
	    	email : '',    	
			centroCustos : null,
			funcionarios : null,
		
		}
	});
	return EmpresaModel;
});
