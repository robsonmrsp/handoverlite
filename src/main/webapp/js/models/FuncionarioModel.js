/* generated: 29/09/2016 12:48:00 */
define(function(require) {
	// Start "Import´s" Definition"
	var _ = require('adapters/underscore-adapter');
	var $ = require('adapters/jquery-adapter');
	var Backbone = require('adapters/backbone-adapter');
	var BaseModel = require('models/BaseModel');
	// End of "Import´s definition"

	// #####################################################################################################
	// ¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨ MAIN BODY  ¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨
	// #####################################################################################################

	var FuncionarioModel = BaseModel.extend({

		urlRoot : 'rs/crud/funcionarios',

		defaults : {
			id: null,
	    	nome : '',    	
	    	email : '',    	
	    	telefone : '',    	
	    	limiteMensal : '',    	
	    	cpf : '',    	
	    	matricula : '',    	
	    	statusFuncionario : '',    	
	    	autorizadoTicket : '',    	
	    	observacao : '',    	
			empresa : null,
			centroCusto : null,
		
		}
	});
	return FuncionarioModel;
});
