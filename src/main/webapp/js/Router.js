define(function(require) {
	var $ = require('adapters/jquery-adapter');
	var _ = require('adapters/underscore-adapter');
	var Backbone = require('adapters/backbone-adapter');
	var Marionette = require('marionette');
	var util = require('utilities/utils');
		
	var PageCentroCusto = require('views/centroCusto/PageCentroCusto');
	var FormCentroCusto = require('views/centroCusto/FormCentroCusto');
	var CentroCustoModel = require('models/CentroCustoModel');
	
	var PageEmpresa = require('views/empresa/PageEmpresa');
	var FormEmpresa = require('views/empresa/FormEmpresa');
	var EmpresaModel = require('models/EmpresaModel');
	
	var PageFuncionario = require('views/funcionario/PageFuncionario');
	var FormFuncionario = require('views/funcionario/FormFuncionario');
	var FuncionarioModel = require('models/FuncionarioModel');
	
	var PageBairro = require('views/bairro/PageBairro');
	var FormBairro = require('views/bairro/FormBairro');
	var BairroModel = require('models/BairroModel');
	
	var PageCep = require('views/cep/PageCep');
	var FormCep = require('views/cep/FormCep');
	var CepModel = require('models/CepModel');
	
	var PageCidade = require('views/cidade/PageCidade');
	var FormCidade = require('views/cidade/FormCidade');
	var CidadeModel = require('models/CidadeModel');
	
	var PageEndereco = require('views/endereco/PageEndereco');
	var FormEndereco = require('views/endereco/FormEndereco');
	var EnderecoModel = require('models/EnderecoModel');
	
	var PageEstado = require('views/estado/PageEstado');
	var FormEstado = require('views/estado/FormEstado');
	var EstadoModel = require('models/EstadoModel');
	
	var PagePais = require('views/pais/PagePais');
	var FormPais = require('views/pais/FormPais');
	var PaisModel = require('models/PaisModel');
	
	var PageItem = require('views/item/PageItem');
	var FormItem = require('views/item/FormItem');
	var ItemModel = require('models/ItemModel');
	
	var PageItemType = require('views/itemType/PageItemType');
	var FormItemType = require('views/itemType/FormItemType');
	var ItemTypeModel = require('models/ItemTypeModel');
	
	var PageOperation = require('views/operation/PageOperation');
	var FormOperation = require('views/operation/FormOperation');
	var OperationModel = require('models/OperationModel');
	
	var PagePermission = require('views/permission/PagePermission');
	var FormPermission = require('views/permission/FormPermission');
	var PermissionModel = require('models/PermissionModel');
	
	var PageRole = require('views/role/PageRole');
	var FormRole = require('views/role/FormRole');
	var RoleModel = require('models/RoleModel');
	
	var PageSession = require('views/session/PageSession');
	var FormSession = require('views/session/FormSession');
	var SessionModel = require('models/SessionModel');
	
	var PageUser = require('views/user/PageUser');
	var FormUser = require('views/user/FormUser');
	var UserModel = require('models/UserModel');
	
	util.NProgress.setBlockerPanel('block_panel');
	
	var CustomRegion = Marionette.Region.extend({
		el : ".main-content",

		attachHtml : function(view) {
			this.$el.hide();
			this.$el.html(view.el);
			//this.$el.slideDown(300);
			//this.$el.show("slide", { direction: "up" }, 300);
			util.scrollTop();
			this.$el.fadeIn(300);
			view.listenTo(view, 'show', function() {
				setTimeout(function() {
					// ver tambem backbone-adapter
					util.NProgress.done(false, true);
					// uma pequena espera para garantir que o componente foi
					// renderizado antes de mandar remove-lo.
				}, 100);
			});
		},
	});

	var AppRouter = Backbone.Router.extend({
		routes : {
			'' : 'index',
			// hashs de CentroCusto
			'app/centroCustos' : 'centroCustos',
			'app/newCentroCusto' : 'newCentroCusto',
			'app/editCentroCusto/:id' : 'editCentroCusto',
			// hashs de Empresa
			'app/empresas' : 'empresas',
			'app/newEmpresa' : 'newEmpresa',
			'app/editEmpresa/:id' : 'editEmpresa',
			// hashs de Funcionario
			'app/funcionarios' : 'funcionarios',
			'app/newFuncionario' : 'newFuncionario',
			'app/editFuncionario/:id' : 'editFuncionario',
			// hashs de Bairro
			'app/bairros' : 'bairros',
			'app/newBairro' : 'newBairro',
			'app/editBairro/:id' : 'editBairro',
			// hashs de Cep
			'app/ceps' : 'ceps',
			'app/newCep' : 'newCep',
			'app/editCep/:id' : 'editCep',
			// hashs de Cidade
			'app/cidades' : 'cidades',
			'app/newCidade' : 'newCidade',
			'app/editCidade/:id' : 'editCidade',
			// hashs de Endereco
			'app/enderecos' : 'enderecos',
			'app/newEndereco' : 'newEndereco',
			'app/editEndereco/:id' : 'editEndereco',
			// hashs de Estado
			'app/estados' : 'estados',
			'app/newEstado' : 'newEstado',
			'app/editEstado/:id' : 'editEstado',
			// hashs de Pais
			'app/paiss' : 'paiss',
			'app/newPais' : 'newPais',
			'app/editPais/:id' : 'editPais',
			// hashs de Item
			'app/items' : 'items',
			'app/newItem' : 'newItem',
			'app/editItem/:id' : 'editItem',
			// hashs de ItemType
			'app/itemTypes' : 'itemTypes',
			'app/newItemType' : 'newItemType',
			'app/editItemType/:id' : 'editItemType',
			// hashs de Operation
			'app/operations' : 'operations',
			'app/newOperation' : 'newOperation',
			'app/editOperation/:id' : 'editOperation',
			// hashs de Permission
			'app/permissions' : 'permissions',
			'app/newPermission' : 'newPermission',
			'app/editPermission/:id' : 'editPermission',
			// hashs de Role
			'app/roles' : 'roles',
			'app/newRole' : 'newRole',
			'app/editRole/:id' : 'editRole',
			// hashs de Session
			'app/sessions' : 'sessions',
			'app/newSession' : 'newSession',
			'app/editSession/:id' : 'editSession',
			// hashs de User
			'app/users' : 'users',
			'app/newUser' : 'newUser',
			'app/editUser/:id' : 'editUser',
		},
		initialize : function() {
			this.App = new Marionette.Application();
			this.App.addRegions({
				mainRegion : CustomRegion
			});
			this.on('route', function(abc) {
				util.NProgress.start(true);
			});
		},

		index : function(path) {
			util.markActiveItem('dashboard');
			setTimeout(function() {
				util.NProgress.done(false, true);
			}, 500);
		},
		
		//configuração das rotas de CentroCusto
		centroCustos: function() {
			util.markActiveItem('centroCustos');
			this.pageCentroCusto = new PageCentroCusto();
			this.App.mainRegion.show(this.pageCentroCusto);
			util.breadcrumb({
				iconClass : 'fa-desktop',
				itemLabel : 'Centro custo',
				itemSubFolderName : 'Listagem',
				url : 'app/centroCustos'
			});
		},

		newCentroCusto: function() {
			util.markActiveItem('centroCustos');
			var formCentroCusto = new FormCentroCusto({
				model : new CentroCustoModel(),
			});
			this.App.mainRegion.show(formCentroCusto);
			util.breadcrumb({
				iconClass : 'fa-desktop',
				itemLabel : 'Centro custo',
				itemSubFolderName : 'Formulário de cadastro de Centro custo',
				url : 'app/centroCustos'
			});
		},
		
		editCentroCusto: function(idCentroCusto) {
			var that = this;
			util.markActiveItem('centroCustos');
			var formCentroCusto = null;
			if (this.pageCentroCusto) {
				formCentroCusto = new FormCentroCusto({
					model : this.pageCentroCusto.centroCustos.get(idCentroCusto),
				});
				that.App.mainRegion.show(formCentroCusto);
			} else {
				var model = new CentroCustoModel({
					id : idCentroCusto,
				})
				model.fetch({
					success : function(model) {
						formCentroCusto = new FormCentroCusto({
							model : model,
						});
						that.App.mainRegion.show(formCentroCusto);
					},
					error : function(x, y, z) {
						console.error(x, y, z);
					}
				})
				util.breadcrumb({
					iconClass : 'fa-calendar',
					itemLabel : 'CentroCustos',
					itemSubFolderName : 'Formulário de atualização de Centro custo',
					url : 'app/centroCustos'
				});
			}
		},
		
		//configuração das rotas de Empresa
		empresas: function() {
			util.markActiveItem('empresas');
			this.pageEmpresa = new PageEmpresa();
			this.App.mainRegion.show(this.pageEmpresa);
			util.breadcrumb({
				iconClass : 'fa-desktop',
				itemLabel : 'Empresa',
				itemSubFolderName : 'Listagem',
				url : 'app/empresas'
			});
		},

		newEmpresa: function() {
			util.markActiveItem('empresas');
			var formEmpresa = new FormEmpresa({
				model : new EmpresaModel(),
			});
			this.App.mainRegion.show(formEmpresa);
			util.breadcrumb({
				iconClass : 'fa-desktop',
				itemLabel : 'Empresa',
				itemSubFolderName : 'Formulário de cadastro de Empresa',
				url : 'app/empresas'
			});
		},
		
		editEmpresa: function(idEmpresa) {
			var that = this;
			util.markActiveItem('empresas');
			var formEmpresa = null;
			if (this.pageEmpresa) {
				formEmpresa = new FormEmpresa({
					model : this.pageEmpresa.empresas.get(idEmpresa),
				});
				that.App.mainRegion.show(formEmpresa);
			} else {
				var model = new EmpresaModel({
					id : idEmpresa,
				})
				model.fetch({
					success : function(model) {
						formEmpresa = new FormEmpresa({
							model : model,
						});
						that.App.mainRegion.show(formEmpresa);
					},
					error : function(x, y, z) {
						console.error(x, y, z);
					}
				})
				util.breadcrumb({
					iconClass : 'fa-calendar',
					itemLabel : 'Empresas',
					itemSubFolderName : 'Formulário de atualização de Empresa',
					url : 'app/empresas'
				});
			}
		},
		
		//configuração das rotas de Funcionario
		funcionarios: function() {
			util.markActiveItem('funcionarios');
			this.pageFuncionario = new PageFuncionario();
			this.App.mainRegion.show(this.pageFuncionario);
			util.breadcrumb({
				iconClass : 'fa-desktop',
				itemLabel : 'Funcionario',
				itemSubFolderName : 'Listagem',
				url : 'app/funcionarios'
			});
		},

		newFuncionario: function() {
			util.markActiveItem('funcionarios');
			var formFuncionario = new FormFuncionario({
				model : new FuncionarioModel(),
			});
			this.App.mainRegion.show(formFuncionario);
			util.breadcrumb({
				iconClass : 'fa-desktop',
				itemLabel : 'Funcionario',
				itemSubFolderName : 'Formulário de cadastro de Funcionario',
				url : 'app/funcionarios'
			});
		},
		
		editFuncionario: function(idFuncionario) {
			var that = this;
			util.markActiveItem('funcionarios');
			var formFuncionario = null;
			if (this.pageFuncionario) {
				formFuncionario = new FormFuncionario({
					model : this.pageFuncionario.funcionarios.get(idFuncionario),
				});
				that.App.mainRegion.show(formFuncionario);
			} else {
				var model = new FuncionarioModel({
					id : idFuncionario,
				})
				model.fetch({
					success : function(model) {
						formFuncionario = new FormFuncionario({
							model : model,
						});
						that.App.mainRegion.show(formFuncionario);
					},
					error : function(x, y, z) {
						console.error(x, y, z);
					}
				})
				util.breadcrumb({
					iconClass : 'fa-calendar',
					itemLabel : 'Funcionarios',
					itemSubFolderName : 'Formulário de atualização de Funcionario',
					url : 'app/funcionarios'
				});
			}
		},
		
		//configuração das rotas de Bairro
		bairros: function() {
			util.markActiveItem('bairros');
			this.pageBairro = new PageBairro();
			this.App.mainRegion.show(this.pageBairro);
			util.breadcrumb({
				iconClass : 'fa-desktop',
				itemLabel : 'Bairro',
				itemSubFolderName : 'Listagem',
				url : 'app/bairros'
			});
		},

		newBairro: function() {
			util.markActiveItem('bairros');
			var formBairro = new FormBairro({
				model : new BairroModel(),
			});
			this.App.mainRegion.show(formBairro);
			util.breadcrumb({
				iconClass : 'fa-desktop',
				itemLabel : 'Bairro',
				itemSubFolderName : 'Formulário de cadastro de Bairro',
				url : 'app/bairros'
			});
		},
		
		editBairro: function(idBairro) {
			var that = this;
			util.markActiveItem('bairros');
			var formBairro = null;
			if (this.pageBairro) {
				formBairro = new FormBairro({
					model : this.pageBairro.bairros.get(idBairro),
				});
				that.App.mainRegion.show(formBairro);
			} else {
				var model = new BairroModel({
					id : idBairro,
				})
				model.fetch({
					success : function(model) {
						formBairro = new FormBairro({
							model : model,
						});
						that.App.mainRegion.show(formBairro);
					},
					error : function(x, y, z) {
						console.error(x, y, z);
					}
				})
				util.breadcrumb({
					iconClass : 'fa-calendar',
					itemLabel : 'Bairros',
					itemSubFolderName : 'Formulário de atualização de Bairro',
					url : 'app/bairros'
				});
			}
		},
		
		//configuração das rotas de Cep
		ceps: function() {
			util.markActiveItem('ceps');
			this.pageCep = new PageCep();
			this.App.mainRegion.show(this.pageCep);
			util.breadcrumb({
				iconClass : 'fa-desktop',
				itemLabel : 'Cep',
				itemSubFolderName : 'Listagem',
				url : 'app/ceps'
			});
		},

		newCep: function() {
			util.markActiveItem('ceps');
			var formCep = new FormCep({
				model : new CepModel(),
			});
			this.App.mainRegion.show(formCep);
			util.breadcrumb({
				iconClass : 'fa-desktop',
				itemLabel : 'Cep',
				itemSubFolderName : 'Formulário de cadastro de Cep',
				url : 'app/ceps'
			});
		},
		
		editCep: function(idCep) {
			var that = this;
			util.markActiveItem('ceps');
			var formCep = null;
			if (this.pageCep) {
				formCep = new FormCep({
					model : this.pageCep.ceps.get(idCep),
				});
				that.App.mainRegion.show(formCep);
			} else {
				var model = new CepModel({
					id : idCep,
				})
				model.fetch({
					success : function(model) {
						formCep = new FormCep({
							model : model,
						});
						that.App.mainRegion.show(formCep);
					},
					error : function(x, y, z) {
						console.error(x, y, z);
					}
				})
				util.breadcrumb({
					iconClass : 'fa-calendar',
					itemLabel : 'Ceps',
					itemSubFolderName : 'Formulário de atualização de Cep',
					url : 'app/ceps'
				});
			}
		},
		
		//configuração das rotas de Cidade
		cidades: function() {
			util.markActiveItem('cidades');
			this.pageCidade = new PageCidade();
			this.App.mainRegion.show(this.pageCidade);
			util.breadcrumb({
				iconClass : 'fa-desktop',
				itemLabel : 'Cidade',
				itemSubFolderName : 'Listagem',
				url : 'app/cidades'
			});
		},

		newCidade: function() {
			util.markActiveItem('cidades');
			var formCidade = new FormCidade({
				model : new CidadeModel(),
			});
			this.App.mainRegion.show(formCidade);
			util.breadcrumb({
				iconClass : 'fa-desktop',
				itemLabel : 'Cidade',
				itemSubFolderName : 'Formulário de cadastro de Cidade',
				url : 'app/cidades'
			});
		},
		
		editCidade: function(idCidade) {
			var that = this;
			util.markActiveItem('cidades');
			var formCidade = null;
			if (this.pageCidade) {
				formCidade = new FormCidade({
					model : this.pageCidade.cidades.get(idCidade),
				});
				that.App.mainRegion.show(formCidade);
			} else {
				var model = new CidadeModel({
					id : idCidade,
				})
				model.fetch({
					success : function(model) {
						formCidade = new FormCidade({
							model : model,
						});
						that.App.mainRegion.show(formCidade);
					},
					error : function(x, y, z) {
						console.error(x, y, z);
					}
				})
				util.breadcrumb({
					iconClass : 'fa-calendar',
					itemLabel : 'Cidades',
					itemSubFolderName : 'Formulário de atualização de Cidade',
					url : 'app/cidades'
				});
			}
		},
		
		//configuração das rotas de Endereco
		enderecos: function() {
			util.markActiveItem('enderecos');
			this.pageEndereco = new PageEndereco();
			this.App.mainRegion.show(this.pageEndereco);
			util.breadcrumb({
				iconClass : 'fa-desktop',
				itemLabel : 'Endereco',
				itemSubFolderName : 'Listagem',
				url : 'app/enderecos'
			});
		},

		newEndereco: function() {
			util.markActiveItem('enderecos');
			var formEndereco = new FormEndereco({
				model : new EnderecoModel(),
			});
			this.App.mainRegion.show(formEndereco);
			util.breadcrumb({
				iconClass : 'fa-desktop',
				itemLabel : 'Endereco',
				itemSubFolderName : 'Formulário de cadastro de Endereco',
				url : 'app/enderecos'
			});
		},
		
		editEndereco: function(idEndereco) {
			var that = this;
			util.markActiveItem('enderecos');
			var formEndereco = null;
			if (this.pageEndereco) {
				formEndereco = new FormEndereco({
					model : this.pageEndereco.enderecos.get(idEndereco),
				});
				that.App.mainRegion.show(formEndereco);
			} else {
				var model = new EnderecoModel({
					id : idEndereco,
				})
				model.fetch({
					success : function(model) {
						formEndereco = new FormEndereco({
							model : model,
						});
						that.App.mainRegion.show(formEndereco);
					},
					error : function(x, y, z) {
						console.error(x, y, z);
					}
				})
				util.breadcrumb({
					iconClass : 'fa-calendar',
					itemLabel : 'Enderecos',
					itemSubFolderName : 'Formulário de atualização de Endereco',
					url : 'app/enderecos'
				});
			}
		},
		
		//configuração das rotas de Estado
		estados: function() {
			util.markActiveItem('estados');
			this.pageEstado = new PageEstado();
			this.App.mainRegion.show(this.pageEstado);
			util.breadcrumb({
				iconClass : 'fa-desktop',
				itemLabel : 'Estado',
				itemSubFolderName : 'Listagem',
				url : 'app/estados'
			});
		},

		newEstado: function() {
			util.markActiveItem('estados');
			var formEstado = new FormEstado({
				model : new EstadoModel(),
			});
			this.App.mainRegion.show(formEstado);
			util.breadcrumb({
				iconClass : 'fa-desktop',
				itemLabel : 'Estado',
				itemSubFolderName : 'Formulário de cadastro de Estado',
				url : 'app/estados'
			});
		},
		
		editEstado: function(idEstado) {
			var that = this;
			util.markActiveItem('estados');
			var formEstado = null;
			if (this.pageEstado) {
				formEstado = new FormEstado({
					model : this.pageEstado.estados.get(idEstado),
				});
				that.App.mainRegion.show(formEstado);
			} else {
				var model = new EstadoModel({
					id : idEstado,
				})
				model.fetch({
					success : function(model) {
						formEstado = new FormEstado({
							model : model,
						});
						that.App.mainRegion.show(formEstado);
					},
					error : function(x, y, z) {
						console.error(x, y, z);
					}
				})
				util.breadcrumb({
					iconClass : 'fa-calendar',
					itemLabel : 'Estados',
					itemSubFolderName : 'Formulário de atualização de Estado',
					url : 'app/estados'
				});
			}
		},
		
		//configuração das rotas de Pais
		paiss: function() {
			util.markActiveItem('paiss');
			this.pagePais = new PagePais();
			this.App.mainRegion.show(this.pagePais);
			util.breadcrumb({
				iconClass : 'fa-desktop',
				itemLabel : 'Pais',
				itemSubFolderName : 'Listagem',
				url : 'app/paiss'
			});
		},

		newPais: function() {
			util.markActiveItem('paiss');
			var formPais = new FormPais({
				model : new PaisModel(),
			});
			this.App.mainRegion.show(formPais);
			util.breadcrumb({
				iconClass : 'fa-desktop',
				itemLabel : 'Pais',
				itemSubFolderName : 'Formulário de cadastro de Pais',
				url : 'app/paiss'
			});
		},
		
		editPais: function(idPais) {
			var that = this;
			util.markActiveItem('paiss');
			var formPais = null;
			if (this.pagePais) {
				formPais = new FormPais({
					model : this.pagePais.paiss.get(idPais),
				});
				that.App.mainRegion.show(formPais);
			} else {
				var model = new PaisModel({
					id : idPais,
				})
				model.fetch({
					success : function(model) {
						formPais = new FormPais({
							model : model,
						});
						that.App.mainRegion.show(formPais);
					},
					error : function(x, y, z) {
						console.error(x, y, z);
					}
				})
				util.breadcrumb({
					iconClass : 'fa-calendar',
					itemLabel : 'Paiss',
					itemSubFolderName : 'Formulário de atualização de Pais',
					url : 'app/paiss'
				});
			}
		},
		
		//configuração das rotas de Item
		items: function() {
			util.markActiveItem('items');
			this.pageItem = new PageItem();
			this.App.mainRegion.show(this.pageItem);
			util.breadcrumb({
				iconClass : 'fa-desktop',
				itemLabel : 'Item',
				itemSubFolderName : 'Listagem',
				url : 'app/items'
			});
		},

		newItem: function() {
			util.markActiveItem('items');
			var formItem = new FormItem({
				model : new ItemModel(),
			});
			this.App.mainRegion.show(formItem);
			util.breadcrumb({
				iconClass : 'fa-desktop',
				itemLabel : 'Item',
				itemSubFolderName : 'Formulário de cadastro de Item',
				url : 'app/items'
			});
		},
		
		editItem: function(idItem) {
			var that = this;
			util.markActiveItem('items');
			var formItem = null;
			if (this.pageItem) {
				formItem = new FormItem({
					model : this.pageItem.items.get(idItem),
				});
				that.App.mainRegion.show(formItem);
			} else {
				var model = new ItemModel({
					id : idItem,
				})
				model.fetch({
					success : function(model) {
						formItem = new FormItem({
							model : model,
						});
						that.App.mainRegion.show(formItem);
					},
					error : function(x, y, z) {
						console.error(x, y, z);
					}
				})
				util.breadcrumb({
					iconClass : 'fa-calendar',
					itemLabel : 'Items',
					itemSubFolderName : 'Formulário de atualização de Item',
					url : 'app/items'
				});
			}
		},
		
		//configuração das rotas de ItemType
		itemTypes: function() {
			util.markActiveItem('itemTypes');
			this.pageItemType = new PageItemType();
			this.App.mainRegion.show(this.pageItemType);
			util.breadcrumb({
				iconClass : 'fa-desktop',
				itemLabel : 'Tipo de Item',
				itemSubFolderName : 'Listagem',
				url : 'app/itemTypes'
			});
		},

		newItemType: function() {
			util.markActiveItem('itemTypes');
			var formItemType = new FormItemType({
				model : new ItemTypeModel(),
			});
			this.App.mainRegion.show(formItemType);
			util.breadcrumb({
				iconClass : 'fa-desktop',
				itemLabel : 'Tipo de Item',
				itemSubFolderName : 'Formulário de cadastro de Tipo de Item',
				url : 'app/itemTypes'
			});
		},
		
		editItemType: function(idItemType) {
			var that = this;
			util.markActiveItem('itemTypes');
			var formItemType = null;
			if (this.pageItemType) {
				formItemType = new FormItemType({
					model : this.pageItemType.itemTypes.get(idItemType),
				});
				that.App.mainRegion.show(formItemType);
			} else {
				var model = new ItemTypeModel({
					id : idItemType,
				})
				model.fetch({
					success : function(model) {
						formItemType = new FormItemType({
							model : model,
						});
						that.App.mainRegion.show(formItemType);
					},
					error : function(x, y, z) {
						console.error(x, y, z);
					}
				})
				util.breadcrumb({
					iconClass : 'fa-calendar',
					itemLabel : 'ItemTypes',
					itemSubFolderName : 'Formulário de atualização de Tipo de Item',
					url : 'app/itemTypes'
				});
			}
		},
		
		//configuração das rotas de Operation
		operations: function() {
			util.markActiveItem('operations');
			this.pageOperation = new PageOperation();
			this.App.mainRegion.show(this.pageOperation);
			util.breadcrumb({
				iconClass : 'fa-desktop',
				itemLabel : 'Operação',
				itemSubFolderName : 'Listagem',
				url : 'app/operations'
			});
		},

		newOperation: function() {
			util.markActiveItem('operations');
			var formOperation = new FormOperation({
				model : new OperationModel(),
			});
			this.App.mainRegion.show(formOperation);
			util.breadcrumb({
				iconClass : 'fa-desktop',
				itemLabel : 'Operação',
				itemSubFolderName : 'Formulário de cadastro de Operação',
				url : 'app/operations'
			});
		},
		
		editOperation: function(idOperation) {
			var that = this;
			util.markActiveItem('operations');
			var formOperation = null;
			if (this.pageOperation) {
				formOperation = new FormOperation({
					model : this.pageOperation.operations.get(idOperation),
				});
				that.App.mainRegion.show(formOperation);
			} else {
				var model = new OperationModel({
					id : idOperation,
				})
				model.fetch({
					success : function(model) {
						formOperation = new FormOperation({
							model : model,
						});
						that.App.mainRegion.show(formOperation);
					},
					error : function(x, y, z) {
						console.error(x, y, z);
					}
				})
				util.breadcrumb({
					iconClass : 'fa-calendar',
					itemLabel : 'Operations',
					itemSubFolderName : 'Formulário de atualização de Operação',
					url : 'app/operations'
				});
			}
		},
		
		//configuração das rotas de Permission
		permissions: function() {
			util.markActiveItem('permissions');
			this.pagePermission = new PagePermission();
			this.App.mainRegion.show(this.pagePermission);
			util.breadcrumb({
				iconClass : 'fa-desktop',
				itemLabel : 'Permissão',
				itemSubFolderName : 'Listagem',
				url : 'app/permissions'
			});
		},

		newPermission: function() {
			util.markActiveItem('permissions');
			var formPermission = new FormPermission({
				model : new PermissionModel(),
			});
			this.App.mainRegion.show(formPermission);
			util.breadcrumb({
				iconClass : 'fa-desktop',
				itemLabel : 'Permissão',
				itemSubFolderName : 'Formulário de cadastro de Permissão',
				url : 'app/permissions'
			});
		},
		
		editPermission: function(idPermission) {
			var that = this;
			util.markActiveItem('permissions');
			var formPermission = null;
			if (this.pagePermission) {
				formPermission = new FormPermission({
					model : this.pagePermission.permissions.get(idPermission),
				});
				that.App.mainRegion.show(formPermission);
			} else {
				var model = new PermissionModel({
					id : idPermission,
				})
				model.fetch({
					success : function(model) {
						formPermission = new FormPermission({
							model : model,
						});
						that.App.mainRegion.show(formPermission);
					},
					error : function(x, y, z) {
						console.error(x, y, z);
					}
				})
				util.breadcrumb({
					iconClass : 'fa-calendar',
					itemLabel : 'Permissions',
					itemSubFolderName : 'Formulário de atualização de Permissão',
					url : 'app/permissions'
				});
			}
		},
		
		//configuração das rotas de Role
		roles: function() {
			util.markActiveItem('roles');
			this.pageRole = new PageRole();
			this.App.mainRegion.show(this.pageRole);
			util.breadcrumb({
				iconClass : 'fa-desktop',
				itemLabel : 'Papel',
				itemSubFolderName : 'Listagem',
				url : 'app/roles'
			});
		},

		newRole: function() {
			util.markActiveItem('roles');
			var formRole = new FormRole({
				model : new RoleModel(),
			});
			this.App.mainRegion.show(formRole);
			util.breadcrumb({
				iconClass : 'fa-desktop',
				itemLabel : 'Papel',
				itemSubFolderName : 'Formulário de cadastro de Papel',
				url : 'app/roles'
			});
		},
		
		editRole: function(idRole) {
			var that = this;
			util.markActiveItem('roles');
			var formRole = null;
			if (this.pageRole) {
				formRole = new FormRole({
					model : this.pageRole.roles.get(idRole),
				});
				that.App.mainRegion.show(formRole);
			} else {
				var model = new RoleModel({
					id : idRole,
				})
				model.fetch({
					success : function(model) {
						formRole = new FormRole({
							model : model,
						});
						that.App.mainRegion.show(formRole);
					},
					error : function(x, y, z) {
						console.error(x, y, z);
					}
				})
				util.breadcrumb({
					iconClass : 'fa-calendar',
					itemLabel : 'Roles',
					itemSubFolderName : 'Formulário de atualização de Papel',
					url : 'app/roles'
				});
			}
		},
		
		//configuração das rotas de Session
		sessions: function() {
			util.markActiveItem('sessions');
			this.pageSession = new PageSession();
			this.App.mainRegion.show(this.pageSession);
			util.breadcrumb({
				iconClass : 'fa-desktop',
				itemLabel : 'Sessão',
				itemSubFolderName : 'Listagem',
				url : 'app/sessions'
			});
		},

		newSession: function() {
			util.markActiveItem('sessions');
			var formSession = new FormSession({
				model : new SessionModel(),
			});
			this.App.mainRegion.show(formSession);
			util.breadcrumb({
				iconClass : 'fa-desktop',
				itemLabel : 'Sessão',
				itemSubFolderName : 'Formulário de cadastro de Sessão',
				url : 'app/sessions'
			});
		},
		
		editSession: function(idSession) {
			var that = this;
			util.markActiveItem('sessions');
			var formSession = null;
			if (this.pageSession) {
				formSession = new FormSession({
					model : this.pageSession.sessions.get(idSession),
				});
				that.App.mainRegion.show(formSession);
			} else {
				var model = new SessionModel({
					id : idSession,
				})
				model.fetch({
					success : function(model) {
						formSession = new FormSession({
							model : model,
						});
						that.App.mainRegion.show(formSession);
					},
					error : function(x, y, z) {
						console.error(x, y, z);
					}
				})
				util.breadcrumb({
					iconClass : 'fa-calendar',
					itemLabel : 'Sessions',
					itemSubFolderName : 'Formulário de atualização de Sessão',
					url : 'app/sessions'
				});
			}
		},
		
		//configuração das rotas de User
		users: function() {
			util.markActiveItem('users');
			this.pageUser = new PageUser();
			this.App.mainRegion.show(this.pageUser);
			util.breadcrumb({
				iconClass : 'fa-desktop',
				itemLabel : 'Usuário',
				itemSubFolderName : 'Listagem',
				url : 'app/users'
			});
		},

		newUser: function() {
			util.markActiveItem('users');
			var formUser = new FormUser({
				model : new UserModel(),
			});
			this.App.mainRegion.show(formUser);
			util.breadcrumb({
				iconClass : 'fa-desktop',
				itemLabel : 'Usuário',
				itemSubFolderName : 'Formulário de cadastro de Usuário',
				url : 'app/users'
			});
		},
		
		editUser: function(idUser) {
			var that = this;
			util.markActiveItem('users');
			var formUser = null;
			if (this.pageUser) {
				formUser = new FormUser({
					model : this.pageUser.users.get(idUser),
				});
				that.App.mainRegion.show(formUser);
			} else {
				var model = new UserModel({
					id : idUser,
				})
				model.fetch({
					success : function(model) {
						formUser = new FormUser({
							model : model,
						});
						that.App.mainRegion.show(formUser);
					},
					error : function(x, y, z) {
						console.error(x, y, z);
					}
				})
				util.breadcrumb({
					iconClass : 'fa-calendar',
					itemLabel : 'Users',
					itemSubFolderName : 'Formulário de atualização de Usuário',
					url : 'app/users'
				});
			}
		},
		
		start : function() {
			Backbone.history.start();
		}
	});
	return AppRouter;
});
