/* generated: 29/09/2016 12:48:00 */
define(function(require) {
	// Start "Import´s" Definition"
	var _ = require('adapters/underscore-adapter');
	var $ = require('adapters/jquery-adapter');
	var Col = require('adapters/col-adapter');
	var Backbone = require('adapters/backbone-adapter');
	var Backgrid = require('adapters/backgrid-adapter');
	var Marionette = require('marionette');
	var util = require('utilities/utils');
	var BaseModel = require('models/BaseModel');
	var CustomNumberCell = require('views/components/CustomNumberCell');

	var ModalMultiSelectFuncionario = require('views/funcionario/ModalMultiSelectFuncionario');
	var MultiSelectFuncionarioTemplate = require('text!views/funcionario/tpl/MultiSelectFuncionarioTemplate.html');

	var MultiSelectFuncionario = Marionette.LayoutView.extend({
		template : _.template(MultiSelectFuncionarioTemplate),

		regions : {
			modalMultiSelectFuncionarioRegion : '#modalMultiSelectFuncionarios',
			gridFuncionariosModalRegion : '#gridMultiselectFuncionarios',
		},

		initialize : function() {
			var that = this;

			this.funcionarios = this.collection;

			this.gridFuncionarios = new Backgrid.Grid({
				className : 'table backgrid table-striped table-bordered table-hover dataTable no-footer  ',
				columns : this._getColumns(),
				emptyText : "Sem registros adicionados",
				collection : this.funcionarios,
			});

			this.modalMultiSelectFuncionario = new ModalMultiSelectFuncionario({
				collection : this.collection
			});

			this.on('show', function() {
				that.modalMultiSelectFuncionarioRegion.show(that.modalMultiSelectFuncionario);
				that.gridFuncionariosModalRegion.show(that.gridFuncionarios);
			});
		},
		clear : function(){
			this.modalMultiSelectFuncionario.clear();
		},
		
		_getColumns : function() {
			var columns = [

			{
				name : "nome",
				editable : false,
				sortable : false,
				label 	 : "Nome",
				cell 	 : "string",
			}, 
			{
				name : "email",
				editable : false,
				sortable : false,
				label 	 : "Email",
				cell 	 : "string",
			}, 
			{
				name : "telefone",
				editable : false,
				sortable : false,
				label 	 : "Telefone",
				cell 	 : "string",
			}, 
			{
				name : "limiteMensal",
				editable : false,
				sortable : false,
				label 	 : "Limite mensal",
				cell : CustomNumberCell.extend({}),
			}, 
			{
				name : "cpf",
				editable : false,
				sortable : false,
				label 	 : "Cpf",
				cell 	 : "string",
			}, 
			{
				name : "matricula",
				editable : false,
				sortable : false,
				label 	 : "Matricula",
				cell 	 : "string",
			}, 
			{
				name : "statusFuncionario",
				editable : false,
				sortable : false,
				label 	 : "Status funcionario",
				cell 	 : "string",
			}, 
			{
				name : "autorizadoTicket",
				editable : false,
				sortable : false,
				label 	 : "Autorizado ticket",
				cell 	 : "string",
			}, 
			{
				name : "observacao",
				editable : false,
				sortable : false,
				label 	 : "Observacao",
				cell 	 : "string",
			}, 
			];
			return columns;
		},
	});

	return MultiSelectFuncionario
});
