/* generated: 29/09/2016 12:48:00 */
define(function(require) {
	// Start "Import´s Definition"
	var _ = require('adapters/underscore-adapter');
	var $ = require('adapters/jquery-adapter');
	var Col = require('adapters/col-adapter');
	var Backbone = require('adapters/backbone-adapter');
	var Backgrid = require('adapters/backgrid-adapter');
	var Marionette = require('marionette');
	var CustomNumberCell = require('views/components/CustomNumberCell');

	var util = require('utilities/utils');
	var FuncionarioPageCollection = require('collections/FuncionarioPageCollection');
	var ModalMultiSelectFuncionarioTemplate = require('text!views/funcionario/tpl/ModalMultiSelectFuncionarioTemplate.html');
	// End of "Import´s" definition

	var ModalFuncionarios = Marionette.LayoutView.extend({
		template : _.template(ModalMultiSelectFuncionarioTemplate),

		regions : {
			gridRegion : '#grid-funcionarios-modal',
			paginatorRegion : '#paginator-funcionarios-modal',
		},
		events : {
			'click .btnOk' : 'close'
		},
		ui : {
			btnOk : ".btnOk",
		},

		initialize : function(opt) {
			var that = this;

			this.projetoFuncionarios = this.collection;
			
			this.funcionarios = new FuncionarioPageCollection();
			this.funcionarios.on('fetched', this.endFetch, this);
			this.funcionarios.on('backgrid:selected', this.selectModel, this);

			this.grid = new Backgrid.Grid({
				className : 'table backgrid table-striped table-bordered table-hover dataTable no-footer  ',
				columns : this.getColumns(),
				emptyText : "Nenhum registro para escolha",
				collection : this.funcionarios,
			});

			this.paginator = new Backgrid.Extension.Paginator({
				columns : this.getColumns(),
				collection : this.funcionarios,
				className : 'dataTables_paginate paging_simple_numbers',
				uiClassName : 'pagination',
			});

			this.funcionarios.getFirstPage({
				success : function(_col, _resp, _opts) {
					console.info('Primeira pagina do grid funcionario');
				},
				error : function(_col, _resp, _opts) {
					console.error(_resp.responseText || _resp.getResponseHeader('exception'));
				}
			});


			this.on('show', function() {
				that.gridRegion.show(that.grid);
				that.paginatorRegion.show(that.paginator);
			});
		},

		selectModel : function(model, checked) {
			if (checked)
				this.projetoFuncionarios.add(model)
			else
				this.projetoFuncionarios.remove(model)
		},

		endFetch : function(_collection) {
			var that = this;
			this.funcionarios.each(function(model) {
				if (that.projetoFuncionarios.findWhere({
					id : model.get('id')
				})) {
					model.trigger("backgrid:select", model, true);
				}
			});
		},
		clear : function(){
			this.grid.$el.find('input[type=checkbox]').prop('checked', false);
		},
		getColumns : function() {
			var columns = [{
				name : "",
				cell : "select-row",
				headerCell : "select-all"
			}, 
			 
			{
				name : "nome",
				editable : false,
				sortable : false,
				label 	 : "Nome",
				cell 	 : "string",
			}, 
			{
				name : "email",
				editable : false,
				sortable : false,
				label 	 : "Email",
				cell 	 : "string",
			}, 
			{
				name : "telefone",
				editable : false,
				sortable : false,
				label 	 : "Telefone",
				cell 	 : "string",
			}, 
			{
				name : "limiteMensal",
				editable : false,
				sortable : false,
				label 	 : "Limite mensal",
				cell : CustomNumberCell.extend({}),
			}, 
			{
				name : "cpf",
				editable : false,
				sortable : false,
				label 	 : "Cpf",
				cell 	 : "string",
			}, 
			{
				name : "matricula",
				editable : false,
				sortable : false,
				label 	 : "Matricula",
				cell 	 : "string",
			}, 
			{
				name : "statusFuncionario",
				editable : false,
				sortable : false,
				label 	 : "Status funcionario",
				cell 	 : "string",
			}, 
			{
				name : "autorizadoTicket",
				editable : false,
				sortable : false,
				label 	 : "Autorizado ticket",
				cell 	 : "string",
			}, 
			{
				name : "observacao",
				editable : false,
				sortable : false,
				label 	 : "Observacao",
				cell 	 : "string",
			}, 
			];
			return columns;
		},
	});

	return ModalFuncionarios;
});
