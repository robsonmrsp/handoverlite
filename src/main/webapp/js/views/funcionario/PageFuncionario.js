/* generated: 29/09/2016 12:48:00 */
define(function(require) {
	// Start "Import´s Definition"
	var _ = require('adapters/underscore-adapter');
	var $ = require('adapters/jquery-adapter');
	var Col = require('adapters/col-adapter');
	var Backbone = require('adapters/backbone-adapter');
	var Marionette = require('marionette');
	var Backgrid = require('adapters/backgrid-adapter');
	var util = require('utilities/utils');
	var Combobox = require('views/components/Combobox');
	var CustomStringCell = require('views/components/CustomStringCell');
	var Counter = require('views/components/Counter');
	var ActionsCell = require('views/components/ActionsCell');
	var GeneralActionsCell = require('views/components/GeneralActionsCell');

	var CustomNumberCell = require('views/components/CustomNumberCell');

	var TemplateFormFuncionarios = require('text!views/funcionario/tpl/FormFuncionarioTemplate.html');
	var FuncionarioModel = require('models/FuncionarioModel');
	var FuncionarioCollection = require('collections/FuncionarioCollection');
	var FuncionarioPageCollection = require('collections/FuncionarioPageCollection');
	var PageFuncionarioTemplate = require('text!views/funcionario/tpl/PageFuncionarioTemplate.html');
	
	//Filter import
	var SearchEmpresaModal = require('views/modalComponents/EmpresaModal');
	var CentroCustoCollection = require('collections/CentroCustoCollection');			
	
	// End of "Import´s" definition

	var PageFuncionario = Marionette.LayoutView.extend({
		template : _.template(PageFuncionarioTemplate),

		regions : {
			gridRegion : '#grid',
			counterRegion : '#counter',
			paginatorRegion : '#paginator',
			searchEmpresaModalRegion : '#empresaModal',
		},
		
		events : {
			'click 	#reset' : 'resetFuncionario',			
			'click #searchEmpresaModal' : 'showSearchEmpresaModal',
			'keypress' : 'treatKeypress',
			
			'click 	.search-button' : 'searchFuncionario',
			'click .show-advanced-search-button' : 'toggleAdvancedForm',
		},
		
		
		ui : {
			inputNome : '#inputNome',
			inputEmail : '#inputEmail',
			inputTelefone : '#inputTelefone',
			inputLimiteMensal : '#inputLimiteMensal',
			inputCpf : '#inputCpf',
			inputMatricula : '#inputMatricula',
			inputStatusFuncionario : '#inputStatusFuncionario',
			inputAutorizadoTicket : '#inputAutorizadoTicket',
			inputObservacao : '#inputObservacao',
		
			inputEmpresaId : '#inputEmpresaId',
			inputEmpresaNome : '#inputEmpresaNome',
			inputCentroCusto : '#inputCentroCusto', 
			form : '#formFuncionarioFilter',
			advancedSearchForm : '.advanced-search-form',
		},
		
		toggleAdvancedForm : function() {
			this.ui.advancedSearchForm.slideToggle("slow");
		},

		
		treatKeypress : function (e){
		    if (util.enterPressed(e)) {
	    		e.preventDefault();
	    		this.searchFuncionario();
	    	}
		},

		initialize : function() {
			var that = this;

			this.funcionarios = new FuncionarioPageCollection();

			this.grid = new Backgrid.Grid({
				className : 'table backgrid table-striped table-bordered table-hover dataTable no-footer  ',
				columns : this.getColumns(),
				emptyText : "Sem registros",
				collection : this.funcionarios
			});

			this.counter = new Counter({
				collection : this.funcionarios,
			});

			this.paginator = new Backgrid.Extension.Paginator({
				columns : this.getColumns(),
				collection : this.funcionarios,
				className : ' paging_simple_numbers',
				uiClassName : 'pagination',
			});

			this.funcionarios.getFirstPage({
				success : function(_col, _resp, _opts) {
					console.info('Primeira pagina do grid funcionario');
				},
				error : function(_col, _resp, _opts) {
					console.error(_resp.responseText || (_resp.getResponseHeader && _resp.getResponseHeader('exception')) );
				}
			});
			this.searchEmpresaModal = new SearchEmpresaModal({
				onSelectModel : function(model) {
					that.selectEmpresa(model);
				},
			});
			this.on('show', function() {
				that.gridRegion.show(that.grid);
				that.counterRegion.show(that.counter);
				that.paginatorRegion.show(that.paginator);
				this.searchEmpresaModalRegion.show(this.searchEmpresaModal);		
				this.ui.inputLimiteMensal.formatNumber(2);
		
				var comboCentroCusto = new Combobox({
					el : this.ui.inputCentroCusto,
					comboId : 'id',
					comboVal : 'nome',
					collectionEntity : CentroCustoCollection,
				});
			});
		},
		 
		searchFuncionario : function(){
			var that = this;

			this.funcionarios.filterQueryParams = {
	    		nome : util.escapeById('inputNome'),
	    		email : util.escapeById('inputEmail'),
	    		telefone : util.escapeById('inputTelefone'),
	    		limiteMensal : util.escapeById('inputLimiteMensal'),
	    		cpf : util.escapeById('inputCpf'),
	    		matricula : util.escapeById('inputMatricula'),
	    		statusFuncionario : util.escapeById('inputStatusFuncionario'),
	    		autorizadoTicket : util.escapeById('inputAutorizadoTicket'),
	    		observacao : util.escapeById('inputObservacao'),
			    empresa : util.escapeById('inputEmpresaId'), 
			    centroCusto : util.escapeById('inputCentroCusto'), 
			}
			this.funcionarios.fetch({
				success : function(_coll, _resp, _opt) {
					console.info('Consulta para o grid funcionario');
				},
				error : function(_coll, _resp, _opt) {
					console.error(_resp.responseText || (_resp.getResponseHeader && _resp.getResponseHeader('exception')));
				},
				complete : function() {
					
				},
			})		
		},
		resetFuncionario : function(){
			this.ui.form.get(0).reset();
			this.funcionarios.reset();
			util.clear('inputEmpresaId');
		},
				
		getColumns : function() {
			var that = this;
			var columns = [
			{
				name : "nome",
				editable : false,
				sortable : true,
				label 	 : "Nome",
				cell 	 : "string",
			}, 
			{
				name : "email",
				editable : false,
				sortable : true,
				label 	 : "Email",
				cell 	 : "string",
			}, 
			{
				name : "telefone",
				editable : false,
				sortable : true,
				label 	 : "Telefone",
				cell 	 : "string",
			}, 
			{
				name : "limiteMensal",
				editable : false,
				sortable : true,
				label 	 : "Limite mensal",
				cell : CustomNumberCell.extend({}),
			}, 
			{
				name : "cpf",
				editable : false,
				sortable : true,
				label 	 : "Cpf",
				cell 	 : "string",
			}, 
			{
				name : "matricula",
				editable : false,
				sortable : true,
				label 	 : "Matricula",
				cell 	 : "string",
			}, 
			{
				name : "statusFuncionario",
				editable : false,
				sortable : true,
				label 	 : "Status funcionario",
				cell 	 : "string",
			}, 
			{
				name : "autorizadoTicket",
				editable : false,
				sortable : true,
				label 	 : "Autorizado ticket",
				cell 	 : "string",
			}, 
			{
				name : "observacao",
				editable : false,
				sortable : true,
				label 	 : "Observacao",
				cell 	 : "string",
			}, 
			{
				name : "empresa.nome",
				editable : false,
				sortable : true,  
				label : "Empresa",
				cell : CustomStringCell.extend({
					fieldName : 'empresa.nome',
				}),
			},	
			{
				name : "centroCusto.nome",
				editable : false,
				sortable : true,  
				label : "Centro custo",
				cell : CustomStringCell.extend({
					fieldName : 'centroCusto.nome',
				}),
			},	
			{
				name : "acoes",
				label : "Ações(Editar, Deletar)",
				sortable : false,
				cell : GeneralActionsCell.extend({
					buttons : that.getCellButtons(),
					context : that,
				})
			} ];
			return columns;
		},
		
		getCellButtons : function() {
			var that = this;
			var buttons = [];

			buttons.push({
				id : 'edita_ficha_button',
				type : 'primary',
				icon : 'icon-pencil fa-pencil',
				hint : 'Editar Funcionario',
				onClick : that.editModel,
			}, {
				id : 'delete_button',
				type : 'danger',
				icon : 'icon-trash fa-trash',
				hint : 'Remover Funcionario',
				onClick : that.deleteModel,
			});

			return buttons;
		},

		deleteModel : function(model) {
			var that = this;
			
			var modelTipo = new FuncionarioModel({
				id : model.id,
			});
			
			util.Bootbox.confirm("Tem certeza que deseja remover o registro [ " + model.get('id') + " ] ?", function(yes) {
				if (yes) {
					modelTipo.destroy({
						success : function() {
							that.funcionarios.remove(model);
							util.showSuccessMessage('Funcionario removido com sucesso!');
						},
						error : function(_model, _resp) {
							util.showErrorMessage('Problema ao remover o registro',_resp);
						}
					});
				}
			});
		},

		editModel : function(model) {
			util.goPage("app/editFuncionario/" + model.get('id'));
		},

		showSearchEmpresaModal : function() {
			this.searchEmpresaModal.showPage();
		},
			
		selectEmpresa : function(empresa) {
			this.searchEmpresaModal.hidePage();	
			this.ui.inputEmpresaId.val(empresa.get('id'));
			this.ui.inputEmpresaNome.val(empresa.get('nome'));		
		},
		

	});

	return PageFuncionario;
});
