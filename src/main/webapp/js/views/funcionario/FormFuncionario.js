/* generated: 29/09/2016 12:48:00 */
define(function(require) {
	// Start "Import´s" Definition"
	var _ = require('adapters/underscore-adapter');
	var $ = require('adapters/jquery-adapter');
	var Col = require('adapters/col-adapter');
	var Backbone = require('adapters/backbone-adapter');
	var Marionette = require('marionette');
	var Backgrid = require('adapters/backgrid-adapter');
	var util = require('utilities/utils');
	var Combobox = require('views/components/Combobox');

	var TemplateFormFuncionarios = require('text!views/funcionario/tpl/FormFuncionarioTemplate.html');
	var FuncionarioModel = require('models/FuncionarioModel');
	var FuncionarioCollection = require('collections/FuncionarioCollection');
	var SearchEmpresaModal = require('views/modalComponents/EmpresaModal');
	var CentroCustoCollection = require('collections/CentroCustoCollection');			
	
	// End of "Import´s" definition

	// #####################################################################################################
	// ¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨MAIN BODY¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨
	// #####################################################################################################

	var FormFuncionarios = Marionette.LayoutView.extend({
		template : _.template(TemplateFormFuncionarios),

		regions : {
			searchEmpresaModalRegion : '#empresaModal',
		},

		events : {
			'click 	.save' : 'save',
			'click 	.saveAndContinue' : 'saveAndContinue',
			'click #searchEmpresaModal' : 'showSearchEmpresaModal',
		},
		
		ui : {
			inputId : '#inputId',
			inputNome : '#inputNome',
			inputEmail : '#inputEmail',
			inputTelefone : '#inputTelefone',
			inputLimiteMensal : '#inputLimiteMensal',
			inputCpf : '#inputCpf',
			inputMatricula : '#inputMatricula',
			inputStatusFuncionario : '#inputStatusFuncionario',
			inputAutorizadoTicket : '#inputAutorizadoTicket',
			inputObservacao : '#inputObservacao',
		
			inputEmpresaId : '#inputEmpresaId',
			inputEmpresaNome : '#inputEmpresaNome',
			inputCentroCusto : '#inputCentroCusto', 
			form : '#formFuncionario',
		},

		initialize : function() {
			var that = this;
			this.searchEmpresaModal = new SearchEmpresaModal({
				onSelectModel : function(model) {
					that.selectEmpresa(model);
				},
			});
			this.on('show', function() {
				this.searchEmpresaModalRegion.show(this.searchEmpresaModal);		
				this.ui.inputLimiteMensal.formatNumber(2);
				this.comboCentroCusto = new Combobox({
					el : this.ui.inputCentroCusto,
					comboId : 'id',
					comboVal : 'nome',
					collectionEntity : CentroCustoCollection, 
				});
				this.comboCentroCusto.setValue(this.model.get('centroCusto'));					
				this.ui.form.validationEngine('attach', {
					promptPosition : "topLeft",
					isOverflown : false,
					validationEventTrigger : "change"
				});
			});
		},

		saveAndContinue : function() {
			this.save(true)
		},

		save : function(continua) {
			var that = this;
			var funcionario = that.getModel();

			if (this.isValid()) {
				funcionario.save({}, {
					success : function(_model, _resp, _options) {
						util.showSuccessMessage('Funcionario salvo com sucesso!');
						that.clearForm();

						if (continua != true) {
							util.goPage('app/funcionarios');
						}
					},

					error : function(_model, _resp, _options) {
						util.showErrorMessage('Problema ao salvar registro',_resp);
					}
				});
			} else {
				util.showMessage('error', 'Verifique campos em destaque!');
			}
		},

		
		clearForm : function() {
			util.clear('inputId');
			util.clear('inputNome'); 
			util.clear('inputEmail'); 
			util.clear('inputTelefone'); 
			util.clear('inputLimiteMensal'); 
			util.clear('inputCpf'); 
			util.clear('inputMatricula'); 
			util.clear('inputStatusFuncionario'); 
			util.clear('inputAutorizadoTicket'); 
			util.clear('inputObservacao'); 
			util.clear('inputEmpresaId');
			util.clear('inputEmpresaNome');
			util.clear('inputCentroCusto'); 					 	
		},

		possuiCamposInvalidos : function() {
			return util.hasInvalidFields(this.validateFields);
		},

		isValid : function() {
			return this.ui.form.validationEngine('validate', {
				promptPosition : "topLeft",
				isOverflown : false,
				validationEventTrigger : "change"
			});
		},

		getModel : function() {
			var that = this;
			var funcionario = that.model; 
			funcionario.set({
				id: util.escapeById('inputId') || null,
		    	nome : util.escapeById('inputNome'), 
				
		    	email : util.escapeById('inputEmail'), 
				
		    	telefone : util.escapeById('inputTelefone'), 
				
		    	limiteMensal : util.escapeById('inputLimiteMensal', true), 
				
		    	cpf : util.escapeById('inputCpf'), 
				
		    	matricula : util.escapeById('inputMatricula'), 
				
		    	statusFuncionario : util.escapeById('inputStatusFuncionario'), 
				
		    	autorizadoTicket : util.escapeById('inputAutorizadoTicket'), 
				
		    	observacao : util.escapeById('inputObservacao'), 
				
					empresa : that.getEmpresa(),
					centroCusto :  that.comboCentroCusto.getJsonValue(),
			});
			return funcionario;
		},
		 
		getEmpresa : function() {			
			var id = util.escapeById('inputEmpresaId');
			var nome = util.escapeById('inputEmpresaNome');
			var empresa = null;
			
			if (id && nome) {
				empresa = {
					id : id,
					nome : nome,
				}
			}
			return empresa;
		},	
		getCentroCusto : function() {
			var id =  this.comboCentroCusto.getRawValue();			
			
			if(id){
				return {
					id: id,
				}
			}
			return null;
		},				
		
		showSearchEmpresaModal : function() {
			this.searchEmpresaModal.showPage();
		},
			
		selectEmpresa : function(empresa) {
			this.searchEmpresaModal.hidePage();	
			this.ui.inputEmpresaId.val(empresa.get('id'));
			this.ui.inputEmpresaNome.val(empresa.get('nome'));		
		},
				
		
	});

	return FormFuncionarios;
});