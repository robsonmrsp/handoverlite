/* generated: 29/09/2016 12:48:00 */
define(function(require) {
	// Start "Import´s" Definition"
	var _ = require('adapters/underscore-adapter');
	var $ = require('adapters/jquery-adapter');
	var Col = require('adapters/col-adapter');
	var Backbone = require('adapters/backbone-adapter');
	var Marionette = require('marionette');
	var Backgrid = require('adapters/backgrid-adapter');
	var util = require('utilities/utils');
	var RadioButtonCell = require('views/components/RadioButtonCell');
	var Counter = require('views/components/Counter');
	var RowClick = require('views/components/CustomClickedRow');
	var Combobox = require('views/components/Combobox');
	var CustomNumberCell = require('views/components/CustomNumberCell');

	var EmpresaModal = require('text!views/modalComponents/tpl/EmpresaModalTemplate.html');
	var EmpresaPageCollection = require('collections/EmpresaPageCollection');

	// End of "Import´s" definition
	// #####################################################################################################
	// .............................................MAIN-BODY.............................................
	// #####################################################################################################

	var EmpresaModal = Marionette.LayoutView.extend({
		template : _.template(EmpresaModal),

		events : {
			'click #btnSearchEmpresa' : 'searchEmpresa',
			'click #btnClearEmpresa' : 'clearModal',
			'click tr' : 'selectRow',
			'keypress' : 'treatKeypress',
		},

		regions : {
			counterRegion : 	'#counter-empresa',
			gridRegion : 		'#grid-empresa',
			paginatorRegion : 	'#paginator-empresa',
		},

		ui : {
    		inputModalCnpj : '#inputModalCnpj',
    		inputModalNumeroContrato : '#inputModalNumeroContrato',
    		inputModalRazaoSocial : '#inputModalRazaoSocial',
    		inputModalNomeFantasia : '#inputModalNomeFantasia',
    		inputModalUtilizaVoucher : '#inputModalUtilizaVoucher',
    		inputModalUtilizaTicket : '#inputModalUtilizaTicket',
    		inputModalObservacaoTaxista : '#inputModalObservacaoTaxista',
    		inputModalObservacaoCentral : '#inputModalObservacaoCentral',
    		inputModalPercursoDesconto : '#inputModalPercursoDesconto',
    		inputModalInscricaoMunicipal : '#inputModalInscricaoMunicipal',
    		inputModalInscricaoEstadual : '#inputModalInscricaoEstadual',
    		inputModalNf : '#inputModalNf',
    		inputModalPercentualIss : '#inputModalPercentualIss',
    		inputModalPercentualIrf : '#inputModalPercentualIrf',
    		inputModalPercentualInss : '#inputModalPercentualInss',
    		inputModalDiaVencimento : '#inputModalDiaVencimento',
    		inputModalPercentualMotorista : '#inputModalPercentualMotorista',
    		inputModalBanco : '#inputModalBanco',
    		inputModalAgencia : '#inputModalAgencia',
    		inputModalConta : '#inputModalConta',
    		inputModalEmail : '#inputModalEmail',
		
			form : '#formSearchEmpresa',
			modalScreen : '.modal',
		},
		treatKeypress : function (e){
		    if (util.enterPressed(e)) {
	    		e.preventDefault();
	    		this.searchEmpresa();
	    	}
		},

		initialize : function(opt) {
			var that = this;

			this.onSelectModel = opt.onSelectModel;

			this.empresaCollection = new EmpresaPageCollection();
			this.empresaCollection.state.pageSize = 5;
			this.empresaCollection.on('fetching', this.startFetch, this);
			this.empresaCollection.on('fetched', this.stopFetch, this);

			this.grid = new Backgrid.Grid({
				row : RowClick,
				className : 'table backgrid table-striped table-bordered table-hover dataTable no-footer  ',
				columns : this.getColumns(),
				emptyText : "Sem registros",
				collection : this.empresaCollection,
				emptyText : "Sem registros para exibir."

			});
			
			this.counter = new Counter({
				collection : this.empresaCollection ,
			});
			

			this.paginator = new Backgrid.Extension.Paginator({
				columns : this.getColumns(),
				collection : this.empresaCollection,
				className : 'dataTables_paginate paging_simple_numbers',
				uiClassName : 'pagination',
			});

			this.on('show', function() {
				that.gridRegion.show(that.grid);
				that.counterRegion.show(that.counter);
				that.paginatorRegion.show(that.paginator);
			});
		},

		selectRow : function(e) {
			var modelEmpresa = util.getWrappedModel(e);
			if (modelEmpresa)
				this.onSelectModel(modelEmpresa);
		},
		
		getColumns : function() {
			var columns = [	

			{
				name : "cnpj",
				editable : false,
				sortable : true,
				label 	 : "Cnpj",
				cell 	 : "string",
			}, 
			{
				name : "numeroContrato",
				editable : false,
				sortable : true,
				label 	 : "Numero do Contrato",
				cell 	 : "string",
			}, 
			{
				name : "razaoSocial",
				editable : false,
				sortable : true,
				label 	 : "Razao social",
				cell 	 : "string",
			}, 
			{
				name : "nomeFantasia",
				editable : false,
				sortable : true,
				label 	 : "Nome fantasia",
				cell 	 : "string",
			}, 
			{
				name : "utilizaVoucher",
				editable : false,
				sortable : true,
				label 	 : "Utiliza voucher",
				cell 	 : "string",
			}, 
			{
				name : "utilizaTicket",
				editable : false,
				sortable : true,
				label 	 : "Utiliza ticket",
				cell 	 : "string",
			}, 
			{
				name : "observacaoTaxista",
				editable : false,
				sortable : true,
				label 	 : "Observacao taxista",
				cell 	 : "string",
			}, 
			{
				name : "observacaoCentral",
				editable : false,
				sortable : true,
				label 	 : "Observacao central",
				cell 	 : "string",
			}, 
			{
				name : "percursoDesconto",
				editable : false,
				sortable : true,
				label 	 : "Percurso desconto",
				cell : CustomNumberCell.extend({}),
			}, 
			{
				name : "inscricaoMunicipal",
				editable : false,
				sortable : true,
				label 	 : "Inscricao municipal",
				cell 	 : "string",
			}, 
			{
				name : "inscricaoEstadual",
				editable : false,
				sortable : true,
				label 	 : "Inscricao estadual",
				cell 	 : "string",
			}, 
			{
				name : "nf",
				editable : false,
				sortable : true,
				label 	 : "Nf",
				cell 	 : "string",
			}, 
			{
				name : "percentualIss",
				editable : false,
				sortable : true,
				label 	 : "Percentual ISS",
				cell : CustomNumberCell.extend({}),
			}, 
			{
				name : "percentualIrf",
				editable : false,
				sortable : true,
				label 	 : "Percentual IRF",
				cell : CustomNumberCell.extend({}),
			}, 
			{
				name : "percentualInss",
				editable : false,
				sortable : true,
				label 	 : "Percentual INSS",
				cell : CustomNumberCell.extend({}),
			}, 
			{
				name : "diaVencimento",
				editable : false,
				sortable : true,
				label 	 : "Dia vencimento",
				cell : CustomNumberCell.extend({}),
			}, 
			{
				name : "percentualMotorista",
				editable : false,
				sortable : true,
				label 	 : "Percentual motorista",
				cell : CustomNumberCell.extend({}),
			}, 
			{
				name : "banco",
				editable : false,
				sortable : true,
				label 	 : "Banco",
				cell : CustomNumberCell.extend({}),
			}, 
			{
				name : "agencia",
				editable : false,
				sortable : true,
				label 	 : "Agencia",
				cell 	 : "string",
			}, 
			{
				name : "conta",
				editable : false,
				sortable : true,
				label 	 : "Conta",
				cell 	 : "string",
			}, 
			{
				name : "email",
				editable : false,
				sortable : true,
				label 	 : "Email",
				cell 	 : "string",
			}, 
			];
			return columns;
		},

		clearFields : function() {
			util.clear('inputModalId');
			util.clear('inputModalCnpj'); 
			util.clear('inputModalNumeroContrato'); 
			util.clear('inputModalRazaoSocial'); 
			util.clear('inputModalNomeFantasia'); 
			util.clear('inputModalUtilizaVoucher'); 
			util.clear('inputModalUtilizaTicket'); 
			util.clear('inputModalObservacaoTaxista'); 
			util.clear('inputModalObservacaoCentral'); 
			util.clear('inputModalPercursoDesconto'); 
			util.clear('inputModalInscricaoMunicipal'); 
			util.clear('inputModalInscricaoEstadual'); 
			util.clear('inputModalNf'); 
			util.clear('inputModalPercentualIss'); 
			util.clear('inputModalPercentualIrf'); 
			util.clear('inputModalPercentualInss'); 
			util.clear('inputModalDiaVencimento'); 
			util.clear('inputModalPercentualMotorista'); 
			util.clear('inputModalBanco'); 
			util.clear('inputModalAgencia'); 
			util.clear('inputModalConta'); 
			util.clear('inputModalEmail'); 
			util.scrollUpModal();
		},

		searchEmpresa : function() {
			this.empresaCollection.filterQueryParams = {
	    		cnpj : util.escapeById('inputModalCnpj'),
	    		numeroContrato : util.escapeById('inputModalNumeroContrato'),
	    		razaoSocial : util.escapeById('inputModalRazaoSocial'),
	    		nomeFantasia : util.escapeById('inputModalNomeFantasia'),
	    		utilizaVoucher : util.escapeById('inputModalUtilizaVoucher'),
	    		utilizaTicket : util.escapeById('inputModalUtilizaTicket'),
	    		observacaoTaxista : util.escapeById('inputModalObservacaoTaxista'),
	    		observacaoCentral : util.escapeById('inputModalObservacaoCentral'),
	    		percursoDesconto : util.escapeById('inputModalPercursoDesconto'),
	    		inscricaoMunicipal : util.escapeById('inputModalInscricaoMunicipal'),
	    		inscricaoEstadual : util.escapeById('inputModalInscricaoEstadual'),
	    		nf : util.escapeById('inputModalNf'),
	    		percentualIss : util.escapeById('inputModalPercentualIss'),
	    		percentualIrf : util.escapeById('inputModalPercentualIrf'),
	    		percentualInss : util.escapeById('inputModalPercentualInss'),
	    		diaVencimento : util.escapeById('inputModalDiaVencimento'),
	    		percentualMotorista : util.escapeById('inputModalPercentualMotorista'),
	    		banco : util.escapeById('inputModalBanco'),
	    		agencia : util.escapeById('inputModalAgencia'),
	    		conta : util.escapeById('inputModalConta'),
	    		email : util.escapeById('inputModalEmail'),
			};

			this.empresaCollection.fetch({
				resetState : true,
				success : function(_coll, _resp, _opt) {
					//caso queira algum tratamento de sucesso adicional
				},
				error : function(_coll, _resp, _opt) {
					console.error(_coll, _resp, _opt)
				}
			});
		},

		hidePage : function() {
			this.ui.modalScreen.modal('hide');
		},

		showPage : function() {
			this.clearModal();

			this.ui.modalScreen.modal('show');
			this.empresaCollection.getFirstPage({
				success : function(_col, _resp, _opts) {
					//caso queira algum tratamento de sucesso adicional
				},
				error : function(_col, _resp, _opts) {
					console.error(_resp.responseText || (_resp.getResponseHeader && _resp.getResponseHeader('exception')));
				}
			});
		},

		clearModal : function() {
			this.clearFields();
			this.ui.form.get(0).reset();
			this.empresaCollection.reset();
		},
		
		// Executada depois da consulta concluida.
		stopFetch : function() {
			util.stopSpinner();
			util.scrollDownModal();
		},
		
		// Executada Antes da realização da consulta.
		startFetch : function() {
			util.showSpinner('spinEmpresa');
		},
	});

	return EmpresaModal;
});
