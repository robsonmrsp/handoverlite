/* generated: 29/09/2016 12:48:00 */
define(function(require) {
	// Start "Import´s" Definition"
	var _ = require('adapters/underscore-adapter');
	var $ = require('adapters/jquery-adapter');
	var Col = require('adapters/col-adapter');
	var Backbone = require('adapters/backbone-adapter');
	var Marionette = require('marionette');
	var Backgrid = require('adapters/backgrid-adapter');
	var util = require('utilities/utils');
	var RadioButtonCell = require('views/components/RadioButtonCell');
	var Counter = require('views/components/Counter');
	var RowClick = require('views/components/CustomClickedRow');
	var Combobox = require('views/components/Combobox');
	var CustomNumberCell = require('views/components/CustomNumberCell');

	var CentroCustoModal = require('text!views/modalComponents/tpl/CentroCustoModalTemplate.html');
	var CentroCustoPageCollection = require('collections/CentroCustoPageCollection');

	// End of "Import´s" definition
	// #####################################################################################################
	// .............................................MAIN-BODY.............................................
	// #####################################################################################################

	var CentroCustoModal = Marionette.LayoutView.extend({
		template : _.template(CentroCustoModal),

		events : {
			'click #btnSearchCentroCusto' : 'searchCentroCusto',
			'click #btnClearCentroCusto' : 'clearModal',
			'click tr' : 'selectRow',
			'keypress' : 'treatKeypress',
		},

		regions : {
			counterRegion : 	'#counter-centroCusto',
			gridRegion : 		'#grid-centroCusto',
			paginatorRegion : 	'#paginator-centroCusto',
		},

		ui : {
    		inputModalDescricao : '#inputModalDescricao',
    		inputModalValorLimite : '#inputModalValorLimite',
    		inputModalObservacao : '#inputModalObservacao',
    		inputModalObseracaoTaxista : '#inputModalObseracaoTaxista',
    		inputModalInativo : '#inputModalInativo',
		
			form : '#formSearchCentroCusto',
			modalScreen : '.modal',
		},
		treatKeypress : function (e){
		    if (util.enterPressed(e)) {
	    		e.preventDefault();
	    		this.searchCentroCusto();
	    	}
		},

		initialize : function(opt) {
			var that = this;

			this.onSelectModel = opt.onSelectModel;

			this.centroCustoCollection = new CentroCustoPageCollection();
			this.centroCustoCollection.state.pageSize = 5;
			this.centroCustoCollection.on('fetching', this.startFetch, this);
			this.centroCustoCollection.on('fetched', this.stopFetch, this);

			this.grid = new Backgrid.Grid({
				row : RowClick,
				className : 'table backgrid table-striped table-bordered table-hover dataTable no-footer  ',
				columns : this.getColumns(),
				emptyText : "Sem registros",
				collection : this.centroCustoCollection,
				emptyText : "Sem registros para exibir."

			});
			
			this.counter = new Counter({
				collection : this.centroCustoCollection ,
			});
			

			this.paginator = new Backgrid.Extension.Paginator({
				columns : this.getColumns(),
				collection : this.centroCustoCollection,
				className : 'dataTables_paginate paging_simple_numbers',
				uiClassName : 'pagination',
			});

			this.on('show', function() {
				that.gridRegion.show(that.grid);
				that.counterRegion.show(that.counter);
				that.paginatorRegion.show(that.paginator);
			});
		},

		selectRow : function(e) {
			var modelCentroCusto = util.getWrappedModel(e);
			if (modelCentroCusto)
				this.onSelectModel(modelCentroCusto);
		},
		
		getColumns : function() {
			var columns = [	

			{
				name : "descricao",
				editable : false,
				sortable : true,
				label 	 : "Descricao",
				cell 	 : "string",
			}, 
			{
				name : "valorLimite",
				editable : false,
				sortable : true,
				label 	 : "Valor limite",
				cell : CustomNumberCell.extend({}),
			}, 
			{
				name : "observacao",
				editable : false,
				sortable : true,
				label 	 : "Observacao",
				cell 	 : "string",
			}, 
			{
				name : "obseracaoTaxista",
				editable : false,
				sortable : true,
				label 	 : "Obseracao taxista",
				cell 	 : "string",
			}, 
			{
				name : "inativo",
				editable : false,
				sortable : true,
				label 	 : "Inativo",
				cell 	 : "string",
			}, 
			];
			return columns;
		},

		clearFields : function() {
			util.clear('inputModalId');
			util.clear('inputModalDescricao'); 
			util.clear('inputModalValorLimite'); 
			util.clear('inputModalObservacao'); 
			util.clear('inputModalObseracaoTaxista'); 
			util.clear('inputModalInativo'); 
			util.scrollUpModal();
		},

		searchCentroCusto : function() {
			this.centroCustoCollection.filterQueryParams = {
	    		descricao : util.escapeById('inputModalDescricao'),
	    		valorLimite : util.escapeById('inputModalValorLimite'),
	    		observacao : util.escapeById('inputModalObservacao'),
	    		obseracaoTaxista : util.escapeById('inputModalObseracaoTaxista'),
	    		inativo : util.escapeById('inputModalInativo'),
			};

			this.centroCustoCollection.fetch({
				resetState : true,
				success : function(_coll, _resp, _opt) {
					//caso queira algum tratamento de sucesso adicional
				},
				error : function(_coll, _resp, _opt) {
					console.error(_coll, _resp, _opt)
				}
			});
		},

		hidePage : function() {
			this.ui.modalScreen.modal('hide');
		},

		showPage : function() {
			this.clearModal();

			this.ui.modalScreen.modal('show');
			this.centroCustoCollection.getFirstPage({
				success : function(_col, _resp, _opts) {
					//caso queira algum tratamento de sucesso adicional
				},
				error : function(_col, _resp, _opts) {
					console.error(_resp.responseText || (_resp.getResponseHeader && _resp.getResponseHeader('exception')));
				}
			});
		},

		clearModal : function() {
			this.clearFields();
			this.ui.form.get(0).reset();
			this.centroCustoCollection.reset();
		},
		
		// Executada depois da consulta concluida.
		stopFetch : function() {
			util.stopSpinner();
			util.scrollDownModal();
		},
		
		// Executada Antes da realização da consulta.
		startFetch : function() {
			util.showSpinner('spinCentroCusto');
		},
	});

	return CentroCustoModal;
});
