/* generated: 29/09/2016 12:48:00 */
define(function(require) {
	// Start "Import´s" Definition"
	var _ = require('adapters/underscore-adapter');
	var $ = require('adapters/jquery-adapter');
	var Col = require('adapters/col-adapter');
	var Backbone = require('adapters/backbone-adapter');
	var Marionette = require('marionette');
	var Backgrid = require('adapters/backgrid-adapter');
	var util = require('utilities/utils');
	var RadioButtonCell = require('views/components/RadioButtonCell');
	var Counter = require('views/components/Counter');
	var RowClick = require('views/components/CustomClickedRow');
	var Combobox = require('views/components/Combobox');
	var CustomNumberCell = require('views/components/CustomNumberCell');

	var FuncionarioModal = require('text!views/modalComponents/tpl/FuncionarioModalTemplate.html');
	var FuncionarioPageCollection = require('collections/FuncionarioPageCollection');
	var CentroCustoCollection = require('collections/CentroCustoCollection');			

	// End of "Import´s" definition
	// #####################################################################################################
	// .............................................MAIN-BODY.............................................
	// #####################################################################################################

	var FuncionarioModal = Marionette.LayoutView.extend({
		template : _.template(FuncionarioModal),

		events : {
			'click #btnSearchFuncionario' : 'searchFuncionario',
			'click #btnClearFuncionario' : 'clearModal',
			'click tr' : 'selectRow',
			'keypress' : 'treatKeypress',
		},

		regions : {
			counterRegion : 	'#counter-funcionario',
			gridRegion : 		'#grid-funcionario',
			paginatorRegion : 	'#paginator-funcionario',
		},

		ui : {
    		inputModalNome : '#inputModalNome',
    		inputModalEmail : '#inputModalEmail',
    		inputModalTelefone : '#inputModalTelefone',
    		inputModalLimiteMensal : '#inputModalLimiteMensal',
    		inputModalCpf : '#inputModalCpf',
    		inputModalMatricula : '#inputModalMatricula',
    		inputModalStatusFuncionario : '#inputModalStatusFuncionario',
    		inputModalAutorizadoTicket : '#inputModalAutorizadoTicket',
    		inputModalObservacao : '#inputModalObservacao',
			inputModalCentroCusto : '#inputModalCentroCusto', 
		
			form : '#formSearchFuncionario',
			modalScreen : '.modal',
		},
		treatKeypress : function (e){
		    if (util.enterPressed(e)) {
	    		e.preventDefault();
	    		this.searchFuncionario();
	    	}
		},

		initialize : function(opt) {
			var that = this;

			this.onSelectModel = opt.onSelectModel;

			this.funcionarioCollection = new FuncionarioPageCollection();
			this.funcionarioCollection.state.pageSize = 5;
			this.funcionarioCollection.on('fetching', this.startFetch, this);
			this.funcionarioCollection.on('fetched', this.stopFetch, this);

			this.grid = new Backgrid.Grid({
				row : RowClick,
				className : 'table backgrid table-striped table-bordered table-hover dataTable no-footer  ',
				columns : this.getColumns(),
				emptyText : "Sem registros",
				collection : this.funcionarioCollection,
				emptyText : "Sem registros para exibir."

			});
			
			this.counter = new Counter({
				collection : this.funcionarioCollection ,
			});
			

			this.paginator = new Backgrid.Extension.Paginator({
				columns : this.getColumns(),
				collection : this.funcionarioCollection,
				className : 'dataTables_paginate paging_simple_numbers',
				uiClassName : 'pagination',
			});

			this.on('show', function() {
				that.gridRegion.show(that.grid);
				that.counterRegion.show(that.counter);
				that.paginatorRegion.show(that.paginator);
				var comboCentroCusto = new Combobox({
					el : this.ui.inputModalCentroCusto,
					comboId : 'id',
					comboVal : 'nome',
					collectionEntity : CentroCustoCollection, //provavelmente vá ocorrer um erro pois CentroCustoCollection não foi declarado
				});
			});
		},

		selectRow : function(e) {
			var modelFuncionario = util.getWrappedModel(e);
			if (modelFuncionario)
				this.onSelectModel(modelFuncionario);
		},
		
		getColumns : function() {
			var columns = [	

			{
				name : "nome",
				editable : false,
				sortable : true,
				label 	 : "Nome",
				cell 	 : "string",
			}, 
			{
				name : "email",
				editable : false,
				sortable : true,
				label 	 : "Email",
				cell 	 : "string",
			}, 
			{
				name : "telefone",
				editable : false,
				sortable : true,
				label 	 : "Telefone",
				cell 	 : "string",
			}, 
			{
				name : "limiteMensal",
				editable : false,
				sortable : true,
				label 	 : "Limite mensal",
				cell : CustomNumberCell.extend({}),
			}, 
			{
				name : "cpf",
				editable : false,
				sortable : true,
				label 	 : "Cpf",
				cell 	 : "string",
			}, 
			{
				name : "matricula",
				editable : false,
				sortable : true,
				label 	 : "Matricula",
				cell 	 : "string",
			}, 
			{
				name : "statusFuncionario",
				editable : false,
				sortable : true,
				label 	 : "Status funcionario",
				cell 	 : "string",
			}, 
			{
				name : "autorizadoTicket",
				editable : false,
				sortable : true,
				label 	 : "Autorizado ticket",
				cell 	 : "string",
			}, 
			{
				name : "observacao",
				editable : false,
				sortable : true,
				label 	 : "Observacao",
				cell 	 : "string",
			}, 
			];
			return columns;
		},

		clearFields : function() {
			util.clear('inputModalId');
			util.clear('inputModalNome'); 
			util.clear('inputModalEmail'); 
			util.clear('inputModalTelefone'); 
			util.clear('inputModalLimiteMensal'); 
			util.clear('inputModalCpf'); 
			util.clear('inputModalMatricula'); 
			util.clear('inputModalStatusFuncionario'); 
			util.clear('inputModalAutorizadoTicket'); 
			util.clear('inputModalObservacao'); 
			util.clear('inputModalCentroCusto'); 					 	
			util.scrollUpModal();
		},

		searchFuncionario : function() {
			this.funcionarioCollection.filterQueryParams = {
	    		nome : util.escapeById('inputModalNome'),
	    		email : util.escapeById('inputModalEmail'),
	    		telefone : util.escapeById('inputModalTelefone'),
	    		limiteMensal : util.escapeById('inputModalLimiteMensal'),
	    		cpf : util.escapeById('inputModalCpf'),
	    		matricula : util.escapeById('inputModalMatricula'),
	    		statusFuncionario : util.escapeById('inputModalStatusFuncionario'),
	    		autorizadoTicket : util.escapeById('inputModalAutorizadoTicket'),
	    		observacao : util.escapeById('inputModalObservacao'),
			};

			this.funcionarioCollection.fetch({
				resetState : true,
				success : function(_coll, _resp, _opt) {
					//caso queira algum tratamento de sucesso adicional
				},
				error : function(_coll, _resp, _opt) {
					console.error(_coll, _resp, _opt)
				}
			});
		},

		hidePage : function() {
			this.ui.modalScreen.modal('hide');
		},

		showPage : function() {
			this.clearModal();

			this.ui.modalScreen.modal('show');
			this.funcionarioCollection.getFirstPage({
				success : function(_col, _resp, _opts) {
					//caso queira algum tratamento de sucesso adicional
				},
				error : function(_col, _resp, _opts) {
					console.error(_resp.responseText || (_resp.getResponseHeader && _resp.getResponseHeader('exception')));
				}
			});
		},

		clearModal : function() {
			this.clearFields();
			this.ui.form.get(0).reset();
			this.funcionarioCollection.reset();
		},
		
		// Executada depois da consulta concluida.
		stopFetch : function() {
			util.stopSpinner();
			util.scrollDownModal();
		},
		
		// Executada Antes da realização da consulta.
		startFetch : function() {
			util.showSpinner('spinFuncionario');
		},
	});

	return FuncionarioModal;
});
