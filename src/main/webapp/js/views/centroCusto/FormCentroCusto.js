/* generated: 29/09/2016 12:48:00 */
define(function(require) {
	// Start "Import´s" Definition"
	var _ = require('adapters/underscore-adapter');
	var $ = require('adapters/jquery-adapter');
	var Col = require('adapters/col-adapter');
	var Backbone = require('adapters/backbone-adapter');
	var Marionette = require('marionette');
	var Backgrid = require('adapters/backgrid-adapter');
	var util = require('utilities/utils');
	var Combobox = require('views/components/Combobox');

	var TemplateFormCentroCustos = require('text!views/centroCusto/tpl/FormCentroCustoTemplate.html');
	var CentroCustoModel = require('models/CentroCustoModel');
	var CentroCustoCollection = require('collections/CentroCustoCollection');
	var SearchEmpresaModal = require('views/modalComponents/EmpresaModal');
	
	// End of "Import´s" definition

	// #####################################################################################################
	// ¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨MAIN BODY¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨
	// #####################################################################################################

	var FormCentroCustos = Marionette.LayoutView.extend({
		template : _.template(TemplateFormCentroCustos),

		regions : {
			searchEmpresaModalRegion : '#empresaModal',
		},

		events : {
			'click 	.save' : 'save',
			'click 	.saveAndContinue' : 'saveAndContinue',
			'click #searchEmpresaModal' : 'showSearchEmpresaModal',
		},
		
		ui : {
			inputId : '#inputId',
			inputDescricao : '#inputDescricao',
			inputValorLimite : '#inputValorLimite',
			inputObservacao : '#inputObservacao',
			inputObseracaoTaxista : '#inputObseracaoTaxista',
			inputInativo : '#inputInativo',
		
			inputEmpresaId : '#inputEmpresaId',
			inputEmpresaRazaoSocial : '#inputEmpresaRazaoSocial',
			form : '#formCentroCusto',
		},

		initialize : function() {
			var that = this;
			this.searchEmpresaModal = new SearchEmpresaModal({
				onSelectModel : function(model) {
					that.selectEmpresa(model);
				},
			});
			this.on('show', function() {
				this.searchEmpresaModalRegion.show(this.searchEmpresaModal);		
				this.ui.inputValorLimite.formatNumber(2);
				this.ui.form.validationEngine('attach', {
					promptPosition : "topLeft",
					isOverflown : false,
					validationEventTrigger : "change"
				});
			});
		},

		saveAndContinue : function() {
			this.save(true)
		},

		save : function(continua) {
			var that = this;
			var centroCusto = that.getModel();

			if (this.isValid()) {
				centroCusto.save({}, {
					success : function(_model, _resp, _options) {
						util.showSuccessMessage('Centro custo salvo com sucesso!');
						that.clearForm();

						if (continua != true) {
							util.goPage('app/centroCustos');
						}
					},

					error : function(_model, _resp, _options) {
						util.showErrorMessage('Problema ao salvar registro',_resp);
					}
				});
			} else {
				util.showMessage('error', 'Verifique campos em destaque!');
			}
		},

		
		clearForm : function() {
			util.clear('inputId');
			util.clear('inputDescricao'); 
			util.clear('inputValorLimite'); 
			util.clear('inputObservacao'); 
			util.clear('inputObseracaoTaxista'); 
			util.clear('inputInativo'); 
			util.clear('inputEmpresaId');
			util.clear('inputEmpresaRazaoSocial');
		},

		possuiCamposInvalidos : function() {
			return util.hasInvalidFields(this.validateFields);
		},

		isValid : function() {
			return this.ui.form.validationEngine('validate', {
				promptPosition : "topLeft",
				isOverflown : false,
				validationEventTrigger : "change"
			});
		},

		getModel : function() {
			var that = this;
			var centroCusto = that.model; 
			centroCusto.set({
				id: util.escapeById('inputId') || null,
		    	descricao : util.escapeById('inputDescricao'), 
				
		    	valorLimite : util.escapeById('inputValorLimite', true), 
				
		    	observacao : util.escapeById('inputObservacao'), 
				
		    	obseracaoTaxista : util.escapeById('inputObseracaoTaxista'), 
				
		    	inativo : util.escapeById('inputInativo'), 
				
					empresa : that.getEmpresa(),
			});
			return centroCusto;
		},
		 
		getEmpresa : function() {			
			var id = util.escapeById('inputEmpresaId');
			var razaoSocial = util.escapeById('inputEmpresaRazaoSocial');
			var empresa = null;
			
			if (id && razaoSocial) {
				empresa = {
					id : id,
					razaoSocial : razaoSocial,
				}
			}
			return empresa;
		},	
		
		showSearchEmpresaModal : function() {
			this.searchEmpresaModal.showPage();
		},
			
		selectEmpresa : function(empresa) {
			this.searchEmpresaModal.hidePage();	
			this.ui.inputEmpresaId.val(empresa.get('id'));
			this.ui.inputEmpresaRazaoSocial.val(empresa.get('razaoSocial'));		
		},
				
		
	});

	return FormCentroCustos;
});