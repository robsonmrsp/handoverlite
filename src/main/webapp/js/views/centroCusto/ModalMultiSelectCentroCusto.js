/* generated: 29/09/2016 12:48:00 */
define(function(require) {
	// Start "Import´s Definition"
	var _ = require('adapters/underscore-adapter');
	var $ = require('adapters/jquery-adapter');
	var Col = require('adapters/col-adapter');
	var Backbone = require('adapters/backbone-adapter');
	var Backgrid = require('adapters/backgrid-adapter');
	var Marionette = require('marionette');
	var CustomNumberCell = require('views/components/CustomNumberCell');

	var util = require('utilities/utils');
	var CentroCustoPageCollection = require('collections/CentroCustoPageCollection');
	var ModalMultiSelectCentroCustoTemplate = require('text!views/centroCusto/tpl/ModalMultiSelectCentroCustoTemplate.html');
	// End of "Import´s" definition

	var ModalCentroCustos = Marionette.LayoutView.extend({
		template : _.template(ModalMultiSelectCentroCustoTemplate),

		regions : {
			gridRegion : '#grid-centroCustos-modal',
			paginatorRegion : '#paginator-centroCustos-modal',
		},
		events : {
			'click .btnOk' : 'close'
		},
		ui : {
			btnOk : ".btnOk",
		},

		initialize : function(opt) {
			var that = this;

			this.projetoCentroCustos = this.collection;
			
			this.centroCustos = new CentroCustoPageCollection();
			this.centroCustos.on('fetched', this.endFetch, this);
			this.centroCustos.on('backgrid:selected', this.selectModel, this);

			this.grid = new Backgrid.Grid({
				className : 'table backgrid table-striped table-bordered table-hover dataTable no-footer  ',
				columns : this.getColumns(),
				emptyText : "Nenhum registro para escolha",
				collection : this.centroCustos,
			});

			this.paginator = new Backgrid.Extension.Paginator({
				columns : this.getColumns(),
				collection : this.centroCustos,
				className : 'dataTables_paginate paging_simple_numbers',
				uiClassName : 'pagination',
			});

			this.centroCustos.getFirstPage({
				success : function(_col, _resp, _opts) {
					console.info('Primeira pagina do grid centroCusto');
				},
				error : function(_col, _resp, _opts) {
					console.error(_resp.responseText || _resp.getResponseHeader('exception'));
				}
			});


			this.on('show', function() {
				that.gridRegion.show(that.grid);
				that.paginatorRegion.show(that.paginator);
			});
		},

		selectModel : function(model, checked) {
			if (checked)
				this.projetoCentroCustos.add(model)
			else
				this.projetoCentroCustos.remove(model)
		},

		endFetch : function(_collection) {
			var that = this;
			this.centroCustos.each(function(model) {
				if (that.projetoCentroCustos.findWhere({
					id : model.get('id')
				})) {
					model.trigger("backgrid:select", model, true);
				}
			});
		},
		clear : function(){
			this.grid.$el.find('input[type=checkbox]').prop('checked', false);
		},
		getColumns : function() {
			var columns = [{
				name : "",
				cell : "select-row",
				headerCell : "select-all"
			}, 
			 
			{
				name : "descricao",
				editable : false,
				sortable : false,
				label 	 : "Descricao",
				cell 	 : "string",
			}, 
			{
				name : "valorLimite",
				editable : false,
				sortable : false,
				label 	 : "Valor limite",
				cell : CustomNumberCell.extend({}),
			}, 
			{
				name : "observacao",
				editable : false,
				sortable : false,
				label 	 : "Observacao",
				cell 	 : "string",
			}, 
			{
				name : "obseracaoTaxista",
				editable : false,
				sortable : false,
				label 	 : "Obseracao taxista",
				cell 	 : "string",
			}, 
			{
				name : "inativo",
				editable : false,
				sortable : false,
				label 	 : "Inativo",
				cell 	 : "string",
			}, 
			];
			return columns;
		},
	});

	return ModalCentroCustos;
});
