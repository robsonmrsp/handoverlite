/* generated: 29/09/2016 12:48:00 */
define(function(require) {
	// Start "Import´s" Definition"
	var _ = require('adapters/underscore-adapter');
	var $ = require('adapters/jquery-adapter');
	var Col = require('adapters/col-adapter');
	var Backbone = require('adapters/backbone-adapter');
	var Backgrid = require('adapters/backgrid-adapter');
	var Marionette = require('marionette');
	var util = require('utilities/utils');
	var BaseModel = require('models/BaseModel');
	var CustomNumberCell = require('views/components/CustomNumberCell');

	var ModalMultiSelectCentroCusto = require('views/centroCusto/ModalMultiSelectCentroCusto');
	var MultiSelectCentroCustoTemplate = require('text!views/centroCusto/tpl/MultiSelectCentroCustoTemplate.html');

	var MultiSelectCentroCusto = Marionette.LayoutView.extend({
		template : _.template(MultiSelectCentroCustoTemplate),

		regions : {
			modalMultiSelectCentroCustoRegion : '#modalMultiSelectCentroCustos',
			gridCentroCustosModalRegion : '#gridMultiselectCentroCustos',
		},

		initialize : function() {
			var that = this;

			this.centroCustos = this.collection;

			this.gridCentroCustos = new Backgrid.Grid({
				className : 'table backgrid table-striped table-bordered table-hover dataTable no-footer  ',
				columns : this._getColumns(),
				emptyText : "Sem registros adicionados",
				collection : this.centroCustos,
			});

			this.modalMultiSelectCentroCusto = new ModalMultiSelectCentroCusto({
				collection : this.collection
			});

			this.on('show', function() {
				that.modalMultiSelectCentroCustoRegion.show(that.modalMultiSelectCentroCusto);
				that.gridCentroCustosModalRegion.show(that.gridCentroCustos);
			});
		},
		clear : function(){
			this.modalMultiSelectCentroCusto.clear();
		},
		
		_getColumns : function() {
			var columns = [

			{
				name : "descricao",
				editable : false,
				sortable : false,
				label 	 : "Descricao",
				cell 	 : "string",
			}, 
			{
				name : "valorLimite",
				editable : false,
				sortable : false,
				label 	 : "Valor limite",
				cell : CustomNumberCell.extend({}),
			}, 
			{
				name : "observacao",
				editable : false,
				sortable : false,
				label 	 : "Observacao",
				cell 	 : "string",
			}, 
			{
				name : "obseracaoTaxista",
				editable : false,
				sortable : false,
				label 	 : "Obseracao taxista",
				cell 	 : "string",
			}, 
			{
				name : "inativo",
				editable : false,
				sortable : false,
				label 	 : "Inativo",
				cell 	 : "string",
			}, 
			];
			return columns;
		},
	});

	return MultiSelectCentroCusto
});
