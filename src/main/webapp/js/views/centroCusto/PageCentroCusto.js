/* generated: 29/09/2016 12:48:00 */
define(function(require) {
	// Start "Import´s Definition"
	var _ = require('adapters/underscore-adapter');
	var $ = require('adapters/jquery-adapter');
	var Col = require('adapters/col-adapter');
	var Backbone = require('adapters/backbone-adapter');
	var Marionette = require('marionette');
	var Backgrid = require('adapters/backgrid-adapter');
	var util = require('utilities/utils');
	var Combobox = require('views/components/Combobox');
	var CustomStringCell = require('views/components/CustomStringCell');
	var Counter = require('views/components/Counter');
	var ActionsCell = require('views/components/ActionsCell');
	var GeneralActionsCell = require('views/components/GeneralActionsCell');

	var CustomNumberCell = require('views/components/CustomNumberCell');

	var TemplateFormCentroCustos = require('text!views/centroCusto/tpl/FormCentroCustoTemplate.html');
	var CentroCustoModel = require('models/CentroCustoModel');
	var CentroCustoCollection = require('collections/CentroCustoCollection');
	var CentroCustoPageCollection = require('collections/CentroCustoPageCollection');
	var PageCentroCustoTemplate = require('text!views/centroCusto/tpl/PageCentroCustoTemplate.html');
	
	//Filter import
	var SearchEmpresaModal = require('views/modalComponents/EmpresaModal');
	
	// End of "Import´s" definition

	var PageCentroCusto = Marionette.LayoutView.extend({
		template : _.template(PageCentroCustoTemplate),

		regions : {
			gridRegion : '#grid',
			counterRegion : '#counter',
			paginatorRegion : '#paginator',
			searchEmpresaModalRegion : '#empresaModal',
		},
		
		events : {
			'click 	#reset' : 'resetCentroCusto',			
			'click #searchEmpresaModal' : 'showSearchEmpresaModal',
			'keypress' : 'treatKeypress',
			
			'click 	.search-button' : 'searchCentroCusto',
			'click .show-advanced-search-button' : 'toggleAdvancedForm',
		},
		
		
		ui : {
			inputDescricao : '#inputDescricao',
			inputValorLimite : '#inputValorLimite',
			inputObservacao : '#inputObservacao',
			inputObseracaoTaxista : '#inputObseracaoTaxista',
			inputInativo : '#inputInativo',
		
			inputEmpresaId : '#inputEmpresaId',
			inputEmpresaRazaoSocial : '#inputEmpresaRazaoSocial',
			form : '#formCentroCustoFilter',
			advancedSearchForm : '.advanced-search-form',
		},
		
		toggleAdvancedForm : function() {
			this.ui.advancedSearchForm.slideToggle("slow");
		},

		
		treatKeypress : function (e){
		    if (util.enterPressed(e)) {
	    		e.preventDefault();
	    		this.searchCentroCusto();
	    	}
		},

		initialize : function() {
			var that = this;

			this.centroCustos = new CentroCustoPageCollection();

			this.grid = new Backgrid.Grid({
				className : 'table backgrid table-striped table-bordered table-hover dataTable no-footer  ',
				columns : this.getColumns(),
				emptyText : "Sem registros",
				collection : this.centroCustos
			});

			this.counter = new Counter({
				collection : this.centroCustos,
			});

			this.paginator = new Backgrid.Extension.Paginator({
				columns : this.getColumns(),
				collection : this.centroCustos,
				className : ' paging_simple_numbers',
				uiClassName : 'pagination',
			});

			this.centroCustos.getFirstPage({
				success : function(_col, _resp, _opts) {
					console.info('Primeira pagina do grid centroCusto');
				},
				error : function(_col, _resp, _opts) {
					console.error(_resp.responseText || (_resp.getResponseHeader && _resp.getResponseHeader('exception')) );
				}
			});
			this.searchEmpresaModal = new SearchEmpresaModal({
				onSelectModel : function(model) {
					that.selectEmpresa(model);
				},
			});
			this.on('show', function() {
				that.gridRegion.show(that.grid);
				that.counterRegion.show(that.counter);
				that.paginatorRegion.show(that.paginator);
				this.searchEmpresaModalRegion.show(this.searchEmpresaModal);		
				this.ui.inputValorLimite.formatNumber(2);
		
			});
		},
		 
		searchCentroCusto : function(){
			var that = this;

			this.centroCustos.filterQueryParams = {
	    		descricao : util.escapeById('inputDescricao'),
	    		valorLimite : util.escapeById('inputValorLimite'),
	    		observacao : util.escapeById('inputObservacao'),
	    		obseracaoTaxista : util.escapeById('inputObseracaoTaxista'),
	    		inativo : util.escapeById('inputInativo'),
			    empresa : util.escapeById('inputEmpresaId'), 
			}
			this.centroCustos.fetch({
				success : function(_coll, _resp, _opt) {
					console.info('Consulta para o grid centroCusto');
				},
				error : function(_coll, _resp, _opt) {
					console.error(_resp.responseText || (_resp.getResponseHeader && _resp.getResponseHeader('exception')));
				},
				complete : function() {
					
				},
			})		
		},
		resetCentroCusto : function(){
			this.ui.form.get(0).reset();
			this.centroCustos.reset();
			util.clear('inputEmpresaId');
		},
				
		getColumns : function() {
			var that = this;
			var columns = [
			{
				name : "descricao",
				editable : false,
				sortable : true,
				label 	 : "Descricao",
				cell 	 : "string",
			}, 
			{
				name : "valorLimite",
				editable : false,
				sortable : true,
				label 	 : "Valor limite",
				cell : CustomNumberCell.extend({}),
			}, 
			{
				name : "observacao",
				editable : false,
				sortable : true,
				label 	 : "Observacao",
				cell 	 : "string",
			}, 
			{
				name : "obseracaoTaxista",
				editable : false,
				sortable : true,
				label 	 : "Obseracao taxista",
				cell 	 : "string",
			}, 
			{
				name : "inativo",
				editable : false,
				sortable : true,
				label 	 : "Inativo",
				cell 	 : "string",
			}, 
			{
				name : "empresa.razaoSocial",
				editable : false,
				sortable : true,  
				label : "Empresa",
				cell : CustomStringCell.extend({
					fieldName : 'empresa.razaoSocial',
				}),
			},	
			{
				name : "acoes",
				label : "Ações(Editar, Deletar)",
				sortable : false,
				cell : GeneralActionsCell.extend({
					buttons : that.getCellButtons(),
					context : that,
				})
			} ];
			return columns;
		},
		
		getCellButtons : function() {
			var that = this;
			var buttons = [];

			buttons.push({
				id : 'edita_ficha_button',
				type : 'primary',
				icon : 'icon-pencil fa-pencil',
				hint : 'Editar Centro custo',
				onClick : that.editModel,
			}, {
				id : 'delete_button',
				type : 'danger',
				icon : 'icon-trash fa-trash',
				hint : 'Remover Centro custo',
				onClick : that.deleteModel,
			});

			return buttons;
		},

		deleteModel : function(model) {
			var that = this;
			
			var modelTipo = new CentroCustoModel({
				id : model.id,
			});
			
			util.Bootbox.confirm("Tem certeza que deseja remover o registro [ " + model.get('id') + " ] ?", function(yes) {
				if (yes) {
					modelTipo.destroy({
						success : function() {
							that.centroCustos.remove(model);
							util.showSuccessMessage('Centro custo removido com sucesso!');
						},
						error : function(_model, _resp) {
							util.showErrorMessage('Problema ao remover o registro',_resp);
						}
					});
				}
			});
		},

		editModel : function(model) {
			util.goPage("app/editCentroCusto/" + model.get('id'));
		},

		showSearchEmpresaModal : function() {
			this.searchEmpresaModal.showPage();
		},
			
		selectEmpresa : function(empresa) {
			this.searchEmpresaModal.hidePage();	
			this.ui.inputEmpresaId.val(empresa.get('id'));
			this.ui.inputEmpresaRazaoSocial.val(empresa.get('razaoSocial'));		
		},
		

	});

	return PageCentroCusto;
});
