/* generated: 29/09/2016 12:48:00 */
define(function(require) {
	// Start "Import´s" Definition"
	var _ = require('adapters/underscore-adapter');
	var $ = require('adapters/jquery-adapter');
	var Col = require('adapters/col-adapter');
	var Backbone = require('adapters/backbone-adapter');
	var Backgrid = require('adapters/backgrid-adapter');
	var Marionette = require('marionette');
	var util = require('utilities/utils');
	var BaseModel = require('models/BaseModel');
	var CustomNumberCell = require('views/components/CustomNumberCell');

	var ModalMultiSelectEmpresa = require('views/empresa/ModalMultiSelectEmpresa');
	var MultiSelectEmpresaTemplate = require('text!views/empresa/tpl/MultiSelectEmpresaTemplate.html');

	var MultiSelectEmpresa = Marionette.LayoutView.extend({
		template : _.template(MultiSelectEmpresaTemplate),

		regions : {
			modalMultiSelectEmpresaRegion : '#modalMultiSelectEmpresas',
			gridEmpresasModalRegion : '#gridMultiselectEmpresas',
		},

		initialize : function() {
			var that = this;

			this.empresas = this.collection;

			this.gridEmpresas = new Backgrid.Grid({
				className : 'table backgrid table-striped table-bordered table-hover dataTable no-footer  ',
				columns : this._getColumns(),
				emptyText : "Sem registros adicionados",
				collection : this.empresas,
			});

			this.modalMultiSelectEmpresa = new ModalMultiSelectEmpresa({
				collection : this.collection
			});

			this.on('show', function() {
				that.modalMultiSelectEmpresaRegion.show(that.modalMultiSelectEmpresa);
				that.gridEmpresasModalRegion.show(that.gridEmpresas);
			});
		},
		clear : function(){
			this.modalMultiSelectEmpresa.clear();
		},
		
		_getColumns : function() {
			var columns = [

			{
				name : "cnpj",
				editable : false,
				sortable : false,
				label 	 : "Cnpj",
				cell 	 : "string",
			}, 
			{
				name : "numeroContrato",
				editable : false,
				sortable : false,
				label 	 : "Numero do Contrato",
				cell 	 : "string",
			}, 
			{
				name : "razaoSocial",
				editable : false,
				sortable : false,
				label 	 : "Razao social",
				cell 	 : "string",
			}, 
			{
				name : "nomeFantasia",
				editable : false,
				sortable : false,
				label 	 : "Nome fantasia",
				cell 	 : "string",
			}, 
			{
				name : "utilizaVoucher",
				editable : false,
				sortable : false,
				label 	 : "Utiliza voucher",
				cell 	 : "string",
			}, 
			{
				name : "utilizaTicket",
				editable : false,
				sortable : false,
				label 	 : "Utiliza ticket",
				cell 	 : "string",
			}, 
			{
				name : "observacaoTaxista",
				editable : false,
				sortable : false,
				label 	 : "Observacao taxista",
				cell 	 : "string",
			}, 
			{
				name : "observacaoCentral",
				editable : false,
				sortable : false,
				label 	 : "Observacao central",
				cell 	 : "string",
			}, 
			{
				name : "percursoDesconto",
				editable : false,
				sortable : false,
				label 	 : "Percurso desconto",
				cell : CustomNumberCell.extend({}),
			}, 
			{
				name : "inscricaoMunicipal",
				editable : false,
				sortable : false,
				label 	 : "Inscricao municipal",
				cell 	 : "string",
			}, 
			{
				name : "inscricaoEstadual",
				editable : false,
				sortable : false,
				label 	 : "Inscricao estadual",
				cell 	 : "string",
			}, 
			{
				name : "nf",
				editable : false,
				sortable : false,
				label 	 : "Nf",
				cell 	 : "string",
			}, 
			{
				name : "percentualIss",
				editable : false,
				sortable : false,
				label 	 : "Percentual ISS",
				cell : CustomNumberCell.extend({}),
			}, 
			{
				name : "percentualIrf",
				editable : false,
				sortable : false,
				label 	 : "Percentual IRF",
				cell : CustomNumberCell.extend({}),
			}, 
			{
				name : "percentualInss",
				editable : false,
				sortable : false,
				label 	 : "Percentual INSS",
				cell : CustomNumberCell.extend({}),
			}, 
			{
				name : "diaVencimento",
				editable : false,
				sortable : false,
				label 	 : "Dia vencimento",
				cell : CustomNumberCell.extend({}),
			}, 
			{
				name : "percentualMotorista",
				editable : false,
				sortable : false,
				label 	 : "Percentual motorista",
				cell : CustomNumberCell.extend({}),
			}, 
			{
				name : "banco",
				editable : false,
				sortable : false,
				label 	 : "Banco",
				cell : CustomNumberCell.extend({}),
			}, 
			{
				name : "agencia",
				editable : false,
				sortable : false,
				label 	 : "Agencia",
				cell 	 : "string",
			}, 
			{
				name : "conta",
				editable : false,
				sortable : false,
				label 	 : "Conta",
				cell 	 : "string",
			}, 
			{
				name : "email",
				editable : false,
				sortable : false,
				label 	 : "Email",
				cell 	 : "string",
			}, 
			];
			return columns;
		},
	});

	return MultiSelectEmpresa
});
