/* generated: 29/09/2016 12:48:00 */
define(function(require) {
	// Start "Import´s Definition"
	var _ = require('adapters/underscore-adapter');
	var $ = require('adapters/jquery-adapter');
	var Col = require('adapters/col-adapter');
	var Backbone = require('adapters/backbone-adapter');
	var Backgrid = require('adapters/backgrid-adapter');
	var Marionette = require('marionette');
	var CustomNumberCell = require('views/components/CustomNumberCell');

	var util = require('utilities/utils');
	var EmpresaPageCollection = require('collections/EmpresaPageCollection');
	var ModalMultiSelectEmpresaTemplate = require('text!views/empresa/tpl/ModalMultiSelectEmpresaTemplate.html');
	// End of "Import´s" definition

	var ModalEmpresas = Marionette.LayoutView.extend({
		template : _.template(ModalMultiSelectEmpresaTemplate),

		regions : {
			gridRegion : '#grid-empresas-modal',
			paginatorRegion : '#paginator-empresas-modal',
		},
		events : {
			'click .btnOk' : 'close'
		},
		ui : {
			btnOk : ".btnOk",
		},

		initialize : function(opt) {
			var that = this;

			this.projetoEmpresas = this.collection;
			
			this.empresas = new EmpresaPageCollection();
			this.empresas.on('fetched', this.endFetch, this);
			this.empresas.on('backgrid:selected', this.selectModel, this);

			this.grid = new Backgrid.Grid({
				className : 'table backgrid table-striped table-bordered table-hover dataTable no-footer  ',
				columns : this.getColumns(),
				emptyText : "Nenhum registro para escolha",
				collection : this.empresas,
			});

			this.paginator = new Backgrid.Extension.Paginator({
				columns : this.getColumns(),
				collection : this.empresas,
				className : 'dataTables_paginate paging_simple_numbers',
				uiClassName : 'pagination',
			});

			this.empresas.getFirstPage({
				success : function(_col, _resp, _opts) {
					console.info('Primeira pagina do grid empresa');
				},
				error : function(_col, _resp, _opts) {
					console.error(_resp.responseText || _resp.getResponseHeader('exception'));
				}
			});


			this.on('show', function() {
				that.gridRegion.show(that.grid);
				that.paginatorRegion.show(that.paginator);
			});
		},

		selectModel : function(model, checked) {
			if (checked)
				this.projetoEmpresas.add(model)
			else
				this.projetoEmpresas.remove(model)
		},

		endFetch : function(_collection) {
			var that = this;
			this.empresas.each(function(model) {
				if (that.projetoEmpresas.findWhere({
					id : model.get('id')
				})) {
					model.trigger("backgrid:select", model, true);
				}
			});
		},
		clear : function(){
			this.grid.$el.find('input[type=checkbox]').prop('checked', false);
		},
		getColumns : function() {
			var columns = [{
				name : "",
				cell : "select-row",
				headerCell : "select-all"
			}, 
			 
			{
				name : "cnpj",
				editable : false,
				sortable : false,
				label 	 : "Cnpj",
				cell 	 : "string",
			}, 
			{
				name : "numeroContrato",
				editable : false,
				sortable : false,
				label 	 : "Numero do Contrato",
				cell 	 : "string",
			}, 
			{
				name : "razaoSocial",
				editable : false,
				sortable : false,
				label 	 : "Razao social",
				cell 	 : "string",
			}, 
			{
				name : "nomeFantasia",
				editable : false,
				sortable : false,
				label 	 : "Nome fantasia",
				cell 	 : "string",
			}, 
			{
				name : "utilizaVoucher",
				editable : false,
				sortable : false,
				label 	 : "Utiliza voucher",
				cell 	 : "string",
			}, 
			{
				name : "utilizaTicket",
				editable : false,
				sortable : false,
				label 	 : "Utiliza ticket",
				cell 	 : "string",
			}, 
			{
				name : "observacaoTaxista",
				editable : false,
				sortable : false,
				label 	 : "Observacao taxista",
				cell 	 : "string",
			}, 
			{
				name : "observacaoCentral",
				editable : false,
				sortable : false,
				label 	 : "Observacao central",
				cell 	 : "string",
			}, 
			{
				name : "percursoDesconto",
				editable : false,
				sortable : false,
				label 	 : "Percurso desconto",
				cell : CustomNumberCell.extend({}),
			}, 
			{
				name : "inscricaoMunicipal",
				editable : false,
				sortable : false,
				label 	 : "Inscricao municipal",
				cell 	 : "string",
			}, 
			{
				name : "inscricaoEstadual",
				editable : false,
				sortable : false,
				label 	 : "Inscricao estadual",
				cell 	 : "string",
			}, 
			{
				name : "nf",
				editable : false,
				sortable : false,
				label 	 : "Nf",
				cell 	 : "string",
			}, 
			{
				name : "percentualIss",
				editable : false,
				sortable : false,
				label 	 : "Percentual ISS",
				cell : CustomNumberCell.extend({}),
			}, 
			{
				name : "percentualIrf",
				editable : false,
				sortable : false,
				label 	 : "Percentual IRF",
				cell : CustomNumberCell.extend({}),
			}, 
			{
				name : "percentualInss",
				editable : false,
				sortable : false,
				label 	 : "Percentual INSS",
				cell : CustomNumberCell.extend({}),
			}, 
			{
				name : "diaVencimento",
				editable : false,
				sortable : false,
				label 	 : "Dia vencimento",
				cell : CustomNumberCell.extend({}),
			}, 
			{
				name : "percentualMotorista",
				editable : false,
				sortable : false,
				label 	 : "Percentual motorista",
				cell : CustomNumberCell.extend({}),
			}, 
			{
				name : "banco",
				editable : false,
				sortable : false,
				label 	 : "Banco",
				cell : CustomNumberCell.extend({}),
			}, 
			{
				name : "agencia",
				editable : false,
				sortable : false,
				label 	 : "Agencia",
				cell 	 : "string",
			}, 
			{
				name : "conta",
				editable : false,
				sortable : false,
				label 	 : "Conta",
				cell 	 : "string",
			}, 
			{
				name : "email",
				editable : false,
				sortable : false,
				label 	 : "Email",
				cell 	 : "string",
			}, 
			];
			return columns;
		},
	});

	return ModalEmpresas;
});
