/* generated: 29/09/2016 12:48:00 */
define(function(require) {
	// Start "Import´s" Definition"
	var _ = require('adapters/underscore-adapter');
	var $ = require('adapters/jquery-adapter');
	var Col = require('adapters/col-adapter');
	var Backbone = require('adapters/backbone-adapter');
	var Marionette = require('marionette');
	var Backgrid = require('adapters/backgrid-adapter');
	var util = require('utilities/utils');
	var Combobox = require('views/components/Combobox');

	var TemplateFormEmpresas = require('text!views/empresa/tpl/FormEmpresaTemplate.html');
	var EmpresaModel = require('models/EmpresaModel');
	var EmpresaCollection = require('collections/EmpresaCollection');
	
	// End of "Import´s" definition

	// #####################################################################################################
	// ¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨MAIN BODY¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨
	// #####################################################################################################

	var FormEmpresas = Marionette.LayoutView.extend({
		template : _.template(TemplateFormEmpresas),

		regions : {
		},

		events : {
			'click 	.save' : 'save',
			'click 	.saveAndContinue' : 'saveAndContinue',
		},
		
		ui : {
			inputId : '#inputId',
			inputCnpj : '#inputCnpj',
			inputNumeroContrato : '#inputNumeroContrato',
			inputRazaoSocial : '#inputRazaoSocial',
			inputNomeFantasia : '#inputNomeFantasia',
			inputUtilizaVoucher : '#inputUtilizaVoucher',
			inputUtilizaTicket : '#inputUtilizaTicket',
			inputObservacaoTaxista : '#inputObservacaoTaxista',
			inputObservacaoCentral : '#inputObservacaoCentral',
			inputPercursoDesconto : '#inputPercursoDesconto',
			inputInscricaoMunicipal : '#inputInscricaoMunicipal',
			inputInscricaoEstadual : '#inputInscricaoEstadual',
			inputNf : '#inputNf',
			inputPercentualIss : '#inputPercentualIss',
			inputPercentualIrf : '#inputPercentualIrf',
			inputPercentualInss : '#inputPercentualInss',
			inputDiaVencimento : '#inputDiaVencimento',
			inputPercentualMotorista : '#inputPercentualMotorista',
			inputBanco : '#inputBanco',
			inputAgencia : '#inputAgencia',
			inputConta : '#inputConta',
			inputEmail : '#inputEmail',
		
			form : '#formEmpresa',
		},

		initialize : function() {
			var that = this;
			this.on('show', function() {
				this.ui.inputPercursoDesconto.formatNumber(2);
				this.ui.inputPercentualIss.formatNumber(2);
				this.ui.inputPercentualIrf.formatNumber(2);
				this.ui.inputPercentualInss.formatNumber(2);
				this.ui.inputDiaVencimento.formatNumber(2);
				this.ui.inputPercentualMotorista.formatNumber(2);
				this.ui.inputBanco.formatNumber(2);
				this.ui.form.validationEngine('attach', {
					promptPosition : "topLeft",
					isOverflown : false,
					validationEventTrigger : "change"
				});
			});
		},

		saveAndContinue : function() {
			this.save(true)
		},

		save : function(continua) {
			var that = this;
			var empresa = that.getModel();

			if (this.isValid()) {
				empresa.save({}, {
					success : function(_model, _resp, _options) {
						util.showSuccessMessage('Empresa salvo com sucesso!');
						that.clearForm();

						if (continua != true) {
							util.goPage('app/empresas');
						}
					},

					error : function(_model, _resp, _options) {
						util.showErrorMessage('Problema ao salvar registro',_resp);
					}
				});
			} else {
				util.showMessage('error', 'Verifique campos em destaque!');
			}
		},

		
		clearForm : function() {
			util.clear('inputId');
			util.clear('inputCnpj'); 
			util.clear('inputNumeroContrato'); 
			util.clear('inputRazaoSocial'); 
			util.clear('inputNomeFantasia'); 
			util.clear('inputUtilizaVoucher'); 
			util.clear('inputUtilizaTicket'); 
			util.clear('inputObservacaoTaxista'); 
			util.clear('inputObservacaoCentral'); 
			util.clear('inputPercursoDesconto'); 
			util.clear('inputInscricaoMunicipal'); 
			util.clear('inputInscricaoEstadual'); 
			util.clear('inputNf'); 
			util.clear('inputPercentualIss'); 
			util.clear('inputPercentualIrf'); 
			util.clear('inputPercentualInss'); 
			util.clear('inputDiaVencimento'); 
			util.clear('inputPercentualMotorista'); 
			util.clear('inputBanco'); 
			util.clear('inputAgencia'); 
			util.clear('inputConta'); 
			util.clear('inputEmail'); 
		},

		possuiCamposInvalidos : function() {
			return util.hasInvalidFields(this.validateFields);
		},

		isValid : function() {
			return this.ui.form.validationEngine('validate', {
				promptPosition : "topLeft",
				isOverflown : false,
				validationEventTrigger : "change"
			});
		},

		getModel : function() {
			var that = this;
			var empresa = that.model; 
			empresa.set({
				id: util.escapeById('inputId') || null,
		    	cnpj : util.escapeById('inputCnpj'), 
				
		    	numeroContrato : util.escapeById('inputNumeroContrato'), 
				
		    	razaoSocial : util.escapeById('inputRazaoSocial'), 
				
		    	nomeFantasia : util.escapeById('inputNomeFantasia'), 
				
		    	utilizaVoucher : util.escapeById('inputUtilizaVoucher'), 
				
		    	utilizaTicket : util.escapeById('inputUtilizaTicket'), 
				
		    	observacaoTaxista : util.escapeById('inputObservacaoTaxista'), 
				
		    	observacaoCentral : util.escapeById('inputObservacaoCentral'), 
				
		    	percursoDesconto : util.escapeById('inputPercursoDesconto', true), 
				
		    	inscricaoMunicipal : util.escapeById('inputInscricaoMunicipal'), 
				
		    	inscricaoEstadual : util.escapeById('inputInscricaoEstadual'), 
				
		    	nf : util.escapeById('inputNf'), 
				
		    	percentualIss : util.escapeById('inputPercentualIss', true), 
				
		    	percentualIrf : util.escapeById('inputPercentualIrf', true), 
				
		    	percentualInss : util.escapeById('inputPercentualInss', true), 
				
		    	diaVencimento : util.escapeById('inputDiaVencimento', true), 
				
		    	percentualMotorista : util.escapeById('inputPercentualMotorista', true), 
				
		    	banco : util.escapeById('inputBanco', true), 
				
		    	agencia : util.escapeById('inputAgencia'), 
				
		    	conta : util.escapeById('inputConta'), 
				
		    	email : util.escapeById('inputEmail'), 
				
			});
			return empresa;
		},
		 
		
				
		
	});

	return FormEmpresas;
});