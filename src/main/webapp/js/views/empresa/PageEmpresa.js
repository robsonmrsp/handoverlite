/* generated: 29/09/2016 12:48:00 */
define(function(require) {
	// Start "Import´s Definition"
	var _ = require('adapters/underscore-adapter');
	var $ = require('adapters/jquery-adapter');
	var Col = require('adapters/col-adapter');
	var Backbone = require('adapters/backbone-adapter');
	var Marionette = require('marionette');
	var Backgrid = require('adapters/backgrid-adapter');
	var util = require('utilities/utils');
	var Combobox = require('views/components/Combobox');
	var CustomStringCell = require('views/components/CustomStringCell');
	var Counter = require('views/components/Counter');
	var ActionsCell = require('views/components/ActionsCell');
	var GeneralActionsCell = require('views/components/GeneralActionsCell');

	var CustomNumberCell = require('views/components/CustomNumberCell');

	var TemplateFormEmpresas = require('text!views/empresa/tpl/FormEmpresaTemplate.html');
	var EmpresaModel = require('models/EmpresaModel');
	var EmpresaCollection = require('collections/EmpresaCollection');
	var EmpresaPageCollection = require('collections/EmpresaPageCollection');
	var PageEmpresaTemplate = require('text!views/empresa/tpl/PageEmpresaTemplate.html');
	
	//Filter import
	
	// End of "Import´s" definition

	var PageEmpresa = Marionette.LayoutView.extend({
		template : _.template(PageEmpresaTemplate),

		regions : {
			gridRegion : '#grid',
			counterRegion : '#counter',
			paginatorRegion : '#paginator',
		},
		
		events : {
			'click 	#reset' : 'resetEmpresa',			
			'keypress' : 'treatKeypress',
			
			'click 	.search-button' : 'searchEmpresa',
			'click .show-advanced-search-button' : 'toggleAdvancedForm',
		},
		
		
		ui : {
			inputCnpj : '#inputCnpj',
			inputNumeroContrato : '#inputNumeroContrato',
			inputRazaoSocial : '#inputRazaoSocial',
			inputNomeFantasia : '#inputNomeFantasia',
			inputUtilizaVoucher : '#inputUtilizaVoucher',
			inputUtilizaTicket : '#inputUtilizaTicket',
			inputObservacaoTaxista : '#inputObservacaoTaxista',
			inputObservacaoCentral : '#inputObservacaoCentral',
			inputPercursoDesconto : '#inputPercursoDesconto',
			inputInscricaoMunicipal : '#inputInscricaoMunicipal',
			inputInscricaoEstadual : '#inputInscricaoEstadual',
			inputNf : '#inputNf',
			inputPercentualIss : '#inputPercentualIss',
			inputPercentualIrf : '#inputPercentualIrf',
			inputPercentualInss : '#inputPercentualInss',
			inputDiaVencimento : '#inputDiaVencimento',
			inputPercentualMotorista : '#inputPercentualMotorista',
			inputBanco : '#inputBanco',
			inputAgencia : '#inputAgencia',
			inputConta : '#inputConta',
			inputEmail : '#inputEmail',
		
			form : '#formEmpresaFilter',
			advancedSearchForm : '.advanced-search-form',
		},
		
		toggleAdvancedForm : function() {
			this.ui.advancedSearchForm.slideToggle("slow");
		},

		
		treatKeypress : function (e){
		    if (util.enterPressed(e)) {
	    		e.preventDefault();
	    		this.searchEmpresa();
	    	}
		},

		initialize : function() {
			var that = this;

			this.empresas = new EmpresaPageCollection();

			this.grid = new Backgrid.Grid({
				className : 'table backgrid table-striped table-bordered table-hover dataTable no-footer  ',
				columns : this.getColumns(),
				emptyText : "Sem registros",
				collection : this.empresas
			});

			this.counter = new Counter({
				collection : this.empresas,
			});

			this.paginator = new Backgrid.Extension.Paginator({
				columns : this.getColumns(),
				collection : this.empresas,
				className : ' paging_simple_numbers',
				uiClassName : 'pagination',
			});

			this.empresas.getFirstPage({
				success : function(_col, _resp, _opts) {
					console.info('Primeira pagina do grid empresa');
				},
				error : function(_col, _resp, _opts) {
					console.error(_resp.responseText || (_resp.getResponseHeader && _resp.getResponseHeader('exception')) );
				}
			});
			this.on('show', function() {
				that.gridRegion.show(that.grid);
				that.counterRegion.show(that.counter);
				that.paginatorRegion.show(that.paginator);
				this.ui.inputPercursoDesconto.formatNumber(2);
				this.ui.inputPercentualIss.formatNumber(2);
				this.ui.inputPercentualIrf.formatNumber(2);
				this.ui.inputPercentualInss.formatNumber(2);
				this.ui.inputDiaVencimento.formatNumber(2);
				this.ui.inputPercentualMotorista.formatNumber(2);
				this.ui.inputBanco.formatNumber(2);
		
			});
		},
		 
		searchEmpresa : function(){
			var that = this;

			this.empresas.filterQueryParams = {
	    		cnpj : util.escapeById('inputCnpj'),
	    		numeroContrato : util.escapeById('inputNumeroContrato'),
	    		razaoSocial : util.escapeById('inputRazaoSocial'),
	    		nomeFantasia : util.escapeById('inputNomeFantasia'),
	    		utilizaVoucher : util.escapeById('inputUtilizaVoucher'),
	    		utilizaTicket : util.escapeById('inputUtilizaTicket'),
	    		observacaoTaxista : util.escapeById('inputObservacaoTaxista'),
	    		observacaoCentral : util.escapeById('inputObservacaoCentral'),
	    		percursoDesconto : util.escapeById('inputPercursoDesconto'),
	    		inscricaoMunicipal : util.escapeById('inputInscricaoMunicipal'),
	    		inscricaoEstadual : util.escapeById('inputInscricaoEstadual'),
	    		nf : util.escapeById('inputNf'),
	    		percentualIss : util.escapeById('inputPercentualIss'),
	    		percentualIrf : util.escapeById('inputPercentualIrf'),
	    		percentualInss : util.escapeById('inputPercentualInss'),
	    		diaVencimento : util.escapeById('inputDiaVencimento'),
	    		percentualMotorista : util.escapeById('inputPercentualMotorista'),
	    		banco : util.escapeById('inputBanco'),
	    		agencia : util.escapeById('inputAgencia'),
	    		conta : util.escapeById('inputConta'),
	    		email : util.escapeById('inputEmail'),
			}
			this.empresas.fetch({
				success : function(_coll, _resp, _opt) {
					console.info('Consulta para o grid empresa');
				},
				error : function(_coll, _resp, _opt) {
					console.error(_resp.responseText || (_resp.getResponseHeader && _resp.getResponseHeader('exception')));
				},
				complete : function() {
					
				},
			})		
		},
		resetEmpresa : function(){
			this.ui.form.get(0).reset();
			this.empresas.reset();
		},
				
		getColumns : function() {
			var that = this;
			var columns = [
			{
				name : "cnpj",
				editable : false,
				sortable : true,
				label 	 : "Cnpj",
				cell 	 : "string",
			}, 
			{
				name : "numeroContrato",
				editable : false,
				sortable : true,
				label 	 : "Numero do Contrato",
				cell 	 : "string",
			}, 
			{
				name : "razaoSocial",
				editable : false,
				sortable : true,
				label 	 : "Razao social",
				cell 	 : "string",
			}, 
			{
				name : "nomeFantasia",
				editable : false,
				sortable : true,
				label 	 : "Nome fantasia",
				cell 	 : "string",
			}, 
			{
				name : "utilizaVoucher",
				editable : false,
				sortable : true,
				label 	 : "Utiliza voucher",
				cell 	 : "string",
			}, 
			{
				name : "utilizaTicket",
				editable : false,
				sortable : true,
				label 	 : "Utiliza ticket",
				cell 	 : "string",
			}, 
			{
				name : "observacaoTaxista",
				editable : false,
				sortable : true,
				label 	 : "Observacao taxista",
				cell 	 : "string",
			}, 
			{
				name : "observacaoCentral",
				editable : false,
				sortable : true,
				label 	 : "Observacao central",
				cell 	 : "string",
			}, 
			{
				name : "percursoDesconto",
				editable : false,
				sortable : true,
				label 	 : "Percurso desconto",
				cell : CustomNumberCell.extend({}),
			}, 
			{
				name : "inscricaoMunicipal",
				editable : false,
				sortable : true,
				label 	 : "Inscricao municipal",
				cell 	 : "string",
			}, 
			{
				name : "inscricaoEstadual",
				editable : false,
				sortable : true,
				label 	 : "Inscricao estadual",
				cell 	 : "string",
			}, 
			{
				name : "nf",
				editable : false,
				sortable : true,
				label 	 : "Nf",
				cell 	 : "string",
			}, 
			{
				name : "percentualIss",
				editable : false,
				sortable : true,
				label 	 : "Percentual ISS",
				cell : CustomNumberCell.extend({}),
			}, 
			{
				name : "percentualIrf",
				editable : false,
				sortable : true,
				label 	 : "Percentual IRF",
				cell : CustomNumberCell.extend({}),
			}, 
			{
				name : "percentualInss",
				editable : false,
				sortable : true,
				label 	 : "Percentual INSS",
				cell : CustomNumberCell.extend({}),
			}, 
			{
				name : "diaVencimento",
				editable : false,
				sortable : true,
				label 	 : "Dia vencimento",
				cell : CustomNumberCell.extend({}),
			}, 
			{
				name : "percentualMotorista",
				editable : false,
				sortable : true,
				label 	 : "Percentual motorista",
				cell : CustomNumberCell.extend({}),
			}, 
			{
				name : "banco",
				editable : false,
				sortable : true,
				label 	 : "Banco",
				cell : CustomNumberCell.extend({}),
			}, 
			{
				name : "agencia",
				editable : false,
				sortable : true,
				label 	 : "Agencia",
				cell 	 : "string",
			}, 
			{
				name : "conta",
				editable : false,
				sortable : true,
				label 	 : "Conta",
				cell 	 : "string",
			}, 
			{
				name : "email",
				editable : false,
				sortable : true,
				label 	 : "Email",
				cell 	 : "string",
			}, 
			{
				name : "acoes",
				label : "Ações(Editar, Deletar)",
				sortable : false,
				cell : GeneralActionsCell.extend({
					buttons : that.getCellButtons(),
					context : that,
				})
			} ];
			return columns;
		},
		
		getCellButtons : function() {
			var that = this;
			var buttons = [];

			buttons.push({
				id : 'edita_ficha_button',
				type : 'primary',
				icon : 'icon-pencil fa-pencil',
				hint : 'Editar Empresa',
				onClick : that.editModel,
			}, {
				id : 'delete_button',
				type : 'danger',
				icon : 'icon-trash fa-trash',
				hint : 'Remover Empresa',
				onClick : that.deleteModel,
			});

			return buttons;
		},

		deleteModel : function(model) {
			var that = this;
			
			var modelTipo = new EmpresaModel({
				id : model.id,
			});
			
			util.Bootbox.confirm("Tem certeza que deseja remover o registro [ " + model.get('id') + " ] ?", function(yes) {
				if (yes) {
					modelTipo.destroy({
						success : function() {
							that.empresas.remove(model);
							util.showSuccessMessage('Empresa removido com sucesso!');
						},
						error : function(_model, _resp) {
							util.showErrorMessage('Problema ao remover o registro',_resp);
						}
					});
				}
			});
		},

		editModel : function(model) {
			util.goPage("app/editEmpresa/" + model.get('id'));
		},

		

	});

	return PageEmpresa;
});
